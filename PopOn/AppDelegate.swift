
//  AppDelegate.swift
//  PopOn
//
//  Created by Nitin Suri on 12/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.sharedManager().enable = true
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        
        //Set left Menu
        
        let rightmenu =  UIStoryboard(name: "Main",bundle: nil) .instantiateViewControllerWithIdentifier("LeftMenuVC")
        // let slidecontroller = SlideNavigationController.sharedInstance()
        
        slidecontroller.leftMenu = nil
        slidecontroller.rightMenu = rightmenu
        slidecontroller.menuRevealAnimationDuration = 0.18
        
      let rightmenubutton = UIButton()
      rightmenubutton.frame = CGRectMake(0, 0, 30, 30)
      rightmenubutton.setImage(UIImage(named: "toggle"), forState:.Normal)
      rightmenubutton.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)

      let rightbarbuttonitem = UIBarButtonItem(customView: rightmenubutton)
      slidecontroller.rightBarButtonItem = rightbarbuttonitem
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //print(userDefaults .valueForKey("login"))
        if userDefaults .valueForKey("login") != nil {
            if userDefaults .valueForKey("login") as! String == "yes"
            {
                let QuestionVCObj = storyboard.instantiateViewControllerWithIdentifier("QuestionVC") as! QuestionVC
                slidecontroller.viewControllers = [QuestionVCObj]
                self.window!.rootViewController = slidecontroller
            }
        }
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(application: UIApplication,
                     openURL url: NSURL,
                             sourceApplication: String?,
                             annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(
            application,
            openURL: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
    }
    
    func application( application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData )
    {
        let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>" )
        let deviceTokenString: String = (deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet(characterSet)
            .stringByReplacingOccurrencesOfString(" ", withString: "") as String
        
        //let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(deviceTokenString, forKey: "Device_Token")
        userDefaults.synchronize()
        //print(deviceTokenString)
    }
    func application( application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError )
    {
        var token: String = "414f9df4e50bdbbb080b7a7db8e199e4a7e3a2cf30e861a60135d0996f112b38"
        token = token.stringByReplacingOccurrencesOfString(" ", withString: "")
        //let userDefaults = NSUserDefaults.standardUserDefaults()
        //print(token)
        userDefaults.setValue(token, forKey: "Device_Token")
        userDefaults.synchronize()
    }

    func applicationWillResignActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

