//
//  Hex.h
//  Hexagon
//
//  Created by Raman Kant on 5/18/16.
//
//

#import <UIKit/UIKit.h>
#define kAlphaVisibleThreshold (0.1f)

@interface Hex : UIButton{
    
}

@property (nonatomic, assign) CGPoint   previousTouchPoint;
@property (nonatomic, assign) BOOL      previousTouchHitTestResponse;
@property (nonatomic, strong) UIImage * buttonImage;
@property (nonatomic, strong) UIImage * buttonBackground;

@end
