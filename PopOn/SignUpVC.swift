//
//  SignUpVC.swift
//  PopOn
//
//  Created by Nitin Suri on 13/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader

class SignUpVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,WebServiceDelegate {
    
    
    @IBOutlet weak var textUserName: UITextField!
    
    @IBOutlet weak var textPwd: UITextField!
    
    @IBOutlet weak var textFullName: UITextField!
    
    @IBOutlet weak var textEmail: UITextField!
    
    @IBOutlet weak var textPhone: UITextField!
    @IBOutlet weak var imageUserPic: UIImageView!
    @IBOutlet weak var btnJoin: UIButton!
    var strBase64 = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        
        // Do any additional setup after loading the view.
        
        textUserName.attributedPlaceholder = NSAttributedString(string:"User Name",
                                                                attributes:[NSForegroundColorAttributeName: UIColor.init(red: 41/255, green: 72/255.0, blue: 114/255.0, alpha: 1.0)])
        
        textPwd.attributedPlaceholder = NSAttributedString(string:"Password",
                                                                attributes:[NSForegroundColorAttributeName: UIColor.init(red: 41/255, green: 72/255.0, blue: 114/255.0, alpha: 1.0)])
        
        textFullName.attributedPlaceholder = NSAttributedString(string:"Full Name",
                                                                attributes:[NSForegroundColorAttributeName: UIColor.init(red: 41/255, green: 72/255.0, blue: 114/255.0, alpha: 1.0)])
        
        textEmail.attributedPlaceholder = NSAttributedString(string:"Email",
                                                                attributes:[NSForegroundColorAttributeName: UIColor.init(red: 41/255, green: 72/255.0, blue: 114/255.0, alpha: 1.0)])
        
        textPhone.attributedPlaceholder = NSAttributedString(string:"Phone",
                                                               attributes:[NSForegroundColorAttributeName: UIColor.init(red: 41/255, green: 72/255.0, blue: 114/255.0, alpha: 1.0)])
        
        // SETTING CORNER RADIUS OF UIBUTTON JOIN & USER PROFILE IMAGE
        
        btnJoin.backgroundColor = UIColor.clearColor()
        btnJoin.layer.cornerRadius = 28
        btnJoin.layer.borderWidth = 2
        btnJoin.layer.borderColor = UIColor.init(red: 0/255.0, green: 155/255.0, blue: 251/255.0, alpha: 1.0).CGColor
        
        
        // IMAGE TAP METHOD TO UPLOAD USER PROFILE PIC
        
        imageUserPic.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpVC.imageTappedMethod(_:)))
        //Add the recognizer to your view.
        imageUserPic.addGestureRecognizer(tapRecognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - UILAERT CONTROLLER FOR USER PROFILE IMAGE
    
    func imageTappedMethod(gestureRecognizer: UITapGestureRecognizer) {
        _ = gestureRecognizer.view!
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Pop-Op", message: "Profile Image", preferredStyle: .ActionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Library", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    // MARK: - IMAGE PIKCER CONTROLLER DELEGATE
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        imageUserPic.image = image
        imageUserPic.backgroundColor = UIColor.clearColor()
        imageUserPic.layer.cornerRadius = 45
        imageUserPic.layer.masksToBounds = true
        self.dismissViewControllerAnimated(true, completion: nil);
        
        let imageConv : UIImage = image
        //Now use image to create into NSData format
        
        let imageData:NSData = UIImageJPEGRepresentation(imageConv,0.1)!
        
        strBase64 = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
    }
    
    // MARK: - TEXT FIELD DELEGATE
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    // MARK: - BUTTON BACK ACTION
    
    @IBAction func btnBackAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var newLength = 0
        if textField == textPhone {
            newLength = textPhone.text!.characters.count + string.characters.count - range.length
        }
        
        return newLength <= 10
    }

    
    // MARK: - BUTTON JOIN ACTION METHOD
    
    @IBAction func btnJoinAction(sender: AnyObject) {
        
        let trimNameString = textUserName.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimPasswordString=textPwd.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimFullNameString=textFullName.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimEmailString=textEmail.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let phoneNumberString=textPhone.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        //print(trimNameString)
        
        //let string = "The quick brown dog jumps over the foxy lady."
        let resultName = trimNameString.removeWhitespace()
        
        //print(resultName)
        
        if(resultName.characters.count==0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter the user name." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
            
        }
        else if(trimPasswordString.characters.count==0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter the password." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if(trimFullNameString.characters.count==0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter full name." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if(trimEmailString.characters.count==0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter the email." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
       
       
        else if(phoneNumberString.characters.count==0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter phone number." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if phoneNumberString.characters.count < 10
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Phone number must be 10 characters." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            if !AppManager.sharedManager.isValidEmail(trimEmailString)
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "E-mail is invalid." as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else
            {
                if trimPasswordString.characters.count >= 6 && trimPasswordString.characters.count <= 15
                {
                    if AppManager.NetWorkReachability.isConnectedToNetwork() == true
                    {
                        AppManager.sharedManager.delegate = self
                        
                        SwiftLoader.show(animated: true)
                        
                        if strBase64.characters.count == 0 {
                            strBase64 = ""
                        }
                        //let userDefaults = NSUserDefaults.standardUserDefaults()
                        let deviceToken=userDefaults.valueForKey("Device_Token");
                        
                        let dict_User_Info : NSDictionary = [ "result": "true", "user_name": trimNameString, "password": trimPasswordString, "full_name":  trimFullNameString, "email": trimEmailString, "phone": phoneNumberString, "device_token": deviceToken!, "device_type": "ios", "image": strBase64]
                        
                        do
                        {
                            let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                            //print(jsonString)
                            
                            let params: [String: AnyObject] = ["user_details":jsonString]
                            
                            //print(params)
                            
                            AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KSignUP)
                        }
//                        catch let error as NSError
//                        {
//                            print(error.description)
//                        }
                    }
                    else
                    {
                        let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                        }
                        alertViewShow.addAction(okAction)
                        self.presentViewController(alertViewShow, animated: true, completion: nil)
                    }
                }
                else
                {
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Password length should be atleast 6 to 15 characters." as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
                
            }
            
        }
    }

func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
{
    
    //print(responseDict)
    //AppManager.sharedManager.hideHUD()
    SwiftLoader.hide()
    if serviceurl .isEqualToString(Header.KSignUP)
    {
        if responseDict .valueForKey("result")! .boolValue == true
        {
            //let userDefaults = NSUserDefaults.standardUserDefaults()
            //userDefaults.setObject(responseDict .objectForKey("user_id")! .valueForKeyPath("user_id"), forKey: "UserIdSave")
            
            userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKeyPath("id"), forKey: "UserIdSave")
            userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("access_token"), forKey: "access_token")
            userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("email"), forKey: "email")
            
            if responseDict .objectForKey("user_details")! .valueForKey("user_name") as? String != nil {
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("user_name"), forKey: "user_name")
            }
            else
            {
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_name")
            }
            
            if responseDict .objectForKey("user_details")! .valueForKey("profile_image") as? String != nil {
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("profile_image"), forKey: "profile_image")
            }
            
            if responseDict .objectForKey("user_details")! .valueForKey("city") as? String != nil {
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("city"), forKey: "user_city")
            }
            
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                self.navigationController?.popViewControllerAnimated(false)
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
            
        }
        
        else if responseDict .valueForKey("result")! .boolValue == false
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
}

func failureRsponseError(failureError:NSError)
{
    //print(failureError)
    //AppManager.sharedManager.hideHUD()
    SwiftLoader.hide()
    AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
}
}

// MARK: REMOVE WHITE SPACE FROM USERNAME

extension String {
    func replace(string:String, replacement:String) -> String {
        return self.stringByReplacingOccurrencesOfString(string, withString: replacement, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
}

