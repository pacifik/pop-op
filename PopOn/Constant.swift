//
//  Constant.swift
//  PopOn
//
//  Created by Nitin Suri on 13/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import Foundation

class Header
{
    // MARK: List of Constants
    
    static let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    //static let BASE_URL  = "http://goldrush.dbcollective.co/pop_op/webservices/webservices/"
    //http://dev414.trigma.us/pop_op/apidoc.html
    static let BASE_URL  = "http://dev414.trigma.us/pop_op/webservices/webservices/"
    static let APP_TITLE         = "Pop Op"
    static let KSignUP           = "userRegistration"
    static let KLogin            = "login"
    static let KForgotPassword   = "forgotPassword"
    static let KGetCategories    = "getCategories"
    static let KSaveCategories   = "saveYourCategories"
    static let KgetPolls         = "getPolls"
    static let KSavePolls        = "saveYourAnswer"
    static let KFollowUser       = "follow"
    static let KCreatePoll       = "createYourPoll"
    static let KMyPolls          = "myPolls"
    static let KTagPeople        = "tagPeople"
    static let KGetTagPeople     = "get100RandomUsers"
    static let KAnsweredPolls    = "answeredPolls"
    static let KFollowers        = "followers"
    static let KFollowing        = "following"
    static let KProfileView      = "viewProfile"
    static let KEditProfile      = "editAccount"
    static let KFBlogin          = "loginWithFB"
    static let KResultGender     = "showAvgResultByGender"
    static let KAvgResult        = "showAvgResult"
    static let KLogOut           = "logout"
    static let KResultsAge       = "showAvgResultByAge"
    static let KUnfollow         = "unfollow"
    static let KVerifyEmail      = "verifyEmail"
    static let KTrendingPeople   = "discover"
    static let KChangePushNotify = "changeNotification"
    static let KCheckEmailFB     = "checkEmailExist"
    static let KFollowFBuser     = "followFBFriend"
    static let KUnFollowFBuser   = "unfollowFBFriend"
}

let slidecontroller = SlideNavigationController.sharedInstance()

let userDefaults = NSUserDefaults.standardUserDefaults()

let IS_IPHONE6PLUS = fabs(UIScreen.mainScreen().bounds.size.height-736.0) < 1

let IS_IPHONE5 = fabs(UIScreen.mainScreen().bounds.size.height-568.0) < 1

//let IS_IPAD = (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
//let IS_IOS8 = (UIDevice.currentDevice().systemVersion.floatValue >= 8)
//let APP_DEFAULT_FONT_FACE_NAME = "HelveticaNeue-Light";
//let APP_DEFAULT_FONT_FACE_THIN_NAME = "HelveticaNeue-UltraLight";
//let APP_VERSION_STRING = "1.2";