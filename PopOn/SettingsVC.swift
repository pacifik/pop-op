//
//  SettingsVC.swift
//  PopOn
//
//  Created by Nitin Suri on 27/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit

import SwiftCharts
import FBSDKLoginKit
import FBSDKCoreKit
import SwiftLoader

class SettingsVC: UIViewController,SlideNavigationControllerDelegate,WebServiceDelegate {

    @IBOutlet weak var btnSwitchPush: UISwitch!
    
    @IBOutlet weak var btnRightSliderIOutlet: UIButton!
    
    var pushStatus_str = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btnRightSliderIOutlet.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        btnSwitchPush.setOn(true, animated: false)
        
        btnSwitchPush.on = true
        
        if (userDefaults .valueForKey("pushstatus") != nil) {
            
            if userDefaults .valueForKey("pushstatus") as! String == "yes" {
                
               btnSwitchPush.setOn(true, animated: true)
                btnSwitchPush.on = true
            }
            else
            {
                btnSwitchPush.setOn(true, animated: true)
                btnSwitchPush.on = false
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: < slider delegate for showing left and right menu >
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    // MARK: - BUTTON SWITCH PUSH ACTION
    
    @IBAction func btnSwitchPushAction(sender: AnyObject) {
        if btnSwitchPush.on == true {
            pushStatus_str = "OFF"
            self.changePushNotificationAPI()
        } else {
            pushStatus_str = "ON"
            self.changePushNotificationAPI()
        }
    }
    // MARK: - BUTTON HELP ACTION
    
    @IBAction func btnHelpAction(sender: AnyObject) {
        
    }
    
    // MARK: - BUTTON SIGNOUT ACTION
    
    @IBAction func btnSignOutAction(sender: AnyObject) {
        
        let alertController = UIAlertController(title: Header.APP_TITLE, message: "Are you sure you want to Logout?", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action:UIAlertAction!) in
            //print("you have pressed the Cancel button");
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
            self .logoutAPI()
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true, completion:nil)
    }
    
    // MARK: - LOGOUT API
    
    func logoutAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KLogOut)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - CHANGE PUSH NOTIFICATION API
    
    func changePushNotificationAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"push_status":"ON","access_token": userDefaults .valueForKey("access_token")!]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KChangePushNotify)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        if serviceurl .isEqualToString(Header.KLogOut)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                userDefaults .setValue("no", forKey: "login")
                userDefaults .setValue("no", forKey: "login1")
                //userDefaults .removeObjectForKey("countUserPolls")
                
                userDefaults .removeObjectForKey("user_name")
                
                let login = storyboard!.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC
                slidecontroller.viewControllers = [login]
                
                UIApplication.sharedApplication().keyWindow?.rootViewController = slidecontroller
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            
        }
        else if serviceurl .isEqualToString(Header.KChangePushNotify)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                if responseDict .valueForKey("response") as! String == "Push Notification Enabled." {
                    btnSwitchPush.setOn(true, animated: true)
                    userDefaults .setValue("yes", forKey: "pushstatus")
                    btnSwitchPush.on = true
                }
                else
                {
                btnSwitchPush.setOn(false, animated: true)
                    userDefaults .setValue("no", forKey: "pushstatus")
                    btnSwitchPush.on = false
                }
                
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }
    
    // MARK: BUTTON OPEN MY PROFILE ACTION

    @IBAction func btnMyProfileAction(sender: AnyObject) {
        
        let UserProfileVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("UserProfileVC")as! UserProfileVC
        
        userDefaults .setValue("yes", forKey: "pushSettings")
        
        self.navigationController?.pushViewController(UserProfileVCObj, animated: true)
    }
}
