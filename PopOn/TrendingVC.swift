//
//  TrendingVC.swift
//  PopOn
//
//  Created by Nitin Suri on 13/07/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader

class TrendingVC: UIViewController,SlideNavigationControllerDelegate,WebServiceDelegate {

    @IBOutlet weak var btnToggle: UIButton!
    @IBOutlet weak var scrollTrending: UIScrollView!
    
    var cordBtn         = CGFloat()
    var cordBtnHexONE_Y = CGFloat()
    var cordBtnHexTWO_Y = CGFloat()
    var scrollHeight    = CGFloat()
    
    var tagValue = NSInteger()
    
    var loopCount    = 1
    var loopCount2   = 1
    var arr_Trending = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnToggle.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.trendingPeopleAPI()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hexagonButtonCategories()
    {
        var yPos    = CGFloat()
        yPos        = 0
        var aCount  = 0
       for j in 1 ..< arr_Trending.count
       {
           if ( j % 9 == 0 ) || ((j - 1) % 9 == 0) {
               
               aCount += 1
           }
       }
        
        var indexText = NSInteger()
        
        indexText = 0
        
        for i in 0 ..< aCount{
            
            var xPos = CGFloat()
            
            if i%2 == 0{
                
                xPos = -(CGRectGetWidth(self.view.frame)/4)/2
                
                for j in 0 ..< 5{
                    
                    if arr_Trending.count > indexText {
                    
                    let buttonHexaOne   = Hex(type: UIButtonType.Custom) as UIButton
                    buttonHexaOne.frame = CGRectMake(xPos,yPos, (CGRectGetWidth(self.view.frame)/4) - 5, ((CGRectGetWidth(self.view.frame)/4)) - 5)
                    
                    let urlImage = (NSURL(string: (arr_Trending .objectAtIndex(indexText) .valueForKey("profile_image") as? String)! ))
                    print(urlImage)
                    
                    buttonHexaOne .sd_setImageWithURL(urlImage, forState: UIControlState .Normal)
                    
                    buttonHexaOne.imageView?.contentMode = UIViewContentMode .ScaleAspectFill
                    buttonHexaOne.clipsToBounds = true
                   
                    let maskLayer: CAShapeLayer = CAShapeLayer()
                    maskLayer.fillRule = kCAFillRuleEvenOdd
                    maskLayer.frame = buttonHexaOne.bounds
                    let width: CGFloat = buttonHexaOne.frame.size.width
                    let height: CGFloat = buttonHexaOne.frame.size.height
                    let hPadding: CGFloat = 0
                    UIGraphicsBeginImageContext(buttonHexaOne.frame.size)
                    let path: UIBezierPath = UIBezierPath()
                    path.moveToPoint(CGPointMake(width / 2, 0))
                    path.addLineToPoint(CGPointMake(width - hPadding, height / 4))
                    path.addLineToPoint(CGPointMake(width - hPadding, height * 3 / 4))
                    path.addLineToPoint(CGPointMake(width / 2, height))
                    path.addLineToPoint(CGPointMake(hPadding, height * 3 / 4))
                    path.addLineToPoint(CGPointMake(hPadding, height / 4))
                    path.closePath()
                    path.fill()
                    maskLayer.path = path.CGPath
                    UIGraphicsEndImageContext()
                    buttonHexaOne.layer.mask = maskLayer
                    buttonHexaOne.addTarget(self, action: #selector(CategoriesVC.buttonHexaAction), forControlEvents: UIControlEvents.TouchUpInside)
                    scrollTrending.addSubview(buttonHexaOne)
                    
                    xPos += CGRectGetWidth(self.view.frame)/4
                    
                    buttonHexaOne.tag = tagValue
                    tagValue += 1
                    
                    indexText += 1
                    
                }
                }
                
            }else{
                
                xPos = 0
                
                for j in 0 ..< 4{
                    
                    if arr_Trending.count > indexText {
                    
                    let buttonHexaOne   = Hex(type: UIButtonType.Custom) as UIButton
                    buttonHexaOne.frame = CGRectMake(xPos,yPos, (CGRectGetWidth(self.view.frame)/4) - 5, (CGRectGetWidth(self.view.frame)/4) -  5)
                    buttonHexaOne.backgroundColor = UIColor.randomCategories()
                    let urlImage = (NSURL(string: (arr_Trending .objectAtIndex(indexText) .valueForKey("profile_image") as? String)! ))
            
                    
                    buttonHexaOne .sd_setImageWithURL(urlImage, forState: UIControlState .Normal)
                    let maskLayer: CAShapeLayer = CAShapeLayer()
                    maskLayer.fillRule = kCAFillRuleEvenOdd
                    maskLayer.frame = buttonHexaOne.bounds
                    let width: CGFloat = buttonHexaOne.frame.size.width
                    let height: CGFloat = buttonHexaOne.frame.size.height
                    let hPadding: CGFloat = 0
                    UIGraphicsBeginImageContext(buttonHexaOne.frame.size)
                    let path: UIBezierPath = UIBezierPath()
                    path.moveToPoint(CGPointMake(width / 2, 0))
                    path.addLineToPoint(CGPointMake(width - hPadding, height / 4))
                    path.addLineToPoint(CGPointMake(width - hPadding, height * 3 / 4))
                    path.addLineToPoint(CGPointMake(width / 2, height))
                    path.addLineToPoint(CGPointMake(hPadding, height * 3 / 4))
                    path.addLineToPoint(CGPointMake(hPadding, height / 4))
                    path.closePath()
                    path.fill()
                    maskLayer.path = path.CGPath
                    UIGraphicsEndImageContext()
                    buttonHexaOne.layer.mask = maskLayer
                    buttonHexaOne.addTarget(self, action: #selector(CategoriesVC.buttonHexaAction), forControlEvents: UIControlEvents.TouchUpInside)
                    scrollTrending.addSubview(buttonHexaOne)
                    
                    buttonHexaOne.tag = tagValue
                    tagValue += 1
                    
                    indexText += 1
                    
                    xPos += CGRectGetWidth(self.view.frame)/4
                    
                }
                }
            }
            yPos += CGRectGetWidth(self.view.frame)/4 - (CGRectGetWidth(self.view.frame)/4)/4
            
            scrollHeight = yPos + 100
}

    }
    
    
    @IBAction func buttonHexaAction(sender:UIButton!) {
        
        var intIndex = NSInteger()
        
        intIndex = sender.tag
        
        let indexPath = NSIndexPath(forRow: intIndex, inSection: 0)
        
        //print(indexPath)
        let userProfileObj = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileVC")as! MyProfileVC
            let followID1 = (arr_Trending .objectAtIndex(indexPath.row) .valueForKey("id"))! as AnyObject
            userDefaults .setValue(followID1 , forKey: "otheruserID")
        self.navigationController?.pushViewController(userProfileObj, animated: true)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollTrending.contentSize = CGSizeMake(self.view.frame.size.width, scrollHeight
            + 300)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    // MARK: - TRENDING PEOPLE API
    
    func trendingPeopleAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KTrendingPeople)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KTrendingPeople)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_Trending .removeAllObjects()
                
                print(responseDict .objectForKey("searchResults"))
            arr_Trending .addObjectsFromArray(responseDict .objectForKey("searchResults") as! [AnyObject])
                
                //print(arr_Trending)
                
                self.hexagonButtonCategories()
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }


}
