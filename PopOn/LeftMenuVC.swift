//
//  LeftMenuVC.swift
//  PopOn
//
//  Created by Nitin Suri on 24/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit

class LeftMenuVC: UIViewController {
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imageUserProfile: UIImageView!
    @IBOutlet weak var tableMenu: UITableView!
    var slideOutAnimationEnabled : Bool
    
    var arrTitles:NSArray = []
    
    required init(coder aDecoder: NSCoder) {
        
        slideOutAnimationEnabled = true
        super.init(coder: aDecoder)!
        
        arrTitles = ["FEED","CREATE A POLL","POLLS","FOLLOWERS","FOLLOWING","CATEGORIES","TRENDING","INVITE FRIENDS"]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // print(userDefaults .objectForKey("profile_image") as! String)
        
        if (userDefaults .valueForKey("profile_image") as? String != nil) {
            let imageUrl = NSURL(string: (userDefaults .valueForKey("profile_image") as? String)! )
            imageUserProfile.sd_setImageWithURL(imageUrl!, placeholderImage: UIImage (named: "user-lrg"))
        }
        else
        {
            imageUserProfile.image = UIImage (named: "user-lrg")
        }
        
        if userDefaults .valueForKey("user_name") as? String == "" {
            
            lblUserName.text = userDefaults .valueForKey("user_name") as? String
        }
        else
        {
            lblUserName.text = userDefaults .valueForKey("user_full_name") as? String
        }
        
        if userDefaults .valueForKey("user_city") as? String != nil && userDefaults .valueForKey("state") as? String != nil {
            
            let str_City = userDefaults .valueForKey("user_city") as? String
            
            let str_State = userDefaults .valueForKey("state") as? String
            
            if (str_City == "" && str_State == "") {
                
                lblLocation.text = ""
            }
            else
            {
                lblLocation.text = str_City! + ", " + str_State!
            }
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LeftMenuVC.imageUpdated), name: "imageUpdated", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LeftMenuVC.countFollowing), name: "countFollowing", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LeftMenuVC.userDetailsUpdation), name: "userDetailsUpdated", object: nil)
    }
    
    func imageUpdated() -> Void {
        
        if (userDefaults .valueForKey("profile_image") as? String != nil) {
            let imageUrl = NSURL(string: (userDefaults .valueForKey("profile_image") as? String)! )
            imageUserProfile.sd_setImageWithURL(imageUrl!, placeholderImage: UIImage (named: "user-lrg"))
        }
        else
        {
            imageUserProfile.image = UIImage (named: "user-lrg")
        }
        
        if userDefaults .valueForKey("user_city") as? String != nil && userDefaults .valueForKey("state") as? String != nil {
            
            let str_City = userDefaults .valueForKey("user_city") as? String
            
            let str_State = userDefaults .valueForKey("state") as? String
            
            if (str_City == "" && str_State == "") {
                
                lblLocation.text = ""
            }
            else
            {
                lblLocation.text = str_City! + ", " + str_State!
            }
        }
    }
    
    func countFollowing() -> Void {
        
        tableMenu .reloadData()
    }
    
    func userDetailsUpdation() -> Void {
        
        if userDefaults .valueForKey("user_name") as? String == "" {
            
            lblUserName.text = userDefaults .valueForKey("user_name") as? String
        }
        else
        {
            lblUserName.text = userDefaults .valueForKey("user_full_name") as? String
        }
        
        if userDefaults .valueForKey("user_city") as? String != nil && userDefaults .valueForKey("state") as? String != nil {
            
            let str_City = userDefaults .valueForKey("user_city") as? String
            
            let str_State = userDefaults .valueForKey("state") as? String
            
            if (str_City == "" && str_State == "") {
                
                lblLocation.text = ""
            }
            else
            {
                lblLocation.text = str_City! + ", " + str_State!
            }
            tableMenu .reloadData()
    }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        tableMenu .reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 8
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Custom Cell
        let cellIdentifier = "menucell"
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        let lblTitle = cell.viewWithTag(10)as? UILabel
        lblTitle?.text = arrTitles.objectAtIndex(indexPath.row) as? String
        
        let lblPollCount = cell.viewWithTag(20)as? UILabel
        lblPollCount?.hidden = true
        lblPollCount?.layer.cornerRadius = 10
        lblPollCount?.layer.masksToBounds = true
        
        let imageAdd = cell.viewWithTag(123) as? UIImageView
        imageAdd?.hidden = true
        
        if indexPath.row == 1 {
            imageAdd?.hidden = false
            lblPollCount?.hidden = true
        }
        
       if indexPath.row == 0
        {
            imageAdd?.hidden = true
            
            lblPollCount?.hidden = false
            
            if userDefaults .valueForKey("countUserPolls") != nil {
                if indexPath.row == 0 {
                    let pollCount = userDefaults .valueForKey("countUserPolls") as! NSInteger
                    let  str_Count = "\(String(pollCount))"
                    lblPollCount?.text = String(str_Count)
                }
                else
                {
                    lblPollCount?.text = "0"
                }
            }
        }
        if indexPath.row == 3
        {
            lblPollCount?.hidden = false
            if userDefaults .valueForKey("countFollowers") != nil {
                if indexPath.row == 3 {
                    let pollCount = userDefaults .valueForKey("countFollowers") as! NSInteger
                    let  str_Count = "\(String(pollCount))"
                    lblPollCount?.text = String(str_Count)
                }
                else
                {
                    lblPollCount?.text = "0"
                }
            }
        }
        if indexPath.row == 4
        {
            lblPollCount?.hidden = false
            if userDefaults .valueForKey("countFollowing") != nil {
                if indexPath.row == 4 {
                    let pollCount = userDefaults .valueForKey("countFollowing") as! NSInteger
                    let  str_Count = "\(String(pollCount))"
                    lblPollCount?.text = String(str_Count)
                }
                else
                {
                    lblPollCount?.text = "0"
                }
            }
        }
        if indexPath.row > 4
        {
            imageAdd?.hidden = true
            
            lblPollCount?.hidden = true
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let mainStoryboard =  UIStoryboard(name: "Main",bundle: nil)
        //let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        //selectedCell.contentView.backgroundColor = UIColor.lightGrayColor()
        
        switch (indexPath.row)
        {
        case 0:
            let vc  = mainStoryboard.instantiateViewControllerWithIdentifier("QuestionVC") as! QuestionVC
            SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
            {
                
            }
            
            break
        case 1:
            userDefaults .setValue("no", forKey: "pushquestionscreen")
            let vc  = mainStoryboard.instantiateViewControllerWithIdentifier("CreatePollVC") as! CreatePollVC
        
           SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
           {
               
           }
            
//            SlideNavigationController.sharedInstance().closeMenuWithCompletion(nil)
//            SlideNavigationController.sharedInstance().popToRootViewControllerAnimated(false)
//            
//            for controller : UIViewController in SlideNavigationController.sharedInstance().viewControllers{
//                
//                if controller.isKindOfClass(QuestionVC){
//                    
//                    controller.presentViewController(vc, animated: true, completion: nil)
//                }
            
            //}
            
            //SlideNavigationController.sharedInstance().leftMenu.presentViewController(vc, animated: true, completion: nil)
            
            break
        case 2:
            let vc  = mainStoryboard.instantiateViewControllerWithIdentifier("PollsVC") as! PollsVC
            SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
            {
                //slideout
            }
            
            break
            
        case 3:
            
     
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("ProfileFollowerVC") as! ProfileFollowerVC
            SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
            {
                //slideout
                NSNotificationCenter.defaultCenter().postNotificationName("btnFollower", object: nil)
            }
             
            break
        case 4:
            
            
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("ProfileFollowerVC") as! ProfileFollowerVC
           
            SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
            {
                //slideout
                NSNotificationCenter.defaultCenter().postNotificationName("btnFollowing", object: nil)
            }
            
            break
            
        case 5:
            userDefaults .setValue("no", forKey: "catSkip")
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("CategoriesVC") as! CategoriesVC
            SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
            {
                //slideout
            }
            
            break
            
        case 6:
            
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("TrendingVC") as! TrendingVC
            SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
            {
                //slideout
            }
            
            break
            
        case 7:
            
                let vc = mainStoryboard.instantiateViewControllerWithIdentifier("findfriendsVC") as! findfriendsVC
                SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
                {
                    //slidelayout
                }
            
            //break;

        default :
            SlideNavigationController.sharedInstance().popToRootViewControllerAnimated(false)
            return
        }
        
    }
//     func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
//        let cellToDeSelect:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
//        cellToDeSelect.contentView.backgroundColor = UIColor.clearColor()
//    }
    
//    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
//        let cell = tableView.cellForRowAtIndexPath(indexPath)
//        cell?.contentView.backgroundColor = UIColor.blackColor()
//        cell?.backgroundColor = UIColor.blackColor()
//    }
    
//    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
//        let cellToDeSelect:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
//        cellToDeSelect.contentView.backgroundColor = UIColor.clearColor()
//    }
    
    //MARK: - BUTTON SETTINGS SCREEN
    
    @IBAction func btnSettingsAction(sender: AnyObject) {
        
        let mainStoryboard =  UIStoryboard(name: "Main",bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("SettingsVC") as! SettingsVC
        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
        {
            //
        }
    }
    
    //MARK: - BUTTON USER PROFILE SCREEN

    @IBAction func btnUserProfile(sender: AnyObject) {
        
        //userDefaults .setValue("no", forKey: "login1")
        
        let mainStoryboard =  UIStoryboard(name: "Main",bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("UserProfileVC") as! UserProfileVC
        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled)
        {
            //
        }
    }
}
