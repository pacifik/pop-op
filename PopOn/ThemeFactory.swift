//
//  ThemeFactory.swift
//  QScore
//
//  Created by Gurdev Singh on 1/25/15.
//  Copyright (c) 2015 Gurdev Singh. All rights reserved.
//

import UIKit

class ThemeFactory: NSObject {
    
    var themes:[Theme] = [Theme]()
    
    
    override init() {

        let theme1:Theme=Theme()
        theme1.insights_top_color=UIColor(red: 239.0/255.0, green: 83.0/255.0, blue: 98.0/255.0, alpha: 1.0)
        theme1.insights_bottom_color=UIColor(red: 151.0/255.0, green: 128.0/255.0, blue: 177.0/255.0, alpha: 1.0)
        
        theme1.dashboard_top_color=UIColor(red: 92.0/255.0, green: 195.0/255.0, blue: 206.0/255.0, alpha: 1.0)
        theme1.dashboard_bottom_color=UIColor(red: 151.0/255.0, green: 128.0/255.0, blue: 177.0/255.0, alpha: 1.0)

        theme1.results_top_color=UIColor(red: 42.0/255.0, green: 178.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        theme1.results_bottom_color=UIColor(red: 151.0/255.0, green: 128.0/255.0, blue: 177.0/255.0, alpha: 1.0)
        
        theme1.pha_top_color=UIColor(red: 151.0/255.0, green: 128.0/255.0, blue: 177.0/255.0, alpha: 1.0)
        theme1.pha_bottom_color=UIColor(red: 42.0/255.0, green: 178.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        
        
        
        let theme2:Theme=Theme()
        theme2.insights_top_color=UIColor(red: 0.0/255.0, green: 215.0/255.0, blue: 127.0/255.0, alpha: 1.0)
        theme2.insights_bottom_color=UIColor(red: 196.0/255.0, green: 99.0/255.0, blue: 173.0/255.0, alpha: 1.0)
        
        theme2.dashboard_top_color=UIColor(red: 94.0/255.0, green: 199.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        theme2.dashboard_bottom_color=UIColor(red: 255.0/255.0, green: 10.0/255.0, blue: 199.0/255.0, alpha: 1.0)
        
        theme2.results_top_color=UIColor(red: 255.0/255.0, green: 163.0/255.0, blue: 57.0/255.0, alpha: 1.0)
        theme2.results_bottom_color=UIColor(red: 94.0/255.0, green: 199.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        
        theme2.pha_top_color=UIColor(red: 94.0/255.0, green: 199.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        theme2.pha_bottom_color=UIColor(red: 255.0/255.0, green: 163.0/255.0, blue: 57.0/255.0, alpha: 1.0)
        
        
        let theme3:Theme=Theme()
        theme3.insights_top_color=UIColor(red: 255.0/255.0, green: 10.0/255.0, blue: 199.0/255.0, alpha: 1.0)
        theme3.insights_bottom_color=UIColor(red: 234.0/255.0, green: 0.0/255.0, blue: 122.0/255.0, alpha: 1.0)
        
        theme3.dashboard_top_color=UIColor(red: 94.0/255.0, green: 199.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        theme3.dashboard_bottom_color=UIColor(red: 22.0/255.0, green: 216.0/255.0, blue: 190.0/255.0, alpha: 1.0)
        
        theme3.results_top_color=UIColor(red: 255.0/255.0, green: 234.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        theme3.results_bottom_color=UIColor(red: 237.0/255.0, green: 75.0/255.0, blue: 42.0/255.0, alpha: 1.0)
        
        theme3.pha_top_color=UIColor(red: 237.0/255.0, green: 75.0/255.0, blue: 42.0/255.0, alpha: 1.0)
        theme3.pha_bottom_color=UIColor(red: 255.0/255.0, green: 234.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        
        theme1.createLayers()
        theme2.createLayers()
        theme3.createLayers()
        themes.append(theme1)
        themes.append(theme2)
        themes.append(theme3)
    }
    
    func GetTheme(themeIndex:Int) -> Theme {
        return themes[themeIndex]
    }
   
}
