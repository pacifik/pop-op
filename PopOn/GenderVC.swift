//
//  GenderVC.swift
//  PopOn
//
//  Created by Nitin Suri on 02/06/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader

class GenderVC: UIViewController,SlideNavigationControllerDelegate,WebServiceDelegate {
    
    @IBOutlet weak var lblTotalPolls: UILabel!
    var progress: KDCircularProgress!
    
    var progress2: KDCircularProgress2!
    
    @IBOutlet weak var lblChoice2Votes: UILabel!
    @IBOutlet weak var lblChoice2Answer: UILabel!
    @IBOutlet weak var lblChoice1Votes: UILabel!
    @IBOutlet weak var lblChoice1Answer: UILabel!
    @IBOutlet weak var viewUpvotesProgress: TWRProgressView!
    
    @IBOutlet weak var lblChoice2: UILabel!
    @IBOutlet weak var lblChoice1: UILabel!
    @IBOutlet weak var viewDownvotesProgress: TWRProgressView!
    @IBOutlet weak var btnRigthSliderToggle: UIButton!
    
    var arr_GeneralResult = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        btnRigthSliderToggle.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        viewUpvotesProgress.hidden   = true
        viewDownvotesProgress.hidden = true
        
        self.averageResultAPI()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction(sender: AnyObject) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(PollsVC) || controller.isKindOfClass(ProfileFollowerVC) || controller.isKindOfClass(MyProfileVC) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func btnGenderAction(sender: AnyObject) {
        
        let results_Age = self.storyboard?.instantiateViewControllerWithIdentifier("AgeVC")as!
        AgeVC
        self.navigationController?.pushViewController(results_Age, animated: false)
    }
    
    @IBAction func btnAgeAction(sender: AnyObject) {
        
        let results_Result = self.storyboard?.instantiateViewControllerWithIdentifier("ResultsVC")as!
        ResultsVC
        self.navigationController?.pushViewController(results_Result, animated: false)
    }
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    // MARK: - MY POLLS API
    
    func averageResultAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!, "poll_id": userDefaults .valueForKey("poll_id")!,"access_token": userDefaults .valueForKey("access_token")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KAvgResult)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KAvgResult)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_GeneralResult .removeAllObjects()
                arr_GeneralResult .addObject(responseDict .objectForKey("avgResult")! )
                
                viewUpvotesProgress.hidden   = false
                viewDownvotesProgress.hidden = false
                
                let votesTotalCount = arr_GeneralResult .objectAtIndex(0) .valueForKey("totalVotes")! as! NSInteger
                
                let TotalVotes = "\(String(votesTotalCount)) Votes"
                
                lblTotalPolls.text = String(TotalVotes)
                
                let emojiRetrieveChoice1 = arr_GeneralResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("answer")! as? String
                let dataChoice1: NSData = emojiRetrieveChoice1!.dataUsingEncoding(NSUTF8StringEncoding)!
                let strEmojiChoice1: String = String(data: dataChoice1, encoding: NSNonLossyASCIIStringEncoding)!
                
                lblChoice1Answer.text = strEmojiChoice1
                
                let votesCount1 = arr_GeneralResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("votesCount")! as! NSInteger
                
                let mVotes1 = "\(String(votesCount1)) Votes"
                
                lblChoice1Votes.text = String(mVotes1)
                
                let VotesPercent1 = arr_GeneralResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("votesPer")! as! NSInteger
                
                let VotesPercent2 = arr_GeneralResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("votesPer")! as! NSInteger
                
                if VotesPercent1 > VotesPercent2 {
                    
                    let mVotePecr1 = "\(String(VotesPercent1))%"
                    
                    lblChoice1.text = String(mVotePecr1)
                    
                    let mVotePecr2 = "\(String(VotesPercent2))%"
                    
                    lblChoice2.text = String(mVotePecr2)
                    
                    lblChoice1.font = lblChoice1.font.fontWithSize(54)
                    
                    lblChoice2.font = lblChoice2.font.fontWithSize(38)
                }
                else if VotesPercent1 == VotesPercent2
                {
                    let mVotePecr1 = "\(String(VotesPercent1))%"
                    
                    lblChoice1.text = String(mVotePecr1)
                    
                    let mVotePecr2 = "\(String(VotesPercent2))%"
                    
                    lblChoice2.text = String(mVotePecr2)
                    
                    lblChoice1.font = lblChoice1.font.fontWithSize(54)
                    
                    lblChoice2.font = lblChoice2.font.fontWithSize(54)
                }
                else
                {
                    let mVotePecr1 = "\(String(VotesPercent1))%"
                    
                    lblChoice1.text = String(mVotePecr1)
                    
                    let mVotePecr2 = "\(String(VotesPercent2))%"
                    
                    lblChoice2.text = String(mVotePecr2)
                    
                    lblChoice1.font = lblChoice1.font.fontWithSize(38)
                    
                    lblChoice2.font = lblChoice2.font.fontWithSize(54)
                    
                }
                
                //let vP1 =
                
                
                
                //******************************************************
                
                let emojiRetrieveChoice2 = arr_GeneralResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("answer")! as? String
                let dataChoice2: NSData = emojiRetrieveChoice2!.dataUsingEncoding(NSUTF8StringEncoding)!
                let strEmojiChoice2: String = String(data: dataChoice2, encoding: NSNonLossyASCIIStringEncoding)!
                
                lblChoice2Answer.text = strEmojiChoice2
                
                let votesCount2 = arr_GeneralResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("votesCount")! as! NSInteger
                
                let mVotes2 = "\(String(votesCount2)) Votes"
                
                lblChoice2Votes.text = String(mVotes2)
                
                
                //*******************************************************
                
                //...........CIRCULAR CLOCK WISE ANIMATION...............
                
                progress = KDCircularProgress(frame: CGRectMake(0, 20, self.view.frame.size.width + 20 , self.view.frame.size.width + 20 ))
                progress.startAngle = -90
                progress.progressThickness = 0.2
                progress.trackThickness = 0.5
                progress.clockwise = true
                progress.gradientRotateSpeed = 5
                progress.roundedCorners = true
                progress.glowMode = .Forward
                progress.glowAmount = 0
                progress.trackColor = UIColor.clearColor()
                progress.setColors(UIColor.init(red: 235/255.0, green: 76/255.0, blue: 85/255.0, alpha: 1.0))
                progress.center = CGPoint(x: view.center.x, y: view.center.y - 10)
                view.addSubview(progress)
                
                let votesPercentDouble1 = Double(VotesPercent1)
                
                progress.animateFromAngle(0, toAngle: 3.6*votesPercentDouble1, duration: 1.4) { completed in
                    
                }
                
                //...........CIRCULAR ANTI - CLOCK WISE ANIMATION................
                
                progress2 = KDCircularProgress2(frame: CGRectMake(0, 20, self.view.frame.size.width + 20 , self.view.frame.size.width + 20 ))
                progress2.startAngle = -90
                progress2.progressThickness = 0.2
                progress2.trackThickness = 0.5
                progress2.clockwise = false
                progress2.gradientRotateSpeed = 5
                progress2.roundedCorners = true
                progress2.glowMode = .Reverse
                progress2.glowAmount = 0
                progress2.trackColor = UIColor.clearColor()
                
                progress2.setColors(UIColor.init(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0))
                progress2.center = CGPoint(x: view.center.x, y: view.center.y - 10)
                view.addSubview(progress2)
                
                let votesDouble2 = Double(VotesPercent2)
                
                progress2.animateFromAngle(0, toAngle: 3.6*votesDouble2, duration: 1) { completed in
                }
                
                let imageUpFade = UIImage(named: "black-bg")
                viewUpvotesProgress.maskingImage = imageUpFade
                
                viewUpvotesProgress.setBackColor(UIColor .init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0))
                
                viewUpvotesProgress.setFrontColor(UIColor.init(red: 235/255.0, green: 76/255.0, blue: 85/255.0, alpha: 1.0))
                
                var  startingUpFloat = CGFloat()
                
                let choice1Float = CGFloat(VotesPercent1)
                
                startingUpFloat = choice1Float/100
                viewUpvotesProgress.setProgress(startingUpFloat, animated: true)
                
                viewUpvotesProgress.horizontal = true
                
                let imageDownFade = UIImage(named: "black-bg")
                viewDownvotesProgress.maskingImage = imageDownFade
                
                viewDownvotesProgress.setBackColor(UIColor .init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0))
                
                viewDownvotesProgress.setFrontColor(UIColor.init(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0))
                
                viewDownvotesProgress.horizontal = true
                
                var startingDownFloat = CGFloat()
                
                let choice2Float = CGFloat(VotesPercent2)
                startingDownFloat = choice2Float/100
                viewDownvotesProgress.setProgress(startingDownFloat, animated: true)
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }
    
}
