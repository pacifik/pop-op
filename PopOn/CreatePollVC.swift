//
//  CreatePollVC.swift
//  PopOn
//
//  Created by Nitin Suri on 25/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit

import SearchEmojiOnString
import SwiftLoader

class CreatePollVC: UIViewController,SlideNavigationControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate, UIPickerViewDataSource, UIPickerViewDelegate,WebServiceDelegate {

    @IBOutlet weak var btnPickImage1MultiPoll: UIButton!
    
    @IBOutlet weak var btnPickerImg2MultiPoll: UIButton!
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var imagePoll1: UIImageView!
    
    @IBOutlet weak var textYourQuestion: UITextView!
    
    @IBOutlet weak var textChoice1: UITextField!
    
    @IBOutlet weak var textChoice2: UITextField!
    @IBOutlet weak var btnTagPeople: UIButton!
    @IBOutlet weak var btnPostOutlet: UIButton!
    
    @IBOutlet weak var textBorderChoice2: UITextField!
    @IBOutlet weak var textBorderChoice1: UITextField!
    @IBOutlet weak var imageLeftPoll2: UIImageView!
    @IBOutlet weak var imageRightPoll2: UIImageView!
    @IBOutlet weak var viewRedLeftPoll2: UIView!
    @IBOutlet weak var viewGreenRightPoll2: UIView!
    @IBOutlet weak var viewQuestionBG: UIView!
    @IBOutlet weak var lbl120text: UILabel!
    
    @IBOutlet weak var lbl16Choice1: UILabel!
    @IBOutlet weak var lbl16Choice2: UILabel!
    
    @IBOutlet weak var btnImagePickerOutlet: UIButton!
    @IBOutlet weak var btnPoll2Outlet: UIButton!
    @IBOutlet weak var btnPoll1Outlet: UIButton!
    @IBOutlet weak var btnCancelViewOutlet: UIButton!
    
    @IBOutlet weak var pickerTextField: UITextField!
    
    var arrCategoryOptions = NSMutableArray()
    
    var arr_CategoriesObj   :NSMutableArray = []
    
    var arr_TagID          = NSMutableArray()
    
    let arr_ImagesPoll1    = NSMutableArray()
    let arr_ImagesPoll2    = NSMutableArray()
    
    var str_CatID          = String()
    
    let imageCompressed    = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

         //Do any additional setup after loading the view.
            if userDefaults .valueForKey("pushquestionscreen") as! String == "yes" {
                btnBackOutlet.hidden = false
            }
        else
        {
            btnBackOutlet.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        }
        
        let pickerView = UIPickerView()
        
        pickerView.delegate = self
        
        pickerTextField.inputView = pickerView
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        
        btnPostOutlet.backgroundColor = UIColor.clearColor()
        btnPostOutlet.layer.cornerRadius = 28
        btnPostOutlet.layer.borderWidth = 2
        btnPostOutlet.layer.borderColor = UIColor.init(red: 0/255.0, green: 155/255.0, blue: 251/255.0, alpha: 1.0).CGColor
        
        textBorderChoice1.layer.borderWidth = 2
        textBorderChoice1.layer.borderColor = UIColor.init(red: 255/255.0, green: 92/255.0, blue: 106/255.0, alpha: 1.0).CGColor
        
        textBorderChoice2.layer.borderWidth = 2
        textBorderChoice2.layer.borderColor = UIColor.init(red: 122/255.0, green: 255/255.0, blue: 80/255.0, alpha: 1.0).CGColor
        
        btnPoll1Outlet.layer.borderWidth = 2
        btnPoll1Outlet.layer.borderColor = UIColor.blackColor().CGColor
        
        imageLeftPoll2.hidden       = true
        imageRightPoll2.hidden      = true
        viewRedLeftPoll2.hidden     = true
        viewGreenRightPoll2.hidden  = true
        
        
        pickerTextField.attributedPlaceholder = NSAttributedString(string:"Select Category",
                                                             attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        
        let paddingView1 = UIView(frame: CGRectMake(0, 0, 15, textChoice1.frame.height))
        textChoice1.leftView = paddingView1
        textChoice1.leftViewMode = UITextFieldViewMode.Always
        
        let paddingView2 = UIView(frame: CGRectMake(0, 0, 15, textChoice2.frame.height))
        textChoice2.leftView = paddingView2
        textChoice2.leftViewMode = UITextFieldViewMode.Always
        
        let paddingView3 = UIView(frame: CGRectMake(0, 0, 15, pickerTextField.frame.height))
        pickerTextField.leftView = paddingView3
        pickerTextField.leftViewMode = UITextFieldViewMode.Always
        
        btnPoll1Outlet.selected       = true
        btnPoll2Outlet.selected       = false
        btnPickImage1MultiPoll.hidden = true
        btnPickerImg2MultiPoll.hidden = true
        
        textYourQuestion.text = "Your question..."
        textYourQuestion.textColor = UIColor.lightGrayColor()
        
        arrayTagPeople.sharedInstance .removeAllObjects()
        
        arr_TagID .removeAllObjects()
        
        self.getCategoriesAPImethod()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.navigationController?.navigationBarHidden = true
        
        if arrayTagPeople.sharedInstance.count > 0 {
            arr_TagID = arrayTagPeople.sharedInstance
        }
        else
        {
            arr_TagID .removeAllObjects()
        }
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .None)
    }
    
    override func viewWillDisappear(animated: Bool) {
        textChoice1      .resignFirstResponder()
        textChoice2      .resignFirstResponder()
        textYourQuestion .resignFirstResponder()
        pickerTextField  .resignFirstResponder()
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .None)
    }
    
//    override func prefersStatusBarHidden() -> Bool {
//        return true
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - TEXT FIELD FIX LENGTH VALIDATIONS
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let emojis: String = String("\(String(textField.text!))\(String(string))")
        let emojiCount: Int = emojis.emo_emojiCount()
        
        let newStr = emojis.stringByRemovingEmoji()
        var countTextChoice1 = Int()
        if newStr.characters.count + emojiCount <= 16 {
            countTextChoice1 = 16 - newStr.characters.count - emojiCount
            if textField == textChoice1 {
                lbl16Choice1.text = String(countTextChoice1)
            }
            else{
                lbl16Choice2.text = String(countTextChoice1)
            }
        }
        return newStr.characters.count + emojiCount <= 16
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        let emojis: String = String("\(String(textView.text!))\(String(text))")
        let emojiCount: Int = emojis.emo_emojiCount()
        let newStr = emojis.stringByRemovingEmoji()
        var countTextChoice1 = Int()
        if newStr.characters.count + emojiCount <= 120 {
            countTextChoice1 = 120 - newStr.characters.count - emojiCount
            if textView == textYourQuestion {
                lbl120text.text = String(countTextChoice1)
            }
            else
            {
                lbl120text.text = String(countTextChoice1)
            }
        }
        return newStr.characters.count + emojiCount <= 120
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.textColor == UIColor.lightGrayColor() {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Your question..."
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    // MARK: - TEXT FIELD DELEGATE
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    // MARK: - BUTTON TAG PEOPLE ACTION
    
    @IBAction func btnTagPeopleAction(sender: AnyObject) {
        
        let TagPeopleVCbj = self.storyboard?.instantiateViewControllerWithIdentifier("TagPeopleVC")as! TagPeopleVC
        
            arrayTagPeople.sharedInstance = arr_TagID
        
        self.presentViewController(TagPeopleVCbj, animated: false, completion: nil)
        
        //self.navigationController?.pushViewController(TagPeopleVCbj, animated: false)
    }
    
    // MARK: - BUTTON POST POLL ACTION
    
    @IBAction func btnPostAction(sender: AnyObject) {
        
        let trimQuestionString = textYourQuestion.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimYourChoice1String = textChoice1.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimYourChoice2String = textChoice2.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimCategoryString = pickerTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        if(trimQuestionString.characters.count == 0 || textYourQuestion.text == "Your question...")
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter your question." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if(trimYourChoice1String.characters.count == 0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter your first choice." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if(trimYourChoice2String.characters.count == 0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter your second choice." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if(trimCategoryString.characters.count == 0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please select a category." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            if btnPoll1Outlet.selected == true {
                
                if imagePoll1.image == nil {
                    
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please select poll image." as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
                else
                {
                    self.createYourPollAPI()
                }
            }
            else if btnPoll2Outlet.selected == true
            {
                if imageLeftPoll2.image == nil || imageRightPoll2.image == nil {
                    
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please select both poll images." as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
                else
                {
                    self.createYourPollAPI()
                }
            }
        }
    }
    
    // MARK: - BUTTON POLL 1 ACTION
    
    @IBAction func btnPoll1Action(sender: AnyObject) {
        
        imageLeftPoll2.hidden         = true
        imageRightPoll2.hidden        = true
        viewRedLeftPoll2.hidden       = true
        viewGreenRightPoll2.hidden    = true
        imagePoll1.hidden             = false
        btnPickImage1MultiPoll.hidden = true
        btnPickerImg2MultiPoll.hidden = true
        btnImagePickerOutlet.hidden   = false
        
        btnPoll2Outlet.setImage(UIImage(named: "duble-box"), forState: UIControlState.Normal)
        btnPoll2Outlet.selected = false
        btnPoll1Outlet.selected = true
        
        btnPoll1Outlet.layer.borderWidth = 2
        btnPoll1Outlet.layer.borderColor = UIColor.blackColor().CGColor
    }
    
    // MARK: - BUTTON POLL 2 ACTION
    
    @IBAction func btnPoll2Action(sender: AnyObject) {
        
        imageLeftPoll2.hidden         = false
        imageRightPoll2.hidden        = false
        viewRedLeftPoll2.hidden       = false
        viewGreenRightPoll2.hidden    = false
        imagePoll1.hidden             = true
        btnPickImage1MultiPoll.hidden = false
        btnPickerImg2MultiPoll.hidden = false
        btnImagePickerOutlet.hidden   = true
        
        btnPoll2Outlet.setImage(UIImage(named: "black-slide"), forState: UIControlState.Normal)
        btnPoll2Outlet.selected = true
        btnPoll1Outlet.selected = false
        
        btnPoll1Outlet.layer.borderWidth = 2
        btnPoll1Outlet.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    // MARK: - BUTTON POLL IMAGE PICKER ACTION
    
    @IBAction func btnImagePickerAction(sender: AnyObject) {
        btnPickImage1MultiPoll.selected = false
        btnPickerImg2MultiPoll.selected = false
        self.imagePickerAction()
    }
    
    // MARK: - IMAGE PICKER CONTROLLER DELEGATE
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
        if btnPoll1Outlet.selected == true {
            
            btnPickImage1MultiPoll.selected = false
            btnPickerImg2MultiPoll.selected = false
            imagePoll1.image = image
            btnImagePickerOutlet .setImage(nil, forState: UIControlState .Normal)
        }
        else if btnPickImage1MultiPoll.selected == true
        {
            btnPoll1Outlet.selected         = false
            btnPickerImg2MultiPoll.selected = false
            
            imageLeftPoll2.image = image
            btnPickImage1MultiPoll .setImage(nil, forState: UIControlState .Normal)
        }
        else if btnPickerImg2MultiPoll.selected == true
        {
            btnPoll1Outlet.selected         = false
            btnPickImage1MultiPoll.selected = false
            
            imageRightPoll2.image = image
            btnPickerImg2MultiPoll .setImage(nil, forState: UIControlState .Normal)
        }
        self.dismissViewControllerAnimated(true, completion: nil);
        //Use image name from bundle to create NSData
        let imageConv : UIImage = image
        //Now use image to create into NSData format
        
        let imageData:NSData = UIImageJPEGRepresentation(imageConv,0.1)!
        
        var strBase64 = NSString()
        strBase64 = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        //print(strBase64)
        if btnPoll1Outlet.selected == true {
            let dict_ImgData: NSDictionary = ["image":strBase64]
            arr_ImagesPoll1 .removeAllObjects()
            arr_ImagesPoll1 .addObject(dict_ImgData)
        }
        else
        {
            let dict_ImgData2: NSDictionary = ["image":strBase64]
            
            print(arr_ImagesPoll2.count)
            
            if arr_ImagesPoll2.count == 0 {
                
                arr_ImagesPoll2 .addObject(dict_ImgData2)
            }
            else if arr_ImagesPoll2.count == 1
            
            {
                arr_ImagesPoll2 .addObject(dict_ImgData2)
            }
            else
            {
            
            if btnPickImage1MultiPoll.selected == true {
                
                arr_ImagesPoll2 .replaceObjectAtIndex(0, withObject: dict_ImgData2)
            }
            else if btnPickerImg2MultiPoll.selected == true
            {
                arr_ImagesPoll2 .replaceObjectAtIndex(1, withObject: dict_ImgData2)
            }
            }
            
            print(arr_ImagesPoll2.count)
            
        }
    }
    
    // MARK: BUTTON IMAGE POLL 1 MULTI ACTION
    
    @IBAction func btnImg1MultiPollAction(sender: AnyObject) {
        
        btnPickImage1MultiPoll.selected = true
        btnPickerImg2MultiPoll.selected = false
        btnPoll1Outlet.selected = false
        self.imagePickerAction()
    }
    
    // MARK: BUTTON IMAGE POLL 2 MULTI ACTION
    
    @IBAction func btnImg2MultiPollAction(sender: AnyObject) {
        btnPickImage1MultiPoll.selected = false
        btnPickerImg2MultiPoll.selected = true
        btnPoll1Outlet.selected = false
        self.imagePickerAction()
    }
    
    // MARK: - IMAGE PICKER ACTION
    
    func imagePickerAction() -> Void {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Pop-Op", message: "Profile Image", preferredStyle: .ActionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Library", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
        
    }
    
    // MARK: - PICKER VIEW DELEGATE AND DATASOURCE
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_CategoriesObj.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        str_CatID = (arr_CategoriesObj .objectAtIndex(row) .valueForKey("id") as? String)!
        
        pickerTextField.text = arr_CategoriesObj .objectAtIndex(row) .valueForKey("category_name") as? String
        return arr_CategoriesObj .objectAtIndex(row) .valueForKey("category_name") as? String
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerTextField.text = arr_CategoriesObj .objectAtIndex(row) .valueForKey("category_name") as? String
        
        str_CatID = (arr_CategoriesObj .objectAtIndex(row) .valueForKey("id") as? String)!
    }
    
    // MARK: BUTTON BACK ACTION METHOD
    
    @IBAction func btnBackAction(sender: AnyObject) {
        
//        let questionsObj = self.storyboard?.instantiateViewControllerWithIdentifier("QuestionVC") as! QuestionVC
        
        
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        //self.navigationController?.pushViewController(questionsObj, animated: false)
    }
    // MARK: - BUTTON CANCEL POLL ACTION
   
    @IBAction func btnCancelViewAction(sender: AnyObject) {
        
    }
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    
    //MARK: - GET CATEGORIES API METHOD
    
    func getCategoriesAPImethod()  {
        
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = [ "result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                
                let params: [String: AnyObject] = ["user_details":jsonString]
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KGetCategories)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - CREATE YOUR POLL API
    
    func createYourPollAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            var str_PollType = String()
            var arr_ImageData = NSArray()
            if btnPoll1Outlet.selected == true {
                str_PollType = "single"
                arr_ImageData = arr_ImagesPoll1
            }
            else
            {
                str_PollType = "multi"
                arr_ImageData = arr_ImagesPoll2
            }
            
            //**************** EMOJI ENCODING ***************************
            
            let dataChoice1: NSData = textChoice1.text!.dataUsingEncoding(NSNonLossyASCIIStringEncoding)!
            let choice1Emoji: String = String(data: dataChoice1, encoding: NSUTF8StringEncoding)!

            let dataChoice2: NSData = textChoice2.text!.dataUsingEncoding(NSNonLossyASCIIStringEncoding)!
            let choice2Emoji: String = String(data: dataChoice2, encoding: NSUTF8StringEncoding)!
            
            let dataQuestion: NSData = textYourQuestion.text.dataUsingEncoding(NSNonLossyASCIIStringEncoding)!
            let questionEmoji: String = String(data: dataQuestion, encoding: NSUTF8StringEncoding)!
            //**************** ************** ***************************
            
            let dict_Choices : NSDictionary = ["choice1": choice1Emoji,"choice2": choice2Emoji]
            //let arr_TagPeople = NSMutableArray()
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "question": questionEmoji, "choices":dict_Choices,"tag_people":arr_TagID, "poll_type":str_PollType, "category_ids":str_CatID, "images":arr_ImageData]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KCreatePoll)
            }
        }
            
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KCreatePoll)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                textChoice1.text = ""
                textChoice2.text = ""
                textYourQuestion.text = "Your question..."
                textYourQuestion.textColor = UIColor.lightGrayColor()
                imagePoll1.image = nil
                imageLeftPoll2.image = nil
                imageRightPoll2.image = nil
                pickerTextField.text = ""
                
                userDefaults .removeObjectForKey("tag_ID")
                
                arrayTagPeople.sharedInstance .removeAllObjects()
                
                arr_TagID .removeAllObjects()
                
                //userDefaults .setValue(responseDict .valueForKey("countUserPolls"), forKey: "countUserPolls")
                
               // NSNotificationCenter.defaultCenter().postNotificationName("countFollowing", object: nil)
            
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    
                    let questionsObj = self.storyboard?.instantiateViewControllerWithIdentifier("QuestionVC") as! QuestionVC
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("updatedPolls", object: nil)
                    
                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                    self.navigationController?.pushViewController(questionsObj, animated: false)
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if serviceurl .isEqualToString(Header.KGetCategories)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                
                arr_CategoriesObj .removeAllObjects()
                
        arr_CategoriesObj .addObjectsFromArray(responseDict .valueForKey("categories")! as! [AnyObject])
                
            }
                
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }

}

extension Character {
    func isEmoji() -> Bool {
        return Character(UnicodeScalar(0x1d000)) <= self && self <= Character(UnicodeScalar(0x1f77f))
            || Character(UnicodeScalar(0x2100)) <= self && self <= Character(UnicodeScalar(0x26ff))
    }
}

extension String {
    func stringByRemovingEmoji() -> String {
        return String(self.characters.filter{!$0.isEmoji()})
    }
}



