//
//  ForgotPasswordVC.swift
//  PopOn
//
//  Created by Nitin Suri on 16/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader

class ForgotPasswordVC: UIViewController,WebServiceDelegate {

    @IBOutlet weak var textViewForgot: UITextView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var textForgotPwd: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textForgotPwd.attributedPlaceholder = NSAttributedString(string:"email@email.com",
                                                                attributes:[NSForegroundColorAttributeName: UIColor.init(red: 41/255, green: 72/255.0, blue: 114/255.0, alpha: 1.0)])

        self.navigationController?.navigationBarHidden = true
        // Do any additional setup after loading the view.
        
//        textViewForgot.backgroundColor = UIColor.clearColor()
//        textViewForgot.layer.cornerRadius = 2
//        textViewForgot.layer.borderWidth = 1
//        textViewForgot.layer.borderColor = UIColor.init(red: 0/255.0, green: 155/255.0, blue: 251/255.0, alpha: 0.5).CGColor
        
        btnSubmit.backgroundColor = UIColor.clearColor()
        btnSubmit.layer.cornerRadius = 28
        btnSubmit.layer.borderWidth = 2
        btnSubmit.layer.borderColor = UIColor.init(red: 0/255.0, green: 155/255.0, blue: 251/255.0, alpha: 1.0).CGColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - TEXT FIELD DELEGATE
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    // MARK: - BACK BUTTON VIEW ACTION

    @IBAction func btnBackAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - BUTTON SUBMIT ACTION
    
    @IBAction func btnSubmitAction(sender: AnyObject) {
        
        let trimemailString = textForgotPwd.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
         if(trimemailString.characters.count == 0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter the email." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            
            if !AppManager.sharedManager.isValidEmail(trimemailString)
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "E-mail is invalid." as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else
            {
                if AppManager.NetWorkReachability.isConnectedToNetwork() == true
                {
                    AppManager.sharedManager.delegate = self
                    
                    SwiftLoader.show(animated: true)
                    
                    //let userDefaults = NSUserDefaults.standardUserDefaults()
                    var deviceToken=userDefaults.valueForKey("Device_Token");
                    
                    deviceToken = "414f9df4e50bdbbb080b7a7db8e199e4a7e3a2cf30e861a60135d0996f112b38"
                    
                    let dict_User_Info : NSDictionary = [ "result": "true", "user_name": "", "email": trimemailString, "device_token": deviceToken!, "device_type": "ios"]
                    do
                    {
                        let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                        
                        textForgotPwd.resignFirstResponder()
                        
                        let params: [String: AnyObject] = ["user_details":jsonString]
                        AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KForgotPassword)
                    }
                }
                else
                {
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
            }
            
        }
        
    }
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        //AppManager.sharedManager.hideHUD()
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KForgotPassword)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        //AppManager.sharedManager.hideHUD()
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }

}
