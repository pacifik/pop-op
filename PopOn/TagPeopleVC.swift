//
//  TagPeopleVC.swift
//  PopOn
//
//  Created by Nitin Suri on 31/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit

class TagPeopleVC: UIViewController, WebServiceDelegate,UISearchBarDelegate {

    @IBOutlet weak var tableTag: UITableView!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var textSearch: UITextField!
    
    @IBOutlet weak var searchPeople: UISearchBar!
    var arr_SearchResults     = NSMutableArray()
        
    var is_searching:Bool!
    
    var dataArray:NSMutableArray!  // Its data array for UITableview
    var searchingDataArray:NSMutableArray!
    
    var postDataTask = NSURLSessionDataTask()
    
    override func viewDidLoad() {
        super.viewDidLoad()
  UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .None)
        // Do any additional setup after loading the view.
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        textSearch.layer.cornerRadius = 14.0
        textSearch.layer.borderWidth  = 2.0
        textSearch.layer.borderColor  = UIColor.whiteColor().CGColor
        
        btnCancel.layer.cornerRadius  = 12
        btnCancel.layer.borderWidth   = 2
        btnCancel.layer.borderColor   = UIColor.whiteColor().CGColor
        is_searching                  = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
//        if is_searching == true{
//            return searchingDataArray.count
//        }else{
            return arr_SearchResults.count  //Currently Giving default Value
        //}
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Custom Cell
        let cellIdentifier = "tagcell"
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        let lblTitle = cell.viewWithTag(20)as? UILabel
        
        if arr_SearchResults.objectAtIndex(indexPath.row) .valueForKey("user_name") as? String == "" {
            lblTitle?.text = arr_SearchResults.objectAtIndex(indexPath.row) .valueForKey("full_name") as? String
        }
        else
        {
        lblTitle?.text = arr_SearchResults.objectAtIndex(indexPath.row) .valueForKey("user_name") as? String
        }
        
        let image_user = cell.viewWithTag(10) as? UIImageView
        let imageUrl1 = NSURL(string: arr_SearchResults.objectAtIndex(indexPath.row)  .valueForKey("profile_image") as! String)
        
        image_user!.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "user-small"))
        
        let btnTagPeople = cell.viewWithTag(200) as? UIButton
        btnTagPeople?.hidden = false

        let str_Id = arr_SearchResults .objectAtIndex(indexPath.row) .valueForKey("id") as! String
        
        let userID_str = "\(String(str_Id))"
        
        let stringID = String(userID_str)
        
        let uuID = userDefaults .valueForKey("UserIdSave") as! String
        
        let uuID_str = "\(String(uuID))"
        
        let stringUUID = String(uuID_str)
        
        if arrayTagPeople.sharedInstance.count > 0 {
            if arrayTagPeople.sharedInstance .containsObject(arr_SearchResults .objectAtIndex(indexPath.row).valueForKey("id")!) {
                btnTagPeople?.hidden = false
                btnTagPeople! .setImage(UIImage (named: "check"), forState: UIControlState .Normal)
            }
            else if (stringID == stringUUID)
            {
                btnTagPeople?.hidden = true
            }
            else
            {
                btnTagPeople?.hidden = false
                btnTagPeople! .setImage(UIImage (named: "add-2"), forState: UIControlState .Normal)
            }
        }
        
       else if stringID == stringUUID {
            btnTagPeople?.hidden = true
        }
        else
        {
            btnTagPeople?.hidden = false
            btnTagPeople! .setImage(UIImage (named: "add-2"), forState: UIControlState .Normal)
        }
        
        btnTagPeople!.layer .setValue(indexPath.row, forKey: "tagPeopleIndex")
        btnTagPeople!.addTarget(self, action: #selector(TagPeopleVC.buttonTagPeople), forControlEvents: UIControlEvents.TouchUpInside)
    
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
            }
    func textField(textField: UITextField!, shouldChangeCharactersInRange range: NSRange, replacementString string: String!) -> Bool
    {
        if textField == self.textSearch {
            var txtAfterUpdate:NSString = self.textSearch.text! as NSString
            txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
            if txtAfterUpdate.length > 0 {
                self.tagPeopleAPI(txtAfterUpdate)
            }
            else{
                arr_SearchResults .removeAllObjects()
                tableTag .reloadData()
            }
        }
        return true
    }
    
    // MARK: - BUTTON TAG PEOPLE ACTION METHOD
    
    @IBAction func buttonTagPeople(sender:UIButton)
    {
        let btnTagPeople = sender
        let nsindexPathTag = btnTagPeople.layer .valueForKey("tagPeopleIndex") as! NSInteger
        
        if arrayTagPeople.sharedInstance.count > 0 {
            
            let str_Id = arr_SearchResults .objectAtIndex(nsindexPathTag) .valueForKey("id") as! String
            if arrayTagPeople.sharedInstance .containsObject( arr_SearchResults .objectAtIndex(nsindexPathTag).valueForKey("id")!)  {
                let str_TagName = arr_SearchResults .objectAtIndex(nsindexPathTag) .valueForKey("full_name") as! String
                let str_concate = String("\(String(str_TagName)) is removed from your tag list")
                
                arrayTagPeople.sharedInstance .removeObject(arr_SearchResults .objectAtIndex(nsindexPathTag).valueForKey("id")!)
                
                tableTag .reloadData()
                
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: str_concate as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else
            {
                arrayTagPeople.sharedInstance .addObject(str_Id)
                
                    let str_TagName = arr_SearchResults .objectAtIndex(nsindexPathTag) .valueForKey("full_name") as! String
                    
                    let str_concate = String("\(String(str_TagName)) is added to your tag list")
                    
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: str_concate as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        self.dismissViewControllerAnimated(false, completion: nil)
                        //self.navigationController?.popViewControllerAnimated(false)
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            
        }
        else
        {
            let str_Id = arr_SearchResults .objectAtIndex(nsindexPathTag) .valueForKey("id") as! String
            
            arrayTagPeople.sharedInstance .addObject(str_Id)
                
                let str_TagName = arr_SearchResults .objectAtIndex(nsindexPathTag) .valueForKey("full_name") as! String
                
                let str_concate = String("\(String(str_TagName)) is added to your tag list")
                
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: str_concate as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.dismissViewControllerAnimated(false, completion: nil)
                    //self.navigationController?.popViewControllerAnimated(false)
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        
}

    @IBAction func btnCancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
        //self.navigationController?.popViewControllerAnimated(false)
    }
    
    // MARK: - TAG PEOPLE API
    
    func tagPeopleAPI(str_TypeText: NSString)  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "search": str_TypeText, "count_result": 1]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                //print(params)
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KTagPeople)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - TAG PEOPLE API
    
    func getPeopleAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                print(params)
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KGetTagPeople)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    func serverResponse(responseDict: NSDictionary, serviceurl: NSString) {
        
        //print(responseDict)
        if serviceurl .isEqualToString(Header.KTagPeople)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_SearchResults .removeAllObjects()
                arr_SearchResults .addObjectsFromArray(responseDict .objectForKey("searchResults")! as AnyObject as! [AnyObject])
                tableTag .reloadData()
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if serviceurl .isEqualToString(Header.KGetTagPeople)
        {
        if responseDict .valueForKey("result")! .boolValue == true
        {
            arr_SearchResults .addObjectsFromArray(responseDict .objectForKey("searchResults")! as AnyObject as! [AnyObject])
                tableTag .reloadData()
                
                //print(arr_SearchResults)
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        AppManager.sharedManager.hideHUD()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }
    
}
