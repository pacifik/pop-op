//
//  Theme.swift
//  QScore
//
//  Created by Gurdev Singh on 1/25/15.
//  Copyright (c) 2015 Gurdev Singh. All rights reserved.
//

import UIKit

class Theme: NSObject {
    
    var startColor:UIColor
    var endColor:UIColor
    var themeLayer:CAGradientLayer
    
    var insights_top_color:UIColor
    var insights_bottom_color:UIColor

    var dashboard_top_color:UIColor
    var dashboard_bottom_color:UIColor

    var results_top_color:UIColor
    var results_bottom_color:UIColor

    var pha_top_color:UIColor
    var pha_bottom_color:UIColor

    var insightsLayer:CAGradientLayer = CAGradientLayer()
    var dashboardLayer:CAGradientLayer = CAGradientLayer()
    var resultsLayer:CAGradientLayer = CAGradientLayer()
    var phaLayer:CAGradientLayer = CAGradientLayer()
    
    override init() {
        startColor=UIColor.blackColor()
        endColor=UIColor.whiteColor()
        insights_top_color=UIColor.whiteColor()
        insights_bottom_color=UIColor.whiteColor()
        dashboard_top_color=UIColor.whiteColor()
        dashboard_bottom_color=UIColor.whiteColor()
        results_top_color=UIColor.whiteColor()
        results_bottom_color=UIColor.whiteColor()
        pha_top_color=UIColor.whiteColor()
        pha_bottom_color=UIColor.whiteColor()
        themeLayer = CAGradientLayer()
        insightsLayer = CAGradientLayer()
        dashboardLayer = CAGradientLayer()
        resultsLayer = CAGradientLayer()
        phaLayer = CAGradientLayer()
    }
    
    init(startColor:UIColor,endColor:UIColor) {
        themeLayer = CAGradientLayer()
        self.startColor = startColor
        self.endColor = endColor
        insights_top_color=UIColor.whiteColor()
        insights_bottom_color=UIColor.whiteColor()
        dashboard_top_color=UIColor.whiteColor()
        dashboard_bottom_color=UIColor.whiteColor()
        results_top_color=UIColor.whiteColor()
        results_bottom_color=UIColor.whiteColor()
        pha_top_color=UIColor.whiteColor()
        pha_bottom_color=UIColor.whiteColor()
        themeLayer = CAGradientLayer()
        
        insightsLayer = CAGradientLayer()
        insightsLayer.frame = UIScreen.mainScreen().bounds
        var colors=[insights_top_color.CGColor,insights_bottom_color.CGColor]
        insightsLayer.colors=colors
        
        dashboardLayer = CAGradientLayer()
        dashboardLayer.frame = UIScreen.mainScreen().bounds
        colors=[dashboard_top_color.CGColor,dashboard_bottom_color.CGColor]
        dashboardLayer.colors=colors
        
        resultsLayer = CAGradientLayer()
        resultsLayer.frame = UIScreen.mainScreen().bounds
        colors=[results_top_color.CGColor,results_bottom_color.CGColor]
        resultsLayer.colors=colors
        
        phaLayer = CAGradientLayer()
        phaLayer.frame = UIScreen.mainScreen().bounds
        colors=[pha_top_color.CGColor,pha_bottom_color.CGColor]
        phaLayer.colors=colors
    }
    
    func createLayers() {
        insightsLayer = CAGradientLayer()
        insightsLayer.frame = UIScreen.mainScreen().bounds
        var colors=[insights_top_color.CGColor,insights_bottom_color.CGColor]
        insightsLayer.colors=colors
        
        dashboardLayer = CAGradientLayer()
        dashboardLayer.frame = UIScreen.mainScreen().bounds
        colors=[dashboard_top_color.CGColor,dashboard_bottom_color.CGColor]
        dashboardLayer.colors=colors
        
        resultsLayer = CAGradientLayer()
        resultsLayer.frame = UIScreen.mainScreen().bounds
        colors=[results_top_color.CGColor,results_bottom_color.CGColor]
        resultsLayer.colors=colors
        
        phaLayer = CAGradientLayer()
        phaLayer.frame = UIScreen.mainScreen().bounds
        colors=[pha_top_color.CGColor,pha_bottom_color.CGColor]
        phaLayer.colors=colors
    }
    
    func GetTheme() -> Theme {
        themeLayer = CAGradientLayer()
        themeLayer.frame = UIScreen.mainScreen().bounds
        let colors=[startColor.CGColor,endColor.CGColor]
        themeLayer.colors=colors
        return self
    }
    
    
    
}
