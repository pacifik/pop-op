//
//  FriendListVC.swift
//  PopOn
//
//  Created by Nitin Suri on 30/08/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import SwiftLoader
import Social

class FriendListVC: UIViewController,WebServiceDelegate {
    @IBOutlet weak var tableFriends: UITableView!
    
    @IBOutlet weak var searchOutlet: UISearchBar!
    var searchActive : Bool = false
    
    var filtered:[String] = []
    
    var dictArray: NSMutableArray =  NSMutableArray()
    var dictArray_All = NSMutableArray()

    var arr_Following = NSMutableArray()
    
    var str_Follow    = "no"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        searchOutlet.layer.cornerRadius = 20
        
        self.getFBUserData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - MY POLLS API
    
    func UserprofileViewAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!, "view_user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KProfileView)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - FOLLOW USER API
    
    func followUserAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "follow_id": userDefaults.valueForKey("followID")!]
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KFollowFBuser)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - UNFOLLOW USER API
    
    func UnfollowUserAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "follow_id": userDefaults.valueForKey("followID")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KUnFollowFBuser)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KProfileView)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_Following .removeAllObjects()
                arr_Following .addObjectsFromArray(responseDict .objectForKey("profileData")! .objectForKey("following")! as AnyObject as! [AnyObject])
                
                //print(arr_Following)
                
                
                tableFriends.reloadData()
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if serviceurl .isEqualToString(Header.KFollowFBuser)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                
                userDefaults .setValue(responseDict .valueForKey("countFollowers"), forKey: "countFollowers")
                
                userDefaults .setValue(responseDict .valueForKey("countFollowing"), forKey: "countFollowing")
                
                NSNotificationCenter.defaultCenter().postNotificationName("countFollowing", object: nil)
                
                self.UserprofileViewAPI()
                
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if (serviceurl .isEqualToString(Header.KUnFollowFBuser))
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                userDefaults .setValue(responseDict .valueForKey("countFollowers"), forKey: "countFollowers")
                
                userDefaults .setValue(responseDict .valueForKey("countFollowing"), forKey: "countFollowing")
                
                NSNotificationCenter.defaultCenter().postNotificationName("countFollowing", object: nil)
                
                self.UserprofileViewAPI()
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        if dictArray.count != 0 {
            return dictArray.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Custom Cell
        let cellIdentifier = "friendsCell"
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        let lblTitle = cell.viewWithTag(20)as? UILabel
        lblTitle?.text = dictArray.objectAtIndex(indexPath.row) .valueForKey("name") as? String
        
        let image_user = cell.viewWithTag(10) as? UIImageView
        let imageUrl1 = NSURL(string: dictArray.objectAtIndex(indexPath.row)  .valueForKey("proflePic") as! String)
        
        image_user!.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "user-small"))
        
        let btn_Follow_Outlet = cell.viewWithTag(200) as? UIButton
        btn_Follow_Outlet?.hidden = false
        
        
        let id = dictArray.objectAtIndex(indexPath.row).valueForKey("id") as! String
        
        let predicate: NSPredicate      = NSPredicate(format: "SELF.social_id CONTAINS[c] %@",id)
        let filteredArray: NSArray      = self.arr_Following.filteredArrayUsingPredicate(predicate)
        
        if filteredArray.count > 0 {
            
            btn_Follow_Outlet! .setImage(UIImage (named: "check"), forState: UIControlState .Normal)
        }
        else
        {
            btn_Follow_Outlet! .setImage(UIImage (named: "add-2"), forState: UIControlState .Normal)
        }
        
        btn_Follow_Outlet!.layer .setValue(indexPath.row, forKey: "followingIndex")
        
        btn_Follow_Outlet!.addTarget(self, action: #selector(FriendListVC.buttonFollow_Following), forControlEvents: UIControlEvents.TouchUpInside)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
    }
    
    // MARK: - BUTTON FOLLOWING - FOLLOW ACTION METHOD
    
    @IBAction func buttonFollow_Following(sender:UIButton)
    {
        let btnFollowing = sender
        
        let nsindexPathFlng = btnFollowing.layer .valueForKey("followingIndex") as! NSInteger
        
        let id = dictArray.objectAtIndex(nsindexPathFlng).valueForKey("id") as! String
        
        let predicate: NSPredicate      = NSPredicate(format: "SELF.social_id CONTAINS[c] %@",id)
        let filteredArray: NSArray      = self.arr_Following.filteredArrayUsingPredicate(predicate)
        
        if filteredArray.count > 0 {
            
            let followID = (dictArray .objectAtIndex(nsindexPathFlng) .valueForKey("id"))! as AnyObject
            
            userDefaults .setValue(followID , forKey: "followID")
            
            self.actionSheetUnfollow()
        }
        else{
            
            let followID = (dictArray .objectAtIndex(nsindexPathFlng) .valueForKey("id"))! as AnyObject
            
            userDefaults .setValue(followID , forKey: "followID")
            
            self.followUserAPI()
        }
        
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            dictArray = self.dictArray_All
        }
        else{
            let predicate: NSPredicate      = NSPredicate(format: "SELF.name CONTAINS[c] %@",searchBar.text!)
            let filteredArray: NSArray      = self.dictArray_All.filteredArrayUsingPredicate(predicate)
            dictArray                 = filteredArray.mutableCopy() as! NSMutableArray
        }
          self.tableFriends.reloadData()
    }

   
   // MARK: BUTTON FOLLOW ACTION
    
    @IBAction func btnFollow(sender: AnyObject) {
        
    }
    
    // MARK: - BUTTON BACK ACTION
    
    @IBAction func btnBack(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func actionSheetUnfollow() -> Void {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Pop-Op", message: "Unfollow", preferredStyle: .ActionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        let unfollowActionButton: UIAlertAction = UIAlertAction(title: "Unfollow", style: .Default)
        { action -> Void in
            
            self.UnfollowUserAPI()
        }
        
        actionSheetControllerIOS8.addAction(unfollowActionButton)
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func getFBUserData()
    {
        if((FBSDKAccessToken.currentAccessToken()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,friends"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil)
                {
                    
                    if (result.valueForKey("friends")?.valueForKey("data")!.count==0)
                    {
                        
                        let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message:"No facebook friend registered with Pop-Op" , preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                            
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        alertViewShow.addAction(okAction)
                        self.presentViewController(alertViewShow, animated: true, completion: nil)
                            
                        }
                    else
                    {
                        //get facebook friends who use app
                        let keyExists = result.valueForKey("friends")  != nil
                        
                        if keyExists == true
                        {
                            let arrayCount =  result.valueForKey("friends")?.valueForKey("data")?.count
                            //for i in 0 ..< aCount
                            for var i = 0; i <= arrayCount!-1; i += 1 {
                                
                                let id = result.valueForKey("friends")?.valueForKey("data")?.objectAtIndex(i).valueForKey("id") as! String
                                
                                let name = result.valueForKey("friends")?.valueForKey("data")?.objectAtIndex(i).valueForKey("name") as! String
                                
                                var link =  "https://graph.facebook.com/"+id
                                
                                link = link+"/picture?type=large&width=200&height=200" as String
                                
                                let dict = ["name": name , "id": id , "proflePic":link ]
                                
                                self.dictArray.insertObject(dict, atIndex:i)
                                
                                self.dictArray_All = self.dictArray
                                
                        }
                            
                            self.tableFriends.reloadData()
                            self.UserprofileViewAPI()
                }
                        
                }
                }
                else
                {
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Facebook error" as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
            })
        }
        else
        {
            self.facebookLogin()
        }
    }
    
    func facebookLogin() -> Void {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        //"me?fields=id,name,friends"
        fbLoginManager.logInWithReadPermissions(["public_profile","email","user_friends"], fromViewController: self, handler: { (result, error) -> Void in
            //print("User Logged In")
            
            if ((error) != nil)
            {
                // Process error
            }
            else if result.isCancelled
            {
                // Handle cancellations
            }
            else
            {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if result.grantedPermissions.contains("email")
                {
                    //print(result);
                    self.getFBUserData()
                    fbLoginManager.logOut()
                    // Do work
                }
            }
        })
    }

}
