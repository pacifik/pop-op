//
//  UIColor.swift
//  PopOn
//
//  Created by Nitin Suri on 23/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import Foundation


extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func randomAlpha() -> UIColor {
        //let r = CGFloat.random()
        //let g = CGFloat.random()
        //let b = CGFloat.random()
        let a = CGFloat.random()
        
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red: 14/255.0, green: 22/255.0, blue: 42/255.0, alpha: a)
    }
    
    static func randomAlphaDemo() -> UIColor {
        //let r = CGFloat.random()
        //let g = CGFloat.random()
        //let b = CGFloat.random()
        let a = CGFloat.random()
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red: 16/255.0, green: 23/255.0, blue: 39/255.0, alpha: a)
    }
    
    static func randomCategories() -> UIColor
    {
        let r = CGFloat.random()
        let g = CGFloat.random()
        let b = CGFloat.random()
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}

// customised ARRAY for Tag People functinality used in both tag people and Create Poll class

struct arrayTagPeople {
    static var sharedInstance = NSMutableArray()
}


