//
//  AgeVC.swift
//  PopOn
//
//  Created by Nitin Suri on 02/06/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader

class AgeVC: UIViewController,SlideNavigationControllerDelegate,WebServiceDelegate {
    
    @IBOutlet weak var viewMaleProgress: TWRProgressView!
    
    @IBOutlet weak var viewFemaleProgress: TWRProgressView!
    @IBOutlet weak var btnResultOutlet: UIButton!
    @IBOutlet weak var btnGenderOutlet: UIButton!
    @IBOutlet weak var btnAgeOutlet: UIButton!
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    
    @IBOutlet weak var lblMaleVote: UILabel!
    @IBOutlet weak var lblFemaleVote: UILabel!
    @IBOutlet weak var lblPercent1: UILabel!
    @IBOutlet weak var lblPercent1Female: UILabel!
    @IBOutlet weak var lblMalePercent2: UILabel!
    @IBOutlet weak var lblFemalePercent2: UILabel!
    @IBOutlet weak var lblMaleVote2: UILabel!
    @IBOutlet weak var lblFemaleVote2: UILabel!
    @IBOutlet weak var btnRightSlider: UIButton!
    @IBOutlet weak var btnchoice1OUTLET: UIButton!
    @IBOutlet weak var btnChoice2Outlet: UIButton!
    
    var arr_GenderResult = NSMutableArray()
    
    @IBOutlet weak var viewMaleRedBg: TWRProgressView!
    
    @IBOutlet weak var viewFemaleRedBg: TWRProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        
        btnRightSlider.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.genderResultAPI()
        
        btnChoice2Outlet.layer.cornerRadius = 22
        btnChoice2Outlet.layer.borderWidth = 2
        btnChoice2Outlet.layer.borderColor = UIColor.init(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0).CGColor
        
        btnchoice1OUTLET.layer.cornerRadius = 22
        btnchoice1OUTLET.layer.borderWidth = 2
        btnchoice1OUTLET.layer.borderColor = UIColor.init(red: 235/255.0, green: 76/255.0, blue: 85/255.0, alpha: 1.0).CGColor
        
        
        viewMaleRedBg.hidden      = true
        viewMaleProgress.hidden   = true
        viewFemaleRedBg.hidden    = true
        viewFemaleProgress.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBackAction(sender: AnyObject) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(PollsVC) || controller.isKindOfClass(ProfileFollowerVC) || controller.isKindOfClass(MyProfileVC) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
   
    @IBAction func btnResultAction(sender: AnyObject) {
        
        let results_Gender = self.storyboard?.instantiateViewControllerWithIdentifier("GenderVC")as!
        GenderVC
        self.navigationController?.pushViewController(results_Gender, animated: false)
    }
    @IBAction func btnAgeAction(sender: AnyObject) {
        
        let results_Result = self.storyboard?.instantiateViewControllerWithIdentifier("ResultsVC")as!
        ResultsVC
        self.navigationController?.pushViewController(results_Result, animated: false)
    }
    @IBAction func btnChoice2Action(sender: AnyObject) {
    }
    
    @IBAction func btnchoice1Action(sender: AnyObject) {
    }
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    // MARK: - MY POLLS API
    
    func genderResultAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!, "poll_id": userDefaults .valueForKey("poll_id")!,"access_token": userDefaults .valueForKey("access_token")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                //print(params)
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KResultGender)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KResultGender)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
            
                viewMaleRedBg.hidden      = false
                viewMaleProgress.hidden   = false
                viewFemaleRedBg.hidden    = false
                viewFemaleProgress.hidden = false
            
            arr_GenderResult .removeAllObjects()
          arr_GenderResult .addObject(responseDict .objectForKey("resultbyGender")! )
                
                let emojiChoice2 = arr_GenderResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("answer")! as? String
                let dataEmojiChoice2: NSData = emojiChoice2!.dataUsingEncoding(NSUTF8StringEncoding)!
                let strEmojiHated3: String = String(data: dataEmojiChoice2, encoding: NSNonLossyASCIIStringEncoding)!
                
        btnChoice2Outlet .setTitle(strEmojiHated3, forState: UIControlState .Normal)
                
                let malevotesCount1 = arr_GenderResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("maleVotes")! as! NSInteger
                
                let mVotes1 = "\(String(malevotesCount1)) Votes"
                
                lblMaleVote2.text = String(mVotes1)
                
                let MaleVotesPercent1 = arr_GenderResult .objectAtIndex(0) .valueForKey("Male")! .valueForKey("Choice1percentage")! as! NSInteger
                
                //let maleP1Float = CGFloat(MaleVotesPercent1)
                //print(maleP1Float)
                
                let mVotePecr1 = "\(String(MaleVotesPercent1))%"
                
                lblMalePercent2.text = String(mVotePecr1)
                
                let maleVotes2 = arr_GenderResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("maleVotes")! as! NSInteger
                
                let mVote2 = "\(String(maleVotes2)) Votes"
                
                lblMaleVote.text = String(mVote2)
                
                let maleVotesPercent2 = arr_GenderResult .objectAtIndex(0) .valueForKey("Male")! .valueForKey("Choice2percentage")! as! NSInteger
                
                let mVotePercent = "\(String(maleVotesPercent2))%"
                
                lblPercent1.text = String(mVotePercent)
                
                let emojiChoice1 = arr_GenderResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("answer")! as? String
                let dataEmojiChoice1: NSData = emojiChoice1!.dataUsingEncoding(NSUTF8StringEncoding)!
                let strEmojiHated1: String = String(data: dataEmojiChoice1, encoding: NSNonLossyASCIIStringEncoding)!
                btnchoice1OUTLET .setTitle(strEmojiHated1, forState: UIControlState .Normal)
                
                let femaleVotes2 = arr_GenderResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("femaleVotes")! as! NSInteger
                
                let fVote2 = "\(String(femaleVotes2)) Votes"
                
                lblFemaleVote2.text = String(fVote2)
                
                let femalePercent2 = arr_GenderResult .objectAtIndex(0) .valueForKey("Female")! .valueForKey("Choice1percentage")! as! NSInteger
                
                let fPer2 = "\(String(femalePercent2))%"
                
                lblFemalePercent2.text = String(fPer2)
                
                //choice2 green
                
                let femaleVote = arr_GenderResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("femaleVotes")! as! NSInteger
                
                let str_votesF2 = "\(String(femaleVote)) Votes"
                
                lblFemaleVote.text = String(str_votesF2)
                
                let femalePercent = arr_GenderResult .objectAtIndex(0) .valueForKey("Female")! .valueForKey("Choice2percentage")! as! NSInteger
                
                let str_per = "\(String(femalePercent))%"
                
                lblPercent1Female.text = str_per
                
                //.......MALE FEMALE PROGRESS ANIMATION..................
                
                let imageMaleFade = UIImage(named: "boy")
                viewMaleProgress.maskingImage = imageMaleFade
                
                viewMaleProgress.setFrontColor(UIColor.init(red: 235/255.0, green: 76/255.0, blue: 85/255.0, alpha: 1.0))
                
                viewMaleProgress.setBackColor(UIColor.clearColor())
                
                var  startingMaleFloat = CGFloat()
                
                let maleFloat1 = CGFloat(MaleVotesPercent1)
                startingMaleFloat = maleFloat1/100
                
                var valueMaleFloat = CGFloat()
                
                valueMaleFloat = startingMaleFloat
                
                viewMaleProgress.setAnimationTime(valueMaleFloat * 2.0)
                viewMaleProgress.setProgress(startingMaleFloat, animated: true)
                viewMaleProgress.backgroundColor = UIColor.clearColor()
                
                //................................................................
                
                let imageMaleFade2 = UIImage(named: "boy")
                viewMaleRedBg.maskingImage = imageMaleFade2
                
                viewMaleRedBg.setFrontColor(UIColor.init(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0))
                
                viewMaleRedBg.setBackColor(UIColor.clearColor())
                
                var  startingMaleFloat2 = CGFloat()
                
                let maleFloat2 = CGFloat(maleVotesPercent2) + CGFloat(MaleVotesPercent1)
                
                startingMaleFloat2 = maleFloat2/100
                
                var  valueFloatMale2 = CGFloat()
                
                valueFloatMale2 = startingMaleFloat
                
                viewMaleRedBg.setAnimationTime(valueFloatMale2 * 2.0)
                
                viewMaleRedBg.setProgress(startingMaleFloat2, animated: true)
                viewMaleRedBg.backgroundColor = UIColor.clearColor()
                
                //...................................................................
                
                let imageFemaleFade2 = UIImage(named: "girl")
                viewFemaleRedBg.maskingImage = imageFemaleFade2
                
                viewFemaleRedBg.setFrontColor(UIColor.init(red: 235/255.0, green: 76/255.0, blue: 85/255.0, alpha: 1.0))
                
                viewFemaleRedBg.setBackColor(UIColor.clearColor())
                
                var startingFeMaleFloat2 = CGFloat()
                
                let femaleFloat1 = CGFloat(femalePercent2)
                
                startingFeMaleFloat2 = femaleFloat1/100
                
                var valueFloatFemale2 = CGFloat()
                
                valueFloatFemale2 = startingFeMaleFloat2
                
                viewFemaleRedBg.setAnimationTime(valueFloatFemale2 * 2.0)
                viewFemaleRedBg.setProgress(startingFeMaleFloat2, animated: true)
                
                //...................................................................

                
                let imageFemaleFade = UIImage(named: "girl")
                viewFemaleProgress.maskingImage = imageFemaleFade
                
                viewFemaleProgress.setFrontColor(UIColor.init(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0))
                
                viewFemaleProgress.setBackColor(UIColor.clearColor())
                
                var startingFeMaleFloat = CGFloat()
                
                let femaleFloat2 = CGFloat(femalePercent) + CGFloat(femalePercent2)
                
                startingFeMaleFloat = femaleFloat2/100
                
                var valueFloatFemale = CGFloat()
                
                valueFloatFemale = startingFeMaleFloat
                
                viewFemaleRedBg.setAnimationTime(valueFloatFemale * 2.0)
                
                viewFemaleProgress.setProgress(startingFeMaleFloat, animated: true)
                
                //................................................................
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        if serviceurl .isEqualToString(Header.KFollowUser)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
}

    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }

}