//
//  UserProfileVC.swift
//  PopOn
//
//  Created by Nitin Suri on 27/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader

class UserProfileVC: UIViewController,SlideNavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,WebServiceDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate {
    
    @IBOutlet weak var btnSaveOutlet: UIButton!
    @IBOutlet weak var textEmail:     UITextField!
    @IBOutlet weak var textPhone:     UITextField!
    @IBOutlet weak var textState:     UITextField!
    @IBOutlet weak var textCity:      UITextField!
    @IBOutlet weak var btnToggle:     UIButton!
    @IBOutlet weak var imageUserPic:  UIImageView!
    @IBOutlet weak var lblUserName:   UILabel!
    @IBOutlet weak var textUserName:  UITextField!
    @IBOutlet weak var textGender:    UITextField!
    @IBOutlet weak var textDOB:       UITextField!
    
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var scrollProfileUser: UIScrollView!
    var arr_Gender = NSArray()
    var strBase64 = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBarHidden = true
        
        
        btnToggle.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        let pickerView = UIDatePicker()
        
        textDOB.inputView = pickerView
        
        pickerView.datePickerMode = UIDatePickerMode.Date
        
        pickerView.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Year, value: -16, toDate: NSDate(), options: [])
        
        pickerView.setDate(NSCalendar.currentCalendar().dateByAddingUnit(.Year, value: -16, toDate: NSDate(), options: [])!, animated: true)

        pickerView.addTarget(self, action: #selector(UserProfileVC.datePickeraction), forControlEvents: UIControlEvents .ValueChanged)
        
        let pickerView2 = UIPickerView()
        
        pickerView2.delegate = self
        
        textGender.inputView = pickerView2
        
        arr_Gender = ["male","female"]
        
        btnBackOutlet.hidden = true
        
        if userDefaults .valueForKey("login1") as! String != "yes" {
            btnToggle.hidden     = true
            userDefaults .setValue("yes", forKey: "catSkip")
        }
        else
        {
            btnToggle.hidden     = false
            userDefaults .setValue("no", forKey: "catSkip")
        }
        
        if userDefaults .valueForKey("pushSettings") as? String != nil {
            
            if userDefaults .valueForKey("pushSettings") as! String == "yes" {
                
                btnBackOutlet.hidden = false
            }
            else
            {
                btnBackOutlet.hidden = true
            }
            userDefaults .removeObjectForKey("pushSettings")
        }
        
        if (userDefaults .valueForKey("profile_image") as? String != nil) {
            let imageUrl = NSURL(string: (userDefaults .valueForKey("profile_image") as? String)! )
            imageUserPic.sd_setImageWithURL(imageUrl!, placeholderImage: UIImage (named: "user-lrg"))
        }
        else
        {
            imageUserPic.image = UIImage (named: "user-lrg")
        }
        
        //print(userDefaults .valueForKey("user_full_name") as? String)
        
        textUserName.text = userDefaults .valueForKey("user_full_name") as? String
        
        if userDefaults .valueForKey("user_city") as? String != nil {
            
            textCity.text = userDefaults .valueForKey("user_city") as? String
        }
        
        if userDefaults .valueForKey("state") as? String != nil {
            
            textState.text = userDefaults .valueForKey("state") as? String
        }
        
        if userDefaults .valueForKey("phone") as? String != nil {
            
            textPhone.text = userDefaults .valueForKey("phone") as? String
        }
        
        if userDefaults .valueForKey("dob") as? String != nil {
            
            if userDefaults .valueForKey("dob") as? String == "0000-00-00" {
                
                textDOB.text = ""
            }
            else
            {
            textDOB.text = userDefaults .valueForKey("dob") as? String
            }
        }
        
        pickerView2.selectRow(0, inComponent: 0, animated: true)
        
        if userDefaults .valueForKey("gender") as? String != nil {
            
            if userDefaults .valueForKey("gender") as? String == "male" {
                
                pickerView2.selectRow(0, inComponent: 0, animated: true)
            }
            else
            {
               pickerView2.selectRow(1, inComponent: 0, animated: true)
            }
            
            textGender.text = userDefaults .valueForKey("gender") as? String
        }
        
        textEmail.text = userDefaults .valueForKey("email") as? String
        
        // SETTING CORNER RADIUS OF UIBUTTON
        
        btnSaveOutlet.layer.cornerRadius = 28
        btnSaveOutlet.layer.borderWidth = 2
        btnSaveOutlet.layer.borderColor = UIColor.init(red: 0/255.0, green: 155/255.0, blue: 251/255.0, alpha: 1.0).CGColor
        
        // IMAGE TAP METHOD TO UPLOAD USER PROFILE PIC
        
        imageUserPic.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpVC.imageTappedMethod(_:)))
        //Add the recognizer to your view.
        imageUserPic.addGestureRecognizer(tapRecognizer)
    }
    
    func datePickeraction(datePicker:UIDatePicker)
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd yyyy"
        
        let strDate = dateFormatter.stringFromDate(datePicker.date)
        textDOB.text = strDate
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPHONE5 {
            
            scrollProfileUser.contentSize = CGSizeMake(self.view.frame.width, 430)
        }
    }
    
    // MARK: - UIALERT CONTROLLER FOR USER PROFILE IMAGE
    
    func imageTappedMethod(gestureRecognizer: UITapGestureRecognizer) {
        _ = gestureRecognizer.view!
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Pop-Op", message: "Profile Image", preferredStyle: .ActionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Library", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
        
    }
    
    // MARK: - IMAGE PIKCER CONTROLLER DELEGATE
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        imageUserPic.image = image
        self.dismissViewControllerAnimated(true, completion: nil);
        
        let imageConv : UIImage = image
        //Now use image to create into NSData format
        
        let imageData:NSData = UIImageJPEGRepresentation(imageConv,0.1)!
        
        strBase64 = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: BACK BUTTON ACTION
    
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    // MARK: - PICKER VIEW DELEGATE AND DATASOURCE
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_Gender.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        textGender.text = arr_Gender .objectAtIndex(row) as? String
        
        return arr_Gender .objectAtIndex(row) as? String
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textGender.text = arr_Gender .objectAtIndex(row) as? String
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var newLength = 0
        if textField == textPhone {
            newLength = textPhone.text!.characters.count + string.characters.count - range.length
        }
        
        return newLength <= 10
    }
    
    // MARK: - BUTTON SAVE ACTION
    
    @IBAction func btnSaveAction(sender: AnyObject)
    {
        
//        let trimCityString = textCity.text!.stringByTrimmingCharactersInSet(
//            NSCharacterSet.whitespaceAndNewlineCharacterSet());
//        
//        let trimStateString=textState.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimFullNameString=textUserName.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimPhoneString=textPhone.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimEmailString=textEmail.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let DOBString=textDOB.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let genderString=textGender.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        
//        if(trimCityString.characters.count==0)
//        {
//            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter your city." as String, preferredStyle: UIAlertControllerStyle.Alert)
//            
//            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
//                UIAlertAction in
//            }
//            alertViewShow.addAction(okAction)
//            self.presentViewController(alertViewShow, animated: true, completion: nil)
//            
//        }
//        else if(trimStateString.characters.count==0)
//        {
//            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter your state." as String, preferredStyle: UIAlertControllerStyle.Alert)
//            
//            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
//                UIAlertAction in
//            }
//            alertViewShow.addAction(okAction)
//            self.presentViewController(alertViewShow, animated: true, completion: nil)
//        }
        if (trimFullNameString.characters.count==0) {
            
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter full name." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
       else if(trimPhoneString.characters.count==0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter your phone number." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if(trimEmailString.characters.count==0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter the email." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if(DOBString.characters.count==0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter your date of birth." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if (genderString.characters.count == 0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter your gender." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            if !AppManager.sharedManager.isValidEmail(trimEmailString)
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "E-mail is invalid." as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else
            {
                if AppManager.NetWorkReachability.isConnectedToNetwork() == true
                {
                    AppManager.sharedManager.delegate = self
                    SwiftLoader.show(animated: true)
                    
                    let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!, "access_token": userDefaults .valueForKey("access_token")!, "dob":  textDOB.text!, "city": textCity.text!, "state": textState.text!, "phone": textPhone.text!, "gender": textGender.text!, "full_name": textUserName.text!, "profile_image": strBase64]
                    
                    //print(dict_User_Info)
                    do
                    {
                        let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                        //print(jsonString)
                        
                        let params: [String: AnyObject] = ["user_details":jsonString]
                        
                       //print(params)
                        
                        AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KEditProfile)
                    }
                }
                else
                {
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
            }
            
        }
        
    }
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KEditProfile)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                userDefaults .setValue(textCity.text, forKey: "user_city")
                userDefaults .setValue(textState.text, forKey: "state")
                userDefaults .setValue(textPhone.text, forKey: "phone")
                userDefaults .setValue(textDOB.text, forKey: "dob")
                userDefaults .setValue(textGender.text, forKey: "gender")
                userDefaults .setValue(textUserName.text, forKey: "user_full_name")
                
                textCity.text     = userDefaults .valueForKey("user_city") as? String
               
                textState.text    = userDefaults .valueForKey("state") as? String
                
                textPhone.text    = userDefaults .valueForKey("phone") as? String
                
                textUserName.text = userDefaults .valueForKey("user_full_name") as? String
                
                userDefaults .setValue(textCity.text, forKey: "user_city")
                userDefaults .setValue(textState.text, forKey: "state")
                userDefaults .setValue(textPhone.text, forKey: "phone")
                userDefaults .setValue(textDOB.text, forKey: "dob")
                userDefaults .setValue(textGender.text, forKey: "gender")
                userDefaults .setValue(textUserName.text, forKey: "user_full_name")
                
                if responseDict .objectForKey("user_details")! .valueForKey("profile_image") as? String != nil {
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("profile_image"), forKey: "profile_image")
                    
                NSNotificationCenter.defaultCenter().postNotificationName("imageUpdated", object: nil)
                }
                NSNotificationCenter.defaultCenter().postNotificationName("userDetailsUpdated", object: nil)
                
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    
                    //if userDefaults .valueForKey("login1") as! String == "yes" {
                        userDefaults .setValue("yes", forKey: "login1")
                        let CategoriesVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("CategoriesVC")as! CategoriesVC
                        self.navigationController?.pushViewController(CategoriesVCObj, animated: true)
//                    }
//                    else
//                    {
                    
                        //self.navigationController?.popViewControllerAnimated(true)
                    //}
                    
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
                
                //userDefaults .setValue("yes", forKey: "questionPollPush")
                
            }
                
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
    }
    
    func failureRsponseError(failureError:NSError)
    {
        //print(failureError)
       SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }

}

