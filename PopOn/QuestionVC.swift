//
//  QuestionVC.swift
//  PopOn
//
//  Created by Nitin Suri on 20/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader
import ZLSwipeableViewSwift
import Contacts


class QuestionVC: UIViewController,SlideNavigationControllerDelegate,WebServiceDelegate {
    
    @IBOutlet weak var viewSwipeLeftRight: UIView!
    @IBOutlet weak var viewSwipeAnimate: UIView!
    @IBOutlet weak var imgMultiPollPic1: UIImageView!
    @IBOutlet weak var imgMultiPollPic2: UIImageView!
    @IBOutlet weak var lblVotes: UILabel!
    
    @IBOutlet weak var imgSinglePollPic: UIImageView!
    
    @IBOutlet weak var img_UserPic: UIImageView!
    @IBOutlet weak var label_FeedCopyFinal: UILabel!
    @IBOutlet weak var btnRightMenuOutlet: UIButton!
    @IBOutlet weak var btnFeedOutlet: UIButton!
    @IBOutlet weak var btnTrendingOutlet: UIButton!
    //@IBOutlet weak var viewBGHated: UIView!
    @IBOutlet weak var imgPolluserProfilePic: UIImageView!
    @IBOutlet weak var viewBGLoved: UIView!
    @IBOutlet weak var btnHatedLeftOutlet: UIButton!
    @IBOutlet weak var btnLovedRightOutlet: UIButton!
    @IBOutlet weak var lblFeed: UILabel!
    @IBOutlet weak var lblTrending: UILabel!
    @IBOutlet weak var lblFeedCount: UIButton!
    @IBOutlet weak var viewSwipe: UIView!
    //@IBOutlet weak var zlSwipeableview_obj: ZLSwipeableView!
    
    var loadCardsFromXib = true
    
    @IBOutlet weak var lblVoteText: UILabel!
    @IBOutlet weak var scrollSwipe: UIScrollView!
    @IBOutlet weak var viewDemo: UIView!
    @IBOutlet weak var lblPollUsername: UILabel!
    @IBOutlet weak var lblPollUserLocation: UILabel!
    
    @IBOutlet weak var lblLoved: UILabel!
    @IBOutlet weak var lblHated: UILabel!
    @IBOutlet weak var btnFollowUser: UIButton!
    
    @IBOutlet weak var lblAllDone: UILabel!
    @IBOutlet weak var imageAllDone: UIImageView!
    
    internal var numberOfActiveView = Int(0)
    internal var nextView: (() -> UIView?)?
    internal var previousView: (() -> UIView?)?
    
    var wrap = Bool()
    
    var arr_FeedPolls       = NSMutableArray()
    var arr_TrendingPolls   = NSMutableArray()
    
    var poll_Feed_Count     = 0
    var poll_Trending_Count = 0
    var feedCountInt        = 0
    
    var str_Poll_Answer     = String()
    var str_FollowFeed      = String()
    var str_FollowTrending  = String()
    
    
    var str_Loved_True      = Bool()
    var checkBooleanVal     = Bool()
    
    let viewDislike         = UIView()
    let viewLike            = UIView()
    
    var locationPrevious    = CGPoint()
    var startLocation       = CGPoint()
    
    var value               = NSInteger()
    
    let imageDislike        = UIImageView()
    let imagelike           = UIImageView()
    
    let lblLikeText         = UILabel()
    let lblDislikeText      = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        label_FeedCopyFinal.text = String("0")
        viewDemo.hidden = true
        imageAllDone.hidden = true
        lblAllDone.text = ""
        
        value = 0
        
        viewDislike.frame = CGRectMake(-CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
        
        viewLike.frame    = CGRectMake(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)

        
        imageDislike.frame = CGRectMake(0,0, CGRectGetWidth(self.view.frame), 86)
        imageDislike.image = UIImage (named: "red-arrow")
        
        imagelike.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 86)
        imagelike.image = UIImage (named: "green-arrow")
        
        lblLikeText.frame = CGRectMake(CGRectGetWidth(self.view.frame) - 130, 15, 110, 60)
        lblLikeText.textColor = UIColor.blackColor()
        lblLikeText.numberOfLines = 2
        lblLikeText.textAlignment = NSTextAlignment.Right
        lblLikeText.font = UIFont (name: "ProximaNovaSoft-Regular", size: 18)
        
        lblDislikeText.frame = CGRectMake(20, 15, 110, 60)
        lblDislikeText.textColor = UIColor.blackColor()
        lblDislikeText.numberOfLines = 2
        lblDislikeText.textAlignment = NSTextAlignment.Left
        lblDislikeText.font = UIFont (name: "ProximaNovaSoft-Regular", size: 18)
        
        self.view.addSubview(viewLike)
        self.view.addSubview(viewDislike)
        
        viewLike.addSubview(imagelike)
        viewDislike.addSubview(imageDislike)
        
        viewLike.addSubview(lblLikeText)
        viewDislike.addSubview(lblDislikeText)
        
        let view = self.view.viewWithTag(111)
        
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panedView(_:)))
        view!.addGestureRecognizer(panRecognizer)
        
        lblFeedCount.layer.cornerRadius = 10
        //lblFeedCount.backgroundColor    = UIColor.darkGrayColor()
        btnTrendingOutlet.selected      = false
        btnFeedOutlet.selected          = true
        
        lblFeedCount.backgroundColor = UIColor.init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0)
        
        btnRightMenuOutlet.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        slidecontroller.enableSwipeGesture = false
        
        btnFeedOutlet.selected = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QuestionVC.updatedPolls), name: "updatedPolls", object: nil)
        
        self.getPollsAPImethod()
        
        if userDefaults .valueForKey("login1") as! String != "yes" {
            let UserProfileVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("UserProfileVC")as! UserProfileVC
            self.navigationController?.pushViewController(UserProfileVCObj, animated: true)
        }
       
    }
    
    func updatedPolls() -> Void {
        
        self.getPollsAPImethod()
    }

    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBarHidden = true
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resetOrigionalFrame() {
        
        viewLike.backgroundColor    = UIColor.clearColor()
        viewDislike.backgroundColor = UIColor.clearColor()
        
        viewDislike.frame = CGRectMake(-CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
        
        viewLike.frame    = CGRectMake(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
        
        if(value == 1){
            
        self.btnHatedAction()
            
        }else if(value == 2){
            
        self.btnLovedAction()
            
        }
    }
    
    func panedView(sender:UIPanGestureRecognizer){
        
        if (sender.state == UIGestureRecognizerState.Began) {
            
            startLocation = sender.locationInView(self.view)
            
            if((startLocation.x <= CGRectGetMidX(self.view.frame))){
               // print("Dislike Begins Slide")
                value = 1
            }else if((startLocation.x > CGRectGetMidX(self.view.frame))){
               // print("Like Begins Slide")
                value = 2
            }
            
        }
        else if (sender.state == UIGestureRecognizerState.Changed) {
            
            let stopLocation = sender.locationInView(self.view)
            let dx = stopLocation.x - startLocation.x
            let dy = stopLocation.y - startLocation.y
            let distance = sqrt(dx*dx + dy*dy )
            NSLog("Distance: %f", distance)
            
            self.view.bringSubviewToFront(viewDislike)
            
            if(value == 1){
                
                var frame           = CGRect()
                frame               = viewDislike.frame
                if(CGRectGetMaxX(frame) <= CGRectGetMaxX(self.view.frame)){
                    
                    if stopLocation.x > startLocation.x{
                        frame.origin.x  += distance * 1.5
                    }
                    else{
                        frame.origin.x  -= distance * 1.5
                    }
                    
                }else{
                    frame = CGRectMake(0, CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
                }
                viewDislike.frame   = frame
                
            }else if(value == 2){
                
                self.view.bringSubviewToFront(viewLike)
                
                var frame           = CGRect()
                frame               = viewLike.frame
                
                if(CGRectGetMinX(frame) >= CGRectGetMinX(self.view.frame)){
                    
                    if(stopLocation.x < startLocation.x){
                        frame.origin.x  -= distance  * 1.5
                    }
                    else{
                        frame.origin.x  += distance  * 1.5
                    }
                    
                }else{
                    frame = CGRectMake(0, CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
                }
                
                viewLike.frame      = frame
            }
            startLocation = sender.locationInView(self.view);
        }
        else if (sender.state == UIGestureRecognizerState.Ended) {
            
            if(value == 1){
                
                if(CGRectGetMaxX(viewDislike.frame) < CGRectGetMidX(self.view.frame)/3){
                    viewDislike.frame = CGRectMake(-CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
                }else{
                    
                    UIView.animateWithDuration(0.5, animations: {
                        
                       self.viewDislike.frame = CGRectMake(0, CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
                        }, completion: { (value : Bool) in
                            self.viewDislike.backgroundColor = UIColor.init(red: 255/255.0, green: 102/255.0, blue: 102/255.0, alpha: 1.0)
                    })
                    self.performSelector(#selector(resetOrigionalFrame), withObject: nil, afterDelay: 1.0);
                }
            }
            else if(value == 2){
                
                if(CGRectGetMinX(viewLike.frame) > CGRectGetMidX(self.view.frame) * 1.5){
                    viewLike.frame    = CGRectMake(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
                    
                }else{
                    
                    UIView.animateWithDuration(0.5, animations: {
                        
                        self.viewLike.frame    = CGRectMake(0, CGRectGetHeight(self.view.frame)-86, CGRectGetWidth(self.view.frame), 86)
                        }, completion: { (value : Bool) in
                            self.viewLike.backgroundColor = UIColor.init(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0)

                    })
                    self.performSelector(#selector(resetOrigionalFrame), withObject: nil, afterDelay: 1.0);
                }
            }
        }
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: - BUTTON ADD POLE ACTION
    
    @IBAction func btnAddPole(sender: AnyObject) {
        
        let addPoleObj = self.storyboard?.instantiateViewControllerWithIdentifier("CreatePollVC")as! CreatePollVC
        
        userDefaults .setValue("yes", forKey: "pushquestionscreen")
        
        self.presentViewController(addPoleObj, animated: true, completion: nil)
    }
    
    // MARK: - BUTTON RIGHT MENU SLIDER ACTION
    
    @IBAction func btnRightMenuSlider(sender: AnyObject) {
        
    }
    
    // MARK: - BUTTON FEED ACTION
    
    @IBAction func buttonFeed(sender: AnyObject) {
        
        if arr_FeedPolls.count == 0
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "No Feed Polls found" as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            
            lblFeed.textColor     = UIColor.whiteColor()
            
            lblTrending.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
            
            lblFeedCount.backgroundColor = UIColor.init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0)
            
            btnFeedOutlet.selected     = true
            btnTrendingOutlet.selected = false
            
            if arr_FeedPolls.count == 0 && arr_TrendingPolls.count == 0 {
                
                viewDemo.hidden = true
            }
            else
            {
                if arr_FeedPolls.count > 0 {
                    
                    if poll_Feed_Count > arr_FeedPolls.count && poll_Trending_Count > arr_TrendingPolls.count {
                        
                        viewDemo.hidden = true
                    }
                    else
                    {
                        let transition: CATransition = CATransition()
                        transition.duration = 0.5
                        transition.type = kCATransitionPush
                        transition.subtype = kCATransitionFromLeft
                        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        viewDemo.layer.addAnimation(transition, forKey: nil)
                        
                        label_FeedCopyFinal.text = String("0")
                        
                        let intFeedCount = arr_FeedPolls.count
                        
                        label_FeedCopyFinal.text = String(intFeedCount)
                        
                        let emojiRetrieveQuestFeed = arr_FeedPolls .valueForKey("question") .objectAtIndex(poll_Feed_Count) as? String
                        
                        let dataQuestionEmojiFeed: NSData = emojiRetrieveQuestFeed!.dataUsingEncoding(NSUTF8StringEncoding)!
                        let strEmojiQuestionFeed: String = String(data: dataQuestionEmojiFeed, encoding: NSNonLossyASCIIStringEncoding)!
                        
                        lblVoteText.text = strEmojiQuestionFeed
                        
                        if ((arr_FeedPolls .valueForKey("poll_user_name") .objectAtIndex(poll_Feed_Count) as? String) == "") {
                            lblPollUsername.text = arr_FeedPolls .valueForKey("poll_full_name") .objectAtIndex(poll_Feed_Count) as? String
                        }
                        else
                        {
                            lblPollUsername.text = arr_FeedPolls .valueForKey("poll_user_name") .objectAtIndex(poll_Feed_Count) as? String
                        }
                        
                        if (arr_FeedPolls .valueForKey("following") .objectAtIndex(0) as? String) == "yes" {
                            
                            btnFollowUser.setImage(UIImage (named: "check"), forState: UIControlState.Normal)
                        }
                        else
                        {
                            btnFollowUser.setImage(UIImage (named: "add-2"), forState: UIControlState.Normal)
                        }
                        
                        if (((arr_FeedPolls .valueForKey("poll_user_id") .objectAtIndex(0) as? String)) == userDefaults .valueForKey("UserIdSave")! as? String) {
                            
                            btnFollowUser.hidden = true
                        }
                        else
                        {
                            btnFollowUser.hidden = false
                        }
                        
                        if (arr_FeedPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! != ""  {
                            
                            let imageUrlUser = NSURL(string: (arr_FeedPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! )
                            img_UserPic.sd_setImageWithURL(imageUrlUser!, placeholderImage: nil)
                        }
                        
                        let votesCount = arr_FeedPolls .valueForKey("Votes") .objectAtIndex(0) as! NSInteger
                        
                        lblVotes.text = String(votesCount)
                        
                        let emojiRetrieveHated2 = arr_FeedPolls .valueForKey("choice1") .objectAtIndex(0) as? String
                        let dataHatedEmoji2: NSData = emojiRetrieveHated2!.dataUsingEncoding(NSUTF8StringEncoding)!
                        let strEmojiHated2: String = String(data: dataHatedEmoji2, encoding: NSNonLossyASCIIStringEncoding)!
                        
                        lblHated.text = strEmojiHated2
                        
                        let emojiRetrieveLoved2 = arr_FeedPolls .valueForKey("choice2") .objectAtIndex(0) as? String
                        let dataLovedEmoji2: NSData = emojiRetrieveLoved2!.dataUsingEncoding(NSUTF8StringEncoding)!
                        let strEmojiLoved2: String = String(data: dataLovedEmoji2, encoding: NSNonLossyASCIIStringEncoding)!
                        
                        lblLoved.text       = strEmojiLoved2
                        
                        lblDislikeText.text = strEmojiHated2
                        
                        lblLikeText.text    = strEmojiLoved2
                        
                        let str_City = arr_FeedPolls .valueForKey("poll_user_city") .objectAtIndex(poll_Feed_Count) as? String
                        
                        let str_State = arr_FeedPolls .valueForKey("poll_user_state") .objectAtIndex(poll_Feed_Count) as? String
                        
                        
                        if (str_City == "" && str_State == "") {
                            
                            lblPollUserLocation!.text = ""
                        }
                        else
                        {
                            lblPollUserLocation!.text = str_City! + ", " + str_State!
                        }
                        
                        if ((arr_FeedPolls .valueForKey("poll_type") .objectAtIndex(poll_Feed_Count) as? String) == "multi") {
                            
                            imgSinglePollPic.hidden = true
                            
                            imgMultiPollPic1.image = UIImage (named: "")
                            
                            imgMultiPollPic2.image = UIImage (named: "")
                            
                            let imageUrl = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(poll_Feed_Count) .objectAtIndex(0) as? String)! )
                            imgMultiPollPic1.sd_setImageWithURL(imageUrl!, placeholderImage: UIImage (named: "no-imge"))
                            
                            let imageUrl1 = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(poll_Feed_Count)  .objectAtIndex(1) as? String)! )
                            imgMultiPollPic2.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "no-imge"))
                        }
                        else
                        {
                            imgSinglePollPic.hidden = false
                            imgSinglePollPic.image = UIImage (named: "")
                            
                            let imageUrl2 = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(poll_Feed_Count)  .objectAtIndex(0) as? String)! )
                            imgSinglePollPic.sd_setImageWithURL(imageUrl2!, placeholderImage: UIImage (named: "no-imge"))
                        }
                    }
                }
            }
        }
        
    }
    
    // MARK: - BUTTON TRENDING ACTION
    
    @IBAction func buttonTrending(sender: AnyObject) {
        
        if arr_TrendingPolls.count == 0
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "No Trending Polls found" as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            lblTrending.textColor = UIColor.whiteColor()
            lblFeedCount.backgroundColor = UIColor.init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0)
            lblFeed.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
            
            btnTrendingOutlet.selected = true
            btnFeedOutlet.selected     = false
            
            if arr_FeedPolls.count == 0 && arr_TrendingPolls.count == 0 {
                viewDemo.hidden = true
            }
            else
            {
                if arr_TrendingPolls.count > 0 {
                    
                    if poll_Trending_Count > arr_TrendingPolls.count && poll_Feed_Count > arr_TrendingPolls.count {
                        viewDemo.hidden = true
                    }
                    else
                    {
                        let transition: CATransition = CATransition()
                        transition.duration = 0.5
                        transition.type = kCATransitionPush
                        transition.subtype = kCATransitionFromRight
                        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        viewDemo.layer.addAnimation(transition, forKey: nil)
                        
                        let emojiRetrieveQuestTrend = arr_TrendingPolls .valueForKey("question") .objectAtIndex(poll_Trending_Count) as? String
                        
                        let dataQuestionEmojiTrend: NSData = emojiRetrieveQuestTrend!.dataUsingEncoding(NSUTF8StringEncoding)!
                        let strEmojiQuestionTrend: String = String(data: dataQuestionEmojiTrend, encoding: NSNonLossyASCIIStringEncoding)!
                        
                        lblVoteText.text = strEmojiQuestionTrend
                        
                        if ((arr_TrendingPolls .valueForKey("poll_user_name") .objectAtIndex(poll_Trending_Count) as? String) == "") {
                            lblPollUsername.text = arr_TrendingPolls .valueForKey("poll_full_name") .objectAtIndex(poll_Trending_Count) as? String
                        }
                        else
                        {
                            lblPollUsername.text = arr_TrendingPolls .valueForKey("poll_user_name") .objectAtIndex(poll_Trending_Count) as? String
                        }
                        
                        if (arr_TrendingPolls .valueForKey("following") .objectAtIndex(0) as? String) == "yes" {
                            
                            btnFollowUser.setImage(UIImage (named: "check"), forState: UIControlState.Normal)
                        }
                        else
                        {
                            btnFollowUser.setImage(UIImage (named: "add-2"), forState: UIControlState.Normal)
                        }
                        
                        if (((arr_TrendingPolls .valueForKey("poll_user_id") .objectAtIndex(0) as? String)) == userDefaults .valueForKey("UserIdSave")! as? String) {
                            
                            btnFollowUser.hidden = true
                        }
                        else
                        {
                            btnFollowUser.hidden = false
                        }
                        
                        if (arr_TrendingPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! != ""  {
                            
                            let imageUrlUser = NSURL(string: (arr_TrendingPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! )
                            img_UserPic.sd_setImageWithURL(imageUrlUser!, placeholderImage: nil)
                        }
                        let votesCount = arr_TrendingPolls .valueForKey("Votes") .objectAtIndex(0) as! NSInteger
                        
                        lblVotes.text = String(votesCount)
                        
                        let emojiRetrieveHated3 = arr_TrendingPolls .valueForKey("choice1") .objectAtIndex(0) as? String
                        let dataHatedEmoji3: NSData = emojiRetrieveHated3!.dataUsingEncoding(NSUTF8StringEncoding)!
                        let strEmojiHated3: String = String(data: dataHatedEmoji3, encoding: NSNonLossyASCIIStringEncoding)!
                        
                        lblHated.text = strEmojiHated3
                        
                        let emojiRetrieveLoved3 = arr_TrendingPolls .valueForKey("choice2") .objectAtIndex(0) as? String
                        let dataLovedEmoji3: NSData = emojiRetrieveLoved3!.dataUsingEncoding(NSUTF8StringEncoding)!
                        let strEmojiLoved3: String = String(data: dataLovedEmoji3, encoding: NSNonLossyASCIIStringEncoding)!
                        
                        lblLoved.text       = strEmojiLoved3
                        
                        lblDislikeText.text = strEmojiHated3
                        
                        lblLikeText.text    = strEmojiLoved3
                        
                        let str_City = arr_TrendingPolls .valueForKey("poll_user_city") .objectAtIndex(poll_Trending_Count) as? String
                        
                        let str_State = arr_TrendingPolls .valueForKey("poll_user_state") .objectAtIndex(poll_Trending_Count) as? String
                        
                        if (str_City == "" && str_State == "") {
                            
                            lblPollUserLocation!.text = ""
                        }
                        else
                        {
                            lblPollUserLocation!.text = str_City! + ", " + str_State!
                        }
                        
                        if ((arr_TrendingPolls .valueForKey("poll_type") .objectAtIndex(poll_Trending_Count) as? String) == "multi") {
                            
                            imgSinglePollPic.hidden = true
                            
                            imgMultiPollPic1.image = UIImage (named: "")
                            
                            imgMultiPollPic2.image = UIImage (named: "")
                            
                            let imageUrl = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(poll_Trending_Count) .objectAtIndex(0) as? String)! )
                            imgMultiPollPic1.sd_setImageWithURL(imageUrl!, placeholderImage:UIImage (named: "no-imge"))
                            
                            let imageUrl1 = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(poll_Trending_Count)  .objectAtIndex(1) as? String)! )
                            imgMultiPollPic2.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "no-imge"))
                        }
                        else
                        {
                            imgSinglePollPic.hidden = false
                            imgSinglePollPic.image = UIImage (named: "")
                            
                            let imageUrl2 = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(poll_Trending_Count)  .objectAtIndex(0) as? String)! )
                            imgSinglePollPic.sd_setImageWithURL(imageUrl2!, placeholderImage: UIImage (named: "no-imge"))
                        }
                    }
                }
            }
        }
    }
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
        
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    // MARK: - BUTTON LOVED ACTION
    
    func btnLovedAction() {
        
        str_Loved_True = true
        
        if btnFeedOutlet.selected == true {
            
            if arr_FeedPolls.count == 0 && arr_TrendingPolls.count == 0 {
                
                viewDemo.hidden = true
            }
            else
            {
                let transition: CATransition = CATransition()
                transition.duration = 1.0
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                viewSwipe.layer.addAnimation(transition, forKey: nil)
                
                let intFeedCount = arr_FeedPolls.count
                
                label_FeedCopyFinal.text = String(intFeedCount)
                
                str_Poll_Answer = arr_FeedPolls .valueForKey("choice2") .objectAtIndex(poll_Feed_Count) as! String as String!
                
                userDefaults .setValue(arr_FeedPolls .valueForKey("poll_id") .objectAtIndex(poll_Feed_Count), forKey: "pollID")
                
                self.savePollsAPImethod()
            }
        }
        else if btnTrendingOutlet.selected == true
        {
            if arr_FeedPolls.count == 0 && arr_TrendingPolls.count == 0 {
                
                viewDemo.hidden = true
            }
            else
            {
                let transition: CATransition = CATransition()
                transition.duration = 1.0
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                viewSwipe.layer.addAnimation(transition, forKey: nil)
                
                str_Poll_Answer = arr_TrendingPolls .valueForKey("choice2") .objectAtIndex(poll_Trending_Count) as! String as String!
                
                userDefaults .setValue(arr_TrendingPolls .valueForKey("poll_id") .objectAtIndex(poll_Trending_Count), forKey: "pollID")
                
                self.savePollsAPImethod()
            }
        }
    }
    
    // MARK: - BUTTON HATED ACTION
    
    func btnHatedAction() {
        
        str_Loved_True = false
        
        if btnFeedOutlet.selected == true {
            
            if arr_FeedPolls.count == 0 && arr_TrendingPolls.count == 0 {
                
                viewDemo.hidden = true
            }
            else
            {
                
                let transition: CATransition = CATransition()
                transition.duration = 1.0
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromLeft
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                viewSwipe.layer.addAnimation(transition, forKey: nil)
                
                let intFeedCount = arr_FeedPolls.count
                
                label_FeedCopyFinal.text = String(intFeedCount)
                
                str_Poll_Answer = arr_FeedPolls .valueForKey("choice1") .objectAtIndex(poll_Feed_Count) as! String as String!
                
                userDefaults .setValue(arr_FeedPolls .valueForKey("poll_id") .objectAtIndex(poll_Feed_Count), forKey: "pollID")
                
                self.savePollsAPImethod()
            }
        }
        else if btnTrendingOutlet.selected == true
        {
            if arr_FeedPolls.count == 0 && arr_TrendingPolls.count == 0 {
                
                viewDemo.hidden = true
            }
            else
            {
                let transition: CATransition = CATransition()
                transition.duration = 1.0
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromLeft
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                viewSwipe.layer.addAnimation(transition, forKey: nil)
                
                str_Poll_Answer = arr_TrendingPolls .valueForKey("choice1") .objectAtIndex(poll_Trending_Count) as! String as String!
                
                userDefaults .setValue(arr_TrendingPolls .valueForKey("poll_id") .objectAtIndex(poll_Trending_Count), forKey: "pollID")
                
                self.savePollsAPImethod()
            }
        }
        
    }
    
    func btnTrendingSelected() {
        
        if arr_TrendingPolls.count == 0
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "No Trending Polls found" as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            
            if str_Loved_True == true {
                
                let transition: CATransition = CATransition()
                transition.duration = 1.0
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                viewDemo.layer.addAnimation(transition, forKey: nil)
            }
            else
            {
                let transition: CATransition = CATransition()
                transition.duration = 1.0
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromLeft
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                viewDemo.layer.addAnimation(transition, forKey: nil)
            }
            
            lblTrending.textColor = UIColor.whiteColor()
            lblFeedCount.backgroundColor = UIColor.init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0)
            lblFeed.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
            
            btnTrendingOutlet.selected = true
            btnFeedOutlet.selected     = false
            
            
            if (((arr_TrendingPolls .valueForKey("following") .objectAtIndex(0) as? String)) == "yes") {
                
                btnFollowUser.setImage(UIImage (named: "check"), forState: UIControlState.Normal)
                
                str_FollowTrending = "yes"
            }
            else
            {
                btnFollowUser.setImage(UIImage (named: "add-2"), forState: UIControlState.Normal)
                
                str_FollowTrending = "no"
            }
            
            if (((arr_TrendingPolls .valueForKey("poll_user_id") .objectAtIndex(0) as? String)) == userDefaults .valueForKey("UserIdSave")! as? String) {
                
                btnFollowUser.hidden = true
            }
            else
            {
                btnFollowUser.hidden = false
            }
            
            if (arr_TrendingPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! != ""  {
                
                let imageUrlUser = NSURL(string: (arr_TrendingPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! )
                img_UserPic.sd_setImageWithURL(imageUrlUser!, placeholderImage: nil)
            }
            
            let emojiRetrieveHated4 = arr_TrendingPolls .valueForKey("choice1") .objectAtIndex(0) as? String
            let dataHatedEmoji4: NSData = emojiRetrieveHated4!.dataUsingEncoding(NSUTF8StringEncoding)!
            let strEmojiHated4: String = String(data: dataHatedEmoji4, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblHated.text = strEmojiHated4
            
            let emojiRetrieveLoved4 = arr_TrendingPolls .valueForKey("choice2") .objectAtIndex(0) as? String
            let dataLovedEmoji4: NSData = emojiRetrieveLoved4!.dataUsingEncoding(NSUTF8StringEncoding)!
            let strEmojiLoved4: String = String(data: dataLovedEmoji4, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblLoved.text       = strEmojiLoved4
            
            lblDislikeText.text = strEmojiHated4
            
            lblLikeText.text    = strEmojiLoved4
            
            let emojiRetrieveQuestTrending = arr_TrendingPolls .valueForKey("question") .objectAtIndex(0) as? String
            
            let dataQuestionEmojiTrend: NSData = emojiRetrieveQuestTrending!.dataUsingEncoding(NSUTF8StringEncoding)!
            let strEmojiQuestionTrend: String = String(data: dataQuestionEmojiTrend, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblVoteText.text = strEmojiQuestionTrend
            
            if ((arr_TrendingPolls .valueForKey("poll_user_name") .objectAtIndex(poll_Trending_Count) as? String) == "") {
                lblPollUsername.text = arr_TrendingPolls .valueForKey("poll_full_name") .objectAtIndex(poll_Trending_Count) as? String
            }
            else
            {
                lblPollUsername.text = arr_TrendingPolls .valueForKey("poll_user_name") .objectAtIndex(poll_Trending_Count) as? String
            }
            
            let str_City = arr_TrendingPolls .valueForKey("poll_user_city") .objectAtIndex(poll_Trending_Count) as? String
            
            let str_State = arr_TrendingPolls .valueForKey("poll_user_state") .objectAtIndex(poll_Trending_Count) as? String
            
            //print(str_City! + ", " + str_State!)
            
            if (str_City == "" && str_State == "") {
                
                lblPollUserLocation!.text = ""
            }
            else
            {
                lblPollUserLocation!.text = str_City! + ", " + str_State!
            }
            
            if ((arr_TrendingPolls .valueForKey("poll_type") .objectAtIndex(poll_Trending_Count) as? String) == "multi") {
                
                imgSinglePollPic.hidden = true
                
                imgMultiPollPic1.image = UIImage (named: "")
                
                imgMultiPollPic2.image = UIImage (named: "")
                
                let imageUrl = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(poll_Trending_Count) .objectAtIndex(0) as? String)! )
                imgMultiPollPic1.sd_setImageWithURL(imageUrl!, placeholderImage: UIImage (named: "no-imge"))
                
                let imageUrl1 = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(poll_Trending_Count)  .objectAtIndex(1) as? String)! )
                imgMultiPollPic2.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "no-imge"))
            }
            else
            {
                imgSinglePollPic.hidden = false
                imgSinglePollPic.image = UIImage (named: "")
                
                let imageUrl2 = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(poll_Trending_Count)  .objectAtIndex(0) as? String)! )
                imgSinglePollPic.sd_setImageWithURL(imageUrl2!, placeholderImage: UIImage (named: "no-imge"))
            }
        }
        
    }
    
    func btnFeedSelected()  {
        
        if  arr_FeedPolls.count == 0
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "No Feed Polls found" as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            if str_Loved_True == true {
                
                let transition: CATransition = CATransition()
                transition.duration = 1.0
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                viewDemo.layer.addAnimation(transition, forKey: nil)
            }
            else
            {
                let transition: CATransition = CATransition()
                transition.duration = 1.0
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromLeft
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                viewDemo.layer.addAnimation(transition, forKey: nil)
            }
            
            lblFeed.textColor     = UIColor.whiteColor()
            
            lblTrending.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
            
            lblFeedCount.backgroundColor = UIColor.init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0)
            
            btnFeedOutlet.selected     = true
            btnTrendingOutlet.selected = false
            
            let intFeedCount = arr_FeedPolls.count
            
            label_FeedCopyFinal.text = String(intFeedCount)
            
            if (((arr_FeedPolls .valueForKey("following") .objectAtIndex(0) as? String)) == "yes") {
                
                btnFollowUser.setImage(UIImage (named: "check"), forState: UIControlState.Normal)
                str_FollowFeed = "yes"
            }
            else
            {
                btnFollowUser.setImage(UIImage (named: "add-2"), forState: UIControlState.Normal)
                str_FollowFeed = "no"
            }
            
            if (((arr_TrendingPolls .valueForKey("poll_user_id") .objectAtIndex(0) as? String)) == userDefaults .valueForKey("UserIdSave")! as? String) {
                
                btnFollowUser.hidden = true
            }
            else
            {
                btnFollowUser.hidden = false
            }
            
            if (arr_FeedPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! != ""  {
                
                let imageUrlUser = NSURL(string: (arr_FeedPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! )
                img_UserPic.sd_setImageWithURL(imageUrlUser!, placeholderImage: nil)
            }
            
            let emojiRetrieveHated5 = arr_FeedPolls .valueForKey("choice1") .objectAtIndex(0) as? String
            let dataHatedEmoji5: NSData = emojiRetrieveHated5!.dataUsingEncoding(NSUTF8StringEncoding)!
            let strEmojiHated5: String = String(data: dataHatedEmoji5, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblHated.text = strEmojiHated5
            
            let emojiRetrieveLoved5 = arr_FeedPolls .valueForKey("choice2") .objectAtIndex(0) as? String
            let dataLovedEmoji5: NSData = emojiRetrieveLoved5!.dataUsingEncoding(NSUTF8StringEncoding)!
            let strEmojiLoved5: String = String(data: dataLovedEmoji5, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblLoved.text       = strEmojiLoved5
            
            lblDislikeText.text = strEmojiHated5
            
            lblLikeText.text    = strEmojiLoved5
            
            let emojiRetrieveQuestFeed = arr_FeedPolls .valueForKey("question") .objectAtIndex(poll_Feed_Count) as? String
            
            let dataQuestionEmojiFeed: NSData = emojiRetrieveQuestFeed!.dataUsingEncoding(NSUTF8StringEncoding)!
            let strEmojiQuestionFeed: String = String(data: dataQuestionEmojiFeed, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblVoteText.text = strEmojiQuestionFeed
            
            if ((arr_FeedPolls .valueForKey("poll_user_name") .objectAtIndex(poll_Feed_Count) as? String) == "") {
                lblPollUsername.text = arr_FeedPolls .valueForKey("poll_full_name") .objectAtIndex(poll_Feed_Count) as? String
            }
            else
            {
                lblPollUsername.text = arr_FeedPolls .valueForKey("poll_user_name") .objectAtIndex(poll_Feed_Count) as? String
            }
            
            let str_City = arr_FeedPolls .valueForKey("poll_user_city") .objectAtIndex(poll_Feed_Count) as? String
            
            let str_State = arr_FeedPolls .valueForKey("poll_user_state") .objectAtIndex(poll_Feed_Count) as? String
            
            if (str_City == "" && str_State == "") {
                
                lblPollUserLocation!.text = ""
            }
            else
            {
                lblPollUserLocation!.text = str_City! + ", " + str_State!
            }
            
            if ((arr_FeedPolls .valueForKey("poll_type") .objectAtIndex(poll_Feed_Count) as? String) == "multi") {
                
                imgSinglePollPic.hidden = true
                
                imgMultiPollPic1.image = UIImage (named: "")
                imgMultiPollPic2.image = UIImage (named: "")
                
                let imageUrl = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(poll_Feed_Count) .objectAtIndex(0) as? String)! )
                imgMultiPollPic1.sd_setImageWithURL(imageUrl!, placeholderImage: UIImage (named: "no-imge"))
                
                let imageUrl1 = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(poll_Feed_Count)  .objectAtIndex(1) as? String)! )
                imgMultiPollPic2.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "no-imge"))
            }
            else
            {
                imgSinglePollPic.hidden = false
                
                imgSinglePollPic.image = UIImage (named: "")
                
                let imageUrl2 = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(poll_Feed_Count)  .objectAtIndex(0) as? String)! )
                imgSinglePollPic.sd_setImageWithURL(imageUrl2!, placeholderImage: UIImage (named: "no-imge"))
            }
        }
        
    }
    
    // MARK: - BUTTON USER PROFILE ACTION
    
    @IBAction func btnUserProfile(sender: AnyObject) {
        
       let userProfileObj = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileVC")as! MyProfileVC
        if btnFeedOutlet.selected == true {
            
            if arr_FeedPolls.count != 0 {
                
        userDefaults .setValue(arr_FeedPolls .valueForKey("poll_user_id") .objectAtIndex(0), forKey: "otheruserID")
                    
        self.navigationController?.pushViewController(userProfileObj, animated: true)
            }
        }
        else if btnTrendingOutlet.selected == true
        {
            if arr_TrendingPolls.count != 0 {
                
            userDefaults .setValue(arr_TrendingPolls .valueForKey("poll_user_id") .objectAtIndex(0), forKey: "otheruserID")
                        
                self.navigationController?.pushViewController(userProfileObj, animated: true)
                
            }
        }
        
        
    }
    
    // MARK: - BUTTON FOLLOW USER
    
    @IBAction func btnFollowUser(sender: AnyObject) {
        
        if btnFeedOutlet.selected == true {
            
            if arr_FeedPolls.count != 0 {
                
               if (((arr_FeedPolls .valueForKey("following") .objectAtIndex(0) as? String)) == "no") {
                        
                        userDefaults .setValue(arr_FeedPolls .valueForKey("poll_user_id") .objectAtIndex(0), forKey: "followUserID")
                        
                        self.followUserAPI()
                    }
                    else
                    {
                        userDefaults .setValue(arr_FeedPolls .valueForKey("poll_user_id") .objectAtIndex(0), forKey: "followUserID")
                        
                        self.actionSheetUnfollow()
                }

            }
        }
        else if btnTrendingOutlet.selected == true
        {
            if arr_TrendingPolls.count != 0 {
                
                if (((arr_TrendingPolls .valueForKey("following") .objectAtIndex(0) as? String)) == "no") {
                    
                    userDefaults .setValue(arr_TrendingPolls .valueForKey("poll_user_id") .objectAtIndex(0), forKey: "followUserID")
                    
                    self.followUserAPI()
                    }
                    else
                    {
                    userDefaults .setValue(arr_TrendingPolls .valueForKey("poll_user_id") .objectAtIndex(0), forKey: "followUserID")
                        
                        self.actionSheetUnfollow()
                    }
                
            }
        }
    }
    
    func actionSheetUnfollow() -> Void {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Pop-Op", message: "Unfollow", preferredStyle: .ActionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        let unfollowActionButton: UIAlertAction = UIAlertAction(title: "Unfollow", style: .Default)
        { action -> Void in
            
            self.UnfollowUserAPI()
        }
        
        actionSheetControllerIOS8.addAction(unfollowActionButton)
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    // MARK: - GET POLLS API METHOD
    
    func getPollsAPImethod() {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KgetPolls)
                
                //print(params)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    // MARK: - Save Polls Answers API METHOD
    
    func savePollsAPImethod() {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "poll_id": userDefaults.valueForKey("pollID")!, "answer": str_Poll_Answer]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KSavePolls)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - FOLLOW USER API
    
    func followUserAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "follow_id": userDefaults.valueForKey("followUserID")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KFollowUser)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - FOLLOW USER API
    
    func UnfollowUserAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "follow_id": userDefaults.valueForKey("followUserID")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KUnfollow)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }

    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
      //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KgetPolls)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_FeedPolls .removeAllObjects()
                arr_TrendingPolls .removeAllObjects()
                
                str_FollowFeed     = "no"
                str_FollowTrending = "no"
                arr_FeedPolls .addObjectsFromArray(responseDict .objectForKey("feedPolls")! as! [AnyObject])
            arr_TrendingPolls .addObjectsFromArray(responseDict .objectForKey("trendingPolls")! as! [AnyObject])
                
                userDefaults .setValue(responseDict .valueForKey("countFollowers"), forKey: "countFollowers")
                
                userDefaults .setValue(responseDict .valueForKey("countFollowing"), forKey: "countFollowing")
                
                userDefaults .setValue(arr_FeedPolls.count, forKey: "countUserPolls")
                
               NSNotificationCenter.defaultCenter().postNotificationName("countFollowing", object: nil)
                
                if arr_FeedPolls.count != 0 || arr_TrendingPolls.count != 0
                {
                    
                if btnFeedOutlet.selected == true {
                    
                    if arr_FeedPolls.count != 0 {
                        
                        viewDemo.hidden = false
                        self.methodFeedAdone()
                    }
                    else
                    {
                        if arr_TrendingPolls.count != 0 {
                            viewDemo.hidden = false
                            self.methodTrendingAdone()
                        }
                        else
                        {
                            label_FeedCopyFinal.text = String("0")
                            viewDemo.hidden = true
                            imageAllDone.hidden = true
                            lblAllDone.text = "NO NEW POLLS FOUND"
                        }
                    }
                    
                }
                else if btnTrendingOutlet.selected == true
                {
                    if arr_TrendingPolls.count != 0 {
                        
                        viewDemo.hidden = false
                        self.methodTrendingAdone()
                    }
                    else
                    {
                        if arr_FeedPolls.count != 0 {
                            viewDemo.hidden = false
                            self.methodFeedAdone()
                        }
                        else
                        {
                            label_FeedCopyFinal.text = String("0")
                            viewDemo.hidden = true
                            imageAllDone.hidden = true
                            lblAllDone.text = "NO NEW POLLS FOUND"
                        }
                    }
                }
                }
                else
                {
                    label_FeedCopyFinal.text = String("0")
                    viewDemo.hidden = true
                    imageAllDone.hidden = true
                    lblAllDone.text = "NO NEW POLLS FOUND"
                }
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                label_FeedCopyFinal.text = String("0")
                viewDemo.hidden = true
                imageAllDone.hidden = true
                lblAllDone.text = "NO NEW POLLS FOUND"
            }
        }
        
        else if serviceurl .isEqualToString(Header.KSavePolls)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_FeedPolls .removeAllObjects()
                arr_TrendingPolls .removeAllObjects()
                
                str_FollowFeed     = "no"
                str_FollowTrending = "no"
                arr_FeedPolls .addObjectsFromArray(responseDict .objectForKey("feedPolls")! as! [AnyObject])
                arr_TrendingPolls .addObjectsFromArray(responseDict .objectForKey("trendingPolls")! as! [AnyObject])
                
                userDefaults .setValue(arr_FeedPolls.count, forKey: "countUserPolls")
                
                NSNotificationCenter.defaultCenter().postNotificationName("countFollowing", object: nil)
                
                label_FeedCopyFinal.text = String("0")
                viewDemo.hidden = false
                imageAllDone.hidden = false
                lblAllDone.text = "ALL DONE"
                
                if arr_FeedPolls.count != 0 || arr_TrendingPolls.count != 0
                {
                
                if btnFeedOutlet.selected == true {
                    
                    if arr_FeedPolls.count != 0 {
                        viewDemo.hidden = false
                        self.methodFeedAdone()
                    }
                    else
                    {
                        if arr_TrendingPolls.count != 0 {
                            viewDemo.hidden = false
                            self.methodTrendingAdone()
                        }
                        else
                        {
                            label_FeedCopyFinal.text = String("0")
                            viewDemo.hidden = true
                            imageAllDone.hidden = false
                            lblAllDone.text = "ALL DONE"
                        }
                    }
                    
                }
                else if btnTrendingOutlet.selected == true
                {
                    if arr_TrendingPolls.count != 0 {
                        
                        viewDemo.hidden = false
                        self.methodTrendingAdone()
                    }
                    else
                    {
                        if arr_FeedPolls.count != 0 {
                            viewDemo.hidden = false
                            self.methodFeedAdone()
                        }
                        else
                        {
                            label_FeedCopyFinal.text = String("0")
                            viewDemo.hidden = true
                            imageAllDone.hidden = false
                            lblAllDone.text = "ALL DONE"
                        }
                    }
                }
                    
                }
                else
                {
                    label_FeedCopyFinal.text = String("0")
                    viewDemo.hidden = true
                    imageAllDone.hidden = false
                    lblAllDone.text = "ALL DONE"
                }
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                label_FeedCopyFinal.text = String("0")
                viewDemo.hidden = true
                imageAllDone.hidden = false
                lblAllDone.text = "ALL DONE"
            }
        }
        else if (serviceurl .isEqualToString(Header.KFollowUser))
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                
                btnFollowUser.setImage(UIImage (named: "check"), forState: UIControlState.Normal)
                
                self.getPollsAPImethod()
                
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if (serviceurl .isEqualToString(Header.KUnfollow))
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                
                btnFollowUser.setImage(UIImage (named: "add-2"), forState: UIControlState.Normal)
                
                self.getPollsAPImethod()
                
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }
    
    
    func methodFeedAdone() -> Void {
        
        numberOfActiveView = arr_FeedPolls.count
        
        lblFeed.textColor     = UIColor.whiteColor()
        
        lblTrending.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        lblFeedCount.backgroundColor = UIColor.init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0)
        
        btnFeedOutlet.selected     = true
        btnTrendingOutlet.selected = false
        
        let intFeedCount = arr_FeedPolls.count
        label_FeedCopyFinal.text = String(intFeedCount)
        
        feedCountInt = arr_FeedPolls.count
        
        let emojiRetrieveQuest = arr_FeedPolls .valueForKey("question") .objectAtIndex(0) as? String
        let dataQuestionEmoji: NSData = emojiRetrieveQuest!.dataUsingEncoding(NSUTF8StringEncoding)!
        let strEmojiQuestion: String = String(data: dataQuestionEmoji, encoding: NSNonLossyASCIIStringEncoding)!
        
        lblVoteText.text = strEmojiQuestion
        
        if (arr_FeedPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! != ""  {
            
            let imageUrlUser = NSURL(string: (arr_FeedPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! )
            img_UserPic.sd_setImageWithURL(imageUrlUser!, placeholderImage: nil)
        }
        
        let votesCount = arr_FeedPolls .valueForKey("Votes") .objectAtIndex(0) as! NSInteger
        
        lblVotes.text = String(votesCount)
        
        if (((arr_FeedPolls .valueForKey("following") .objectAtIndex(0) as? String)) == "yes") {
            
            btnFollowUser.setImage(UIImage (named: "check"), forState: UIControlState.Normal)
            
            str_FollowFeed = "yes"
        }
        else
        {
            btnFollowUser.setImage(UIImage (named: "add-2"), forState: UIControlState.Normal)
            
            str_FollowFeed = "no"
        }
        
        if (((arr_FeedPolls .valueForKey("poll_user_id") .objectAtIndex(0) as? String)) == userDefaults .valueForKey("UserIdSave")! as? String) {
            
            btnFollowUser.hidden = true
        }
        else
        {
            btnFollowUser.hidden = false
        }
        
        if ((arr_FeedPolls .valueForKey("poll_user_name") .objectAtIndex(0) as? String) == "") {
            lblPollUsername.text = arr_FeedPolls .valueForKey("poll_full_name") .objectAtIndex(0) as? String
        }
        else
        {
            lblPollUsername.text = arr_FeedPolls .valueForKey("poll_user_name") .objectAtIndex(0) as? String
        }
        
        let emojiRetrieveHated6 = arr_FeedPolls .valueForKey("choice1") .objectAtIndex(0) as? String
        let dataHatedEmoji6: NSData = emojiRetrieveHated6!.dataUsingEncoding(NSUTF8StringEncoding)!
        let strEmojiHated6: String = String(data: dataHatedEmoji6, encoding: NSNonLossyASCIIStringEncoding)!
        
        lblHated.text = strEmojiHated6
        
        let emojiRetrieveLoved6 = arr_FeedPolls .valueForKey("choice2") .objectAtIndex(0) as? String
        let dataLovedEmoji6: NSData = emojiRetrieveLoved6!.dataUsingEncoding(NSUTF8StringEncoding)!
        let strEmojiLoved6: String = String(data: dataLovedEmoji6, encoding: NSNonLossyASCIIStringEncoding)!
        
        lblLoved.text       = strEmojiLoved6
        
        lblDislikeText.text = strEmojiHated6
        
        lblLikeText.text    = strEmojiLoved6
        
        let str_City = arr_FeedPolls .valueForKey("poll_user_city") .objectAtIndex(0) as? String
        
        let str_State = arr_FeedPolls .valueForKey("poll_user_state") .objectAtIndex(0) as? String
        
        if (str_City == "" && str_State == "") {
            
            lblPollUserLocation.text = ""
        }
        else
        {
            lblPollUserLocation.text = str_City! + ", " + str_State!
        }
        
        if ((arr_FeedPolls .valueForKey("poll_type") .objectAtIndex(0) as? String) == "multi") {
            
            imgSinglePollPic.hidden = true
            
            imgMultiPollPic1.image = UIImage (named: "")
            
            imgMultiPollPic2.image = UIImage (named: "")
            
            let imageUrl = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(0) .objectAtIndex(0) as? String)! )
            imgMultiPollPic1.sd_setImageWithURL(imageUrl!, placeholderImage: UIImage (named: "no-imge"))
            
            let imageUrl1 = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(0)  .objectAtIndex(1) as? String)! )
            imgMultiPollPic2.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "no-imge"))
        }
        else
        {
            imgSinglePollPic.hidden = false
            imgSinglePollPic.image = UIImage (named: "")
            
            let imageUrl2 = NSURL(string: (arr_FeedPolls .valueForKey("images") .objectAtIndex(0)  .objectAtIndex(0) as? String)! )
            imgSinglePollPic.sd_setImageWithURL(imageUrl2!, placeholderImage: UIImage (named: "no-imge"))
        }

    }
    
    func methodTrendingAdone() -> Void {
        
        lblTrending.textColor = UIColor.whiteColor()
        lblFeedCount.backgroundColor = UIColor.init(red: 11/255.0, green: 19/255.0, blue: 31/255.0, alpha: 1.0)
        lblFeed.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        btnTrendingOutlet.selected = true
        btnFeedOutlet.selected     = false
        
        let intFeedCount = arr_FeedPolls.count
        label_FeedCopyFinal.text = String(intFeedCount)
        
        let votesCount = arr_TrendingPolls .valueForKey("Votes") .objectAtIndex(0) as! NSInteger
        
        lblVotes.text = String(votesCount)
        
        if (((arr_TrendingPolls .valueForKey("following") .objectAtIndex(0) as? String)) == "yes") {
            
            btnFollowUser.setImage(UIImage (named: "check"), forState: UIControlState.Normal)
            
            str_FollowTrending = "yes"
        }
        else
        {
            btnFollowUser.setImage(UIImage (named: "add-2"), forState: UIControlState.Normal)
            
            str_FollowTrending = "no"
        }
        
        if (((arr_TrendingPolls .valueForKey("poll_user_id") .objectAtIndex(0) as? String)) == userDefaults .valueForKey("UserIdSave")! as? String) {
            
            btnFollowUser.hidden = true
        }
        else
        {
            btnFollowUser.hidden = false
        }
        
        let emojiRetrieveQuestTrending = arr_TrendingPolls .valueForKey("question") .objectAtIndex(0) as? String
        
        let dataQuestionEmojiTrend: NSData = emojiRetrieveQuestTrending!.dataUsingEncoding(NSUTF8StringEncoding)!
        let strEmojiQuestionTrend: String = String(data: dataQuestionEmojiTrend, encoding: NSNonLossyASCIIStringEncoding)!
        
        lblVoteText.text = strEmojiQuestionTrend
        
        if ((arr_FeedPolls .valueForKey("poll_user_name") .objectAtIndex(0) as? String) == "") {
            lblPollUsername.text = arr_TrendingPolls .valueForKey("poll_full_name") .objectAtIndex(0) as? String
        }
        else
        {
            lblPollUsername.text = arr_TrendingPolls .valueForKey("poll_user_name") .objectAtIndex(0) as? String
        }
        
        if (arr_TrendingPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! != ""  {
            
            let imageUrlUser = NSURL(string: (arr_TrendingPolls .objectAtIndex(0) .valueForKey("user_image") as? String)! )
            img_UserPic.sd_setImageWithURL(imageUrlUser!, placeholderImage: nil)
        }
        
        let emojiRetrieveHated1 = arr_TrendingPolls .valueForKey("choice1") .objectAtIndex(0) as? String
        let dataHatedEmoji1: NSData = emojiRetrieveHated1!.dataUsingEncoding(NSUTF8StringEncoding)!
        let strEmojiHated1: String = String(data: dataHatedEmoji1, encoding: NSNonLossyASCIIStringEncoding)!
        
        lblHated.text = strEmojiHated1
        
        let emojiRetrieveLoved1 = arr_TrendingPolls .valueForKey("choice2") .objectAtIndex(0) as? String
        let dataLovedEmoji1: NSData = emojiRetrieveLoved1!.dataUsingEncoding(NSUTF8StringEncoding)!
        let strEmojiLoved1: String = String(data: dataLovedEmoji1, encoding: NSNonLossyASCIIStringEncoding)!
        
        lblLoved.text       = strEmojiLoved1
        
        lblDislikeText.text = strEmojiHated1
        
        lblLikeText.text    = strEmojiLoved1
        
        let str_City = arr_TrendingPolls .valueForKey("poll_user_city") .objectAtIndex(0) as? String
        
        let str_State = arr_TrendingPolls .valueForKey("poll_user_state") .objectAtIndex(0) as? String
        
        if (str_City == "" && str_State == "") {
            
            lblPollUserLocation!.text = ""
        }
        else
        {
            lblPollUserLocation!.text = str_City! + ", " + str_State!
        }
        
        if ((arr_TrendingPolls .valueForKey("poll_type") .objectAtIndex(0) as? String) == "multi") {
            
            imgSinglePollPic.hidden = true
            
            imgMultiPollPic1.image = UIImage (named: "")
            
            imgMultiPollPic2.image = UIImage (named: "")
            
            let imageUrl = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(0) .objectAtIndex(0) as? String)! )
            imgMultiPollPic1.sd_setImageWithURL(imageUrl!, placeholderImage: UIImage (named: "no-imge"))
            
            let imageUrl1 = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(0)  .objectAtIndex(1) as? String)! )
            imgMultiPollPic2.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "no-imge"))
        }
        else
        {
            imgSinglePollPic.hidden = false
            
            imgSinglePollPic.image = UIImage (named: "")
            
            let imageUrl2 = NSURL(string: (arr_TrendingPolls .valueForKey("images") .objectAtIndex(0)  .objectAtIndex(0) as? String)! )
            imgSinglePollPic.sd_setImageWithURL(imageUrl2!, placeholderImage: UIImage (named: "no-imge"))
        }
        
    }
    
}

