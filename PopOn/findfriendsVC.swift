//
//  findfriendsVC.swift
//  PopOn
//
//  Created by Nitin Suri on 21/07/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit


class findfriendsVC: UIViewController,SlideNavigationControllerDelegate {

    @IBOutlet weak var tableFindPeople: UITableView!
    
    @IBOutlet weak var btnToggle: UIButton!
    var arr_OptionTypes = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        btnToggle.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        slidecontroller.enableSwipeGesture = true
        
        arr_OptionTypes = ["Find Facebook Friends","Find Contact Friends"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cellIdentifier = "followCell"
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            let lblTitle1 = cell.viewWithTag(11)as? UILabel
            lblTitle1?.text = arr_OptionTypes .objectAtIndex(indexPath.row) as? String
        
        if indexPath.row == 0 {
            let image_user = cell.viewWithTag(110) as? UIImageView
            image_user?.image = UIImage.init(named: "fb")
        }
        else
        {
            let image_user = cell.viewWithTag(10) as? UIImageView
            image_user?.image = UIImage.init(named: "friends")
        }
        
            return cell
    }
    
    //MARK: - DIDSELECT TABLEVIEW DELEGATE METHOD
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
//        if arrayFBdata.sharedInstanceArrFB.count == 0 {
//            
//            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message:"No Facebook friend found" as String, preferredStyle: UIAlertControllerStyle.Alert)
//            
//            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
//                UIAlertAction in
//                
//            }
//            alertViewShow.addAction(okAction)
//            self.presentViewController(alertViewShow, animated: true, completion: nil)
//        }
//        else
//        {
        if indexPath.row == 0 {
            
            let friendlistObj = self.storyboard?.instantiateViewControllerWithIdentifier("FriendListVC")as! FriendListVC
            self.navigationController?.pushViewController(friendlistObj, animated: true)
        }
        
        
        //}
    }
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }

}
