//
//  PollsVC.swift
//  PopOn
//
//  Created by Nitin Suri on 26/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader

class PollsVC: UIViewController,SlideNavigationControllerDelegate,WebServiceDelegate {

    @IBOutlet weak var btnRightMenuOutlet: UIButton!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var btnMyPollsOutlet: UIButton!
    @IBOutlet weak var btnAnsPollsOutlet: UIButton!
    @IBOutlet weak var tablePolls: UITableView!
    @IBOutlet weak var lblMyPolls: UILabel!
    @IBOutlet weak var lblAnsPolls: UILabel!
    
    var arr_MyPolls: NSMutableArray       = []
    var arr_AnsweredPolls: NSMutableArray = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        btnRightMenuOutlet.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        //btnAnsPollsOutlet.setImage(UIImage (named: "trend-inactive"), forState: UIControlState.Normal)
        
        //btnMyPollsOutlet.setImage(UIImage (named: "feed-inactive"), forState: UIControlState.Normal)
        
        lblMyPolls.textColor  = UIColor.whiteColor()
        
        lblAnsPolls.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        btnMyPollsOutlet.selected  = true
        btnAnsPollsOutlet.selected = false
        
        self.mypollsAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if btnMyPollsOutlet.selected == true {
            
            return 100
        }
        else
        {
            return 120
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        if btnMyPollsOutlet.selected == true {
            return arr_MyPolls.count
        }
        else
        {
            return arr_AnsweredPolls.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if btnAnsPollsOutlet.selected == true {
            
            let cellIdentifier = "ansPollsCell"
            
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            let lblTitle1 = cell.viewWithTag(5)as? UILabel
            
            let emojiRetrieveQuestion = arr_AnsweredPolls.objectAtIndex(indexPath.row) .valueForKey("question") as? String
            let dataemojiQues: NSData = emojiRetrieveQuestion!.dataUsingEncoding(NSUTF8StringEncoding)!
            //print(dataemojiQues)
            let strEmojiQues: String = String(data: dataemojiQues, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblTitle1?.text = strEmojiQues
            
            let image_user = cell.viewWithTag(6) as? UIImageView
            
            if arr_AnsweredPolls.objectAtIndex(indexPath.row)  .valueForKey("poll_user_image") as! String != "" {
                
                let imageUrl1 = NSURL(string: arr_AnsweredPolls.objectAtIndex(indexPath.row)  .valueForKey("poll_user_image") as! String)
                
                image_user!.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "user-small"))
            }
            else
            {
                image_user?.image = UIImage (named: "user-small")
            }
            
            let lblUsername = cell.viewWithTag(7)as? UILabel
            
            if arr_AnsweredPolls.objectAtIndex(indexPath.row) .valueForKey("poll_user_name") as? String == "" {
                
                lblUsername?.text = arr_AnsweredPolls.objectAtIndex(indexPath.row) .valueForKey("poll_full_name") as? String
            }
            else
            {
                lblUsername?.text = arr_AnsweredPolls.objectAtIndex(indexPath.row) .valueForKey("poll_user_name") as? String
            }
            
            let str_City = arr_AnsweredPolls.objectAtIndex(indexPath.row) .valueForKey("poll_user_city")  as? String
            
            let str_State = arr_AnsweredPolls.objectAtIndex(indexPath.row) .valueForKey("poll_user_state")  as? String
            
            let lblLocation = cell.viewWithTag(8) as? UILabel
            
            if (str_City == "" && str_State == "") {
                
                lblLocation!.text = ""
            }
            else
            {
                lblLocation!.text = str_City! + ", " + str_State!
            }
            
            var intVotes1 = NSInteger()
            
            intVotes1 = arr_AnsweredPolls.objectAtIndex(indexPath.row) .valueForKey("Votes") as! NSInteger
            
            let lblVotes1 = cell.viewWithTag(9) as? UILabel
            lblVotes1?.text = String(intVotes1)
            
            let btnUserProfile = cell.viewWithTag(111) as? UIButton
            
            btnUserProfile?.layer .setValue(indexPath.row, forKey: "userIndex")
            
            return cell
            
        }
        else
        {
            let cellIdentifier = "mypollsCell"
            
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            let lblTitle = cell.viewWithTag(1)as? UILabel
            
            let emojiRetrieveQuestion2 = arr_MyPolls.objectAtIndex(indexPath.row) .valueForKey("question") as? String
            let dataemojiQues2: NSData = emojiRetrieveQuestion2!.dataUsingEncoding(NSUTF8StringEncoding)!
            let strEmojiQues2: String = String(data: dataemojiQues2, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblTitle?.text = strEmojiQues2
            
            var intAvg2 = NSInteger()
            
            intAvg2 = arr_MyPolls.objectAtIndex(indexPath.row) .valueForKey("avgChoice2Percentage") as! NSInteger
            
            let lblChoice1 = cell.viewWithTag(2) as? UILabel
            
            let fVote2 = "\(String(intAvg2))%"
            
            lblChoice1?.text = String(fVote2)
            
            var intAvg1 = NSInteger()
            
            intAvg1 = arr_MyPolls.objectAtIndex(indexPath.row) .valueForKey("avgChoice1Percentage") as! NSInteger
            
            let lblChoice3 = cell.viewWithTag(3) as? UILabel
            
            let fVote1 = "\(String(intAvg1))%"
            
            lblChoice3?.text = String(fVote1)
            
            var intVotes = NSInteger()
            
            intVotes = arr_MyPolls.objectAtIndex(indexPath.row) .valueForKey("Votes") as! NSInteger
            
            let lblVotes = cell.viewWithTag(4) as? UILabel
            lblVotes?.text = String(intVotes)
            
            return cell
        }
    }
    //MARK: - DIDSELECT TABLEVIEW DELEGATE METHOD
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let results_Age = self.storyboard?.instantiateViewControllerWithIdentifier("AgeVC")as!
        AgeVC
        
        if btnMyPollsOutlet.selected == true {
            
            let poll_ID = arr_MyPolls .objectAtIndex(indexPath.row) .valueForKey("poll_id")! as AnyObject
            userDefaults .setValue(poll_ID, forKey: "poll_id")
            self.navigationController?.pushViewController(results_Age, animated: true)
        }
        else
        {
//            let myProfileObj = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileVC")as! MyProfileVC
            let poll_ID = arr_AnsweredPolls .objectAtIndex(indexPath.row) .valueForKey("poll_id")! as AnyObject
            userDefaults .setValue(poll_ID, forKey: "poll_id")
            self.navigationController?.pushViewController(results_Age, animated: true)
        }
        
    }

    @IBAction func btnMyPollsAction(sender: AnyObject) {
        
        lblMyPolls.textColor  = UIColor.whiteColor()
        lblAnsPolls.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        btnMyPollsOutlet.selected  = true
        btnAnsPollsOutlet.selected = false
        
        self.mypollsAPI()
      
    }
    @IBAction func btnAnsPollsAction(sender: AnyObject) {
        
        lblMyPolls.textColor  = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        lblAnsPolls.textColor = UIColor.whiteColor()
        btnAnsPollsOutlet.selected = true
        btnMyPollsOutlet.selected  = false
        self.answeredpollsAPI()
    }
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    // MARK: - MY POLLS API
    func mypollsAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KMyPolls)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - MY POLLS API
    
    func answeredpollsAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KAnsweredPolls)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KMyPolls)
        {
            tablePolls.hidden = false
            
            tablePolls.reloadData()
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_MyPolls .removeAllObjects()
                
                arr_MyPolls .addObjectsFromArray(responseDict .objectForKey("feedPolls")! as AnyObject as! [AnyObject])

                
                tablePolls.reloadData()
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                tablePolls.hidden = true
                
                lblNoDataFound.text = responseDict .valueForKey("response")! as! NSString as String
                
            }
        }
        else if serviceurl .isEqualToString(Header.KAnsweredPolls)
        {
            tablePolls.hidden = false
            
            tablePolls.reloadData()
            if responseDict .valueForKey("result")! .boolValue == true
            {
                
            arr_AnsweredPolls .removeAllObjects()
                
            arr_AnsweredPolls .addObjectsFromArray(responseDict .objectForKey("feedPolls")! as AnyObject as! [AnyObject])
                
                if arr_AnsweredPolls.count > 0 {
                    let arrAnsweredPolls_Reverse = NSMutableArray()
                    arrAnsweredPolls_Reverse .addObjectsFromArray(arr_AnsweredPolls as [AnyObject])
                    
                    arr_AnsweredPolls .removeAllObjects()
                    arr_AnsweredPolls .addObjectsFromArray(arrAnsweredPolls_Reverse.reverseObjectEnumerator().allObjects as AnyObject as! [AnyObject])
                }
                
                tablePolls.reloadData()
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                tablePolls.hidden = true
                
                lblNoDataFound.text = responseDict .valueForKey("response")! as! NSString as String
                
//                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
//                
//                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
//                    UIAlertAction in
//                }
//                alertViewShow.addAction(okAction)
//                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }
    
    // MARK: BUTTON VIEW USER PROFILE ACTION
    
    @IBAction func btnViewUserProfile(sender: AnyObject) {
        
        let btnViewProfile = sender
        
        let indexPath = btnViewProfile.layer .valueForKey("userIndex") as! NSInteger
        
        let myProfileObj = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileVC")as! MyProfileVC
        let poll_ID = arr_AnsweredPolls .objectAtIndex(indexPath) .valueForKey("poll_user_id")! as AnyObject
        userDefaults .setValue(poll_ID, forKey: "otheruserID")
        self.navigationController?.pushViewController(myProfileObj, animated: true)
        
    }

}
