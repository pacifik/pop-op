//
//  CategoriesVC.swift
//  PopOn
//
//  Created by Nitin Suri on 17/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import CoreGraphics
import SwiftLoader

class CategoriesVC: UIViewController,WebServiceDelegate,SlideNavigationControllerDelegate {
    
    @IBOutlet weak var btnRightToggle: UIButton!
    //@IBOutlet weak var scrollCategories: UIView!
    @IBOutlet weak var scrollCategories: UIScrollView!
    @IBOutlet weak var btnSkipOutlet: UIButton!
    var cordBtn         = CGFloat()
    var cordBtnHexONE_Y = CGFloat()
    var cordBtnHexTWO_Y = CGFloat()
    var tagValue = NSInteger()
    
    var arr_Categories   = NSMutableArray()
    
    var arr_SaveCatID    = NSMutableArray()
    
    var arr_SavedUserCat = NSMutableArray()
    
    var loopCount = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBarHidden = true
        
        // CORNER RADIUS BUTTON SKIP //
        
        btnSkipOutlet.backgroundColor    = UIColor.clearColor()
        btnSkipOutlet.layer.cornerRadius = 12
        btnSkipOutlet.layer.borderWidth  = 2
        btnSkipOutlet.layer.borderColor  = UIColor.whiteColor().CGColor
        
        //.............................................................
        
        btnRightToggle.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        scrollCategories.backgroundColor = UIColor.init(red: 19/255.0, green: 31/255.0, blue: 52/255.0, alpha: 1.0)
        
        if userDefaults .valueForKey("catSkip") as! NSString == "yes" {
            
            btnSkipOutlet.hidden  = false
            btnRightToggle.hidden = true
        }
        else
        {
            btnSkipOutlet.hidden  = true
            btnRightToggle.hidden = false
        }
        
        self.getCategoriesAPImethod()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hexagonButtonCategories()
    {
        var aCount = 0
        tagValue        = 1
        cordBtn         = 0
        cordBtnHexTWO_Y = 0
        
        for j in 1 ..< arr_Categories.count
        {
            if ( j % 3 == 0 ) || ((j - 1) % 3 == 0) {
                
                aCount += 1
            }
        }
        
        var indexText = NSInteger()
        
        indexText = 0
        
        for i in 0 ..< aCount
        {
            if (i % 2 == 0) {
                loopCount = 2
                for j in 0 ..< loopCount {
                    print(j)
                    print(i)
                    //............. HEXAGON TWO TOP BUTTON CODE................
                    
                    let buttonHexaOne   = Hex(type: UIButtonType.Custom) as UIButton
                    
                    if IS_IPHONE6PLUS {
                        
                        buttonHexaOne.frame = CGRectMake(cordBtn, cordBtnHexTWO_Y, CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+36)
                    }
                    else
                    {
                        buttonHexaOne.frame = CGRectMake(cordBtn , cordBtnHexTWO_Y,CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+30)
                    }
                    
                    let btnBGcolor = hexStringToUIColor(arr_Categories.objectAtIndex(indexText).valueForKey("color_code") as! String)
                    
                    buttonHexaOne.backgroundColor = UIColor.randomAlpha()
                    buttonHexaOne.setTitle(arr_Categories .valueForKey("category_name") .objectAtIndex(indexText) as? String, forState: UIControlState.Normal)
                    
                    buttonHexaOne.titleLabel!.font =  UIFont(name: "ProximaNovaSoft-Bold", size: 18)
                    buttonHexaOne.contentEdgeInsets = UIEdgeInsetsMake(45, 0, 0, 0);
                    buttonHexaOne.setTitleColor(btnBGcolor, forState: UIControlState.Normal)
                    
                    buttonHexaOne.tag = tagValue
                    tagValue += 1
                    
                    let maskLayer: CAShapeLayer = CAShapeLayer()
                    maskLayer.fillRule = kCAFillRuleEvenOdd
                    maskLayer.frame = buttonHexaOne.bounds
                    let width: CGFloat = buttonHexaOne.frame.size.width
                    let height: CGFloat = buttonHexaOne.frame.size.height
                    let hPadding: CGFloat = 0
                    UIGraphicsBeginImageContext(buttonHexaOne.frame.size)
                    let path: UIBezierPath = UIBezierPath()
                    path.moveToPoint(CGPointMake(width / 2, 0))
                    path.addLineToPoint(CGPointMake(width - hPadding, height / 4))
                    path.addLineToPoint(CGPointMake(width - hPadding, height * 3 / 4))
                    path.addLineToPoint(CGPointMake(width / 2, height))
                    path.addLineToPoint(CGPointMake(hPadding, height * 3 / 4))
                    path.addLineToPoint(CGPointMake(hPadding, height / 4))
                    path.closePath()
                    path.fill()
                    maskLayer.path = path.CGPath
                    UIGraphicsEndImageContext()
                    buttonHexaOne.layer.mask = maskLayer
                    buttonHexaOne.addTarget(self, action: #selector(CategoriesVC.buttonHexaAction), forControlEvents: UIControlEvents.TouchUpInside)
                    
                    //.......CATEGORY TYPE IMAGE............
                    
                    let imageViewCat:UIImageView = UIImageView()
                    imageViewCat.frame = CGRect(x: buttonHexaOne.frame.size.width/2 - 19 , y: buttonHexaOne.frame.size.height/2 - 33, width: 40, height: 40)
                    buttonHexaOne.addSubview(imageViewCat)
                    
                    if arr_Categories .valueForKey("category_image") .objectAtIndex(indexText) as? String != "" {
                        let imageUrl = NSURL(string: (arr_Categories .valueForKey("category_image") .objectAtIndex(indexText)) as! String)
                        
                        imageViewCat.sd_setImageWithURL(imageUrl!, placeholderImage: nil)
                        
                    }
                    
                    if (arr_SavedUserCat .valueForKey("category_slug") ).count != 0
                    {
                        
                        if (arr_SavedUserCat .valueForKey("category_slug") ).containsObject(arr_Categories.objectAtIndex(indexText).valueForKey("category_slug")) {
                            
                            
                            let imageUrl = NSURL(string: (arr_Categories .valueForKey("category_selected_image") .objectAtIndex(indexText)) as! String)
                            
                            imageViewCat.sd_setImageWithURL(imageUrl!, placeholderImage: nil)
                            
                            buttonHexaOne.selected = true
                            buttonHexaOne.backgroundColor = btnBGcolor
                            
                            buttonHexaOne.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                        }
                    }
                    
                    scrollCategories.addSubview(buttonHexaOne)
                    cordBtn         = CGRectGetWidth(self.view.frame)/2
                    if IS_IPHONE6PLUS {
                        cordBtnHexONE_Y = buttonHexaOne.frame.maxY - 61
                    }
                        else if IS_IPHONE5
                    {
                        cordBtnHexONE_Y = buttonHexaOne.frame.maxY - 48
                    }
                    else
                    {
                        cordBtnHexONE_Y = buttonHexaOne.frame.maxY - 55
                    }
                    
                    indexText += 1
                }
            }
            else
            {
                let btnBGcolor2 = hexStringToUIColor(arr_Categories.objectAtIndex(indexText).valueForKey("color_code") as! String)
                
                let buttonHexaTwo   = Hex(type: UIButtonType.Custom) as UIButton
                
                if IS_IPHONE6PLUS {
                    buttonHexaTwo.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 104, cordBtnHexONE_Y , CGRectGetWidth(self.view.frame)/2 + 1, (CGRectGetWidth(self.view.frame)/2)+36)
                }
                    else if IS_IPHONE5
                {
                    buttonHexaTwo.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2-80, cordBtnHexONE_Y , CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+30)
                }
                else
                {
                    buttonHexaTwo.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2-94, cordBtnHexONE_Y , CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+30)
                }
                
                buttonHexaTwo.backgroundColor = UIColor.randomAlpha()
                buttonHexaTwo.setTitle(arr_Categories .valueForKey("category_name") .objectAtIndex(indexText) as? String, forState: UIControlState.Normal)
                
                buttonHexaTwo.titleLabel!.font =  UIFont(name: "ProximaNovaSoft-Bold", size: 18)
                buttonHexaTwo.contentEdgeInsets = UIEdgeInsetsMake(45, 0, 0, 0);
                buttonHexaTwo.setTitleColor(btnBGcolor2, forState: UIControlState.Normal)
                
                buttonHexaTwo.tag = tagValue
                tagValue += 1
                
                let maskLayer: CAShapeLayer = CAShapeLayer()
                maskLayer.fillRule = kCAFillRuleEvenOdd
                maskLayer.frame = buttonHexaTwo.bounds
                let width: CGFloat = buttonHexaTwo.frame.size.width
                let height: CGFloat = buttonHexaTwo.frame.size.height
                let hPadding: CGFloat = 0
                UIGraphicsBeginImageContext(buttonHexaTwo.frame.size)
                let path: UIBezierPath = UIBezierPath()
                path.moveToPoint(CGPointMake(width / 2, 0))
                path.addLineToPoint(CGPointMake(width - hPadding, height / 4))
                path.addLineToPoint(CGPointMake(width - hPadding, height * 3 / 4))
                path.addLineToPoint(CGPointMake(width / 2, height))
                path.addLineToPoint(CGPointMake(hPadding, height * 3 / 4))
                path.addLineToPoint(CGPointMake(hPadding, height / 4))
                path.closePath()
                path.fill()
                maskLayer.path = path.CGPath
                UIGraphicsEndImageContext()
                buttonHexaTwo.layer.mask = maskLayer
                buttonHexaTwo.addTarget(self, action: #selector(CategoriesVC.buttonHexaAction), forControlEvents: UIControlEvents.TouchUpInside)
                
                
                //************** CATEGORY IMAGE TWO ************************
                let imageViewCatTwo = UIImageView()
                if arr_Categories .valueForKey("category_image") .objectAtIndex(indexText) as? String != "" {
                    
                    imageViewCatTwo.frame = CGRect(x: buttonHexaTwo.frame.size.width/2 - 19 , y: buttonHexaTwo.frame.size.height/2 - 33, width: 40, height: 40)
                    
                    let imageUrl1 = NSURL(string: arr_Categories .valueForKey("category_image") .objectAtIndex(indexText) as! String)
                    
                    imageViewCatTwo.sd_setImageWithURL(imageUrl1!, placeholderImage: nil)
                    buttonHexaTwo.addSubview(imageViewCatTwo)
                }
                scrollCategories.addSubview(buttonHexaTwo)
                
                if (arr_SavedUserCat .valueForKey("category_slug") ).count != 0
                {
                    
                    if (arr_SavedUserCat .valueForKey("category_slug") ).containsObject(arr_Categories.objectAtIndex(indexText).valueForKey("category_slug")) {
                        
                        let imageUrl1 = NSURL(string: arr_Categories .valueForKey("category_selected_image") .objectAtIndex(indexText) as! String)
                        
                        imageViewCatTwo.sd_setImageWithURL(imageUrl1!, placeholderImage: nil)
                        
                        buttonHexaTwo.selected = true
                        buttonHexaTwo.backgroundColor = btnBGcolor2
                        
                        buttonHexaTwo.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                    }
                }
                
                indexText += 1
                
                let buttonHexa3   = Hex(type: UIButtonType.Custom) as UIButton
                
                if IS_IPHONE6PLUS {
                    
                    buttonHexa3.frame = CGRectMake(buttonHexa3.frame.minX - 104, cordBtnHexONE_Y , CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+36)
                }
                    else if IS_IPHONE5
                {
                    buttonHexa3.frame = CGRectMake(buttonHexa3.frame.minX - 80, cordBtnHexONE_Y , CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+30)
                }
                else
                {
                    buttonHexa3.frame = CGRectMake(buttonHexa3.frame.minX - 94, cordBtnHexONE_Y , CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+30)
                }
                buttonHexa3.backgroundColor = UIColor.randomAlphaDemo()
                let maskLayer3: CAShapeLayer = CAShapeLayer()
                maskLayer3.fillRule = kCAFillRuleEvenOdd
                maskLayer3.frame = buttonHexa3.bounds
                let width3: CGFloat = buttonHexa3.frame.size.width
                let height3: CGFloat = buttonHexa3.frame.size.height
                let hPadding3: CGFloat = 0
                UIGraphicsBeginImageContext(buttonHexa3.frame.size)
                let path3: UIBezierPath = UIBezierPath()
                path3.moveToPoint(CGPointMake(width3 / 2, 0))
                path3.addLineToPoint(CGPointMake(width3 - hPadding3, height3 / 4))
                path3.addLineToPoint(CGPointMake(width3 - hPadding3, height3 * 3 / 4))
                path3.addLineToPoint(CGPointMake(width3 / 2, height3))
                path3.addLineToPoint(CGPointMake(hPadding3, height3 * 3 / 4))
                path3.addLineToPoint(CGPointMake(hPadding3, height3 / 4))
                path3.closePath()
                path3.fill()
                maskLayer3.path = path3.CGPath
                UIGraphicsEndImageContext()
                buttonHexa3.layer.mask = maskLayer3
                scrollCategories.addSubview(buttonHexa3)
                
                let buttonHexa4   = Hex(type: UIButtonType.Custom) as UIButton
                
                if IS_IPHONE6PLUS {
                    buttonHexa4.frame = CGRectMake(buttonHexaTwo.frame.maxX , cordBtnHexONE_Y , CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+36)
                }
                else
                {
                    buttonHexa4.frame = CGRectMake(buttonHexaTwo.frame.maxX , cordBtnHexONE_Y , CGRectGetWidth(self.view.frame)/2, (CGRectGetWidth(self.view.frame)/2)+30)
                }
                buttonHexa4.backgroundColor = UIColor.randomAlphaDemo()
                let maskLayer4: CAShapeLayer = CAShapeLayer()
                maskLayer4.fillRule = kCAFillRuleEvenOdd
                maskLayer4.frame = buttonHexa4.bounds
                let width4: CGFloat = buttonHexa4.frame.size.width
                let height4: CGFloat = buttonHexa4.frame.size.height
                let hPadding4: CGFloat = 0
                UIGraphicsBeginImageContext(buttonHexa4.frame.size)
                let path4: UIBezierPath = UIBezierPath()
                path4.moveToPoint(CGPointMake(width4 / 2, 0))
                path4.addLineToPoint(CGPointMake(width4 - hPadding4, height4 / 4))
                path4.addLineToPoint(CGPointMake(width4 - hPadding4, height4 * 3 / 4))
                path4.addLineToPoint(CGPointMake(width4 / 2, height4))
                path4.addLineToPoint(CGPointMake(hPadding4, height4 * 3 / 4))
                path4.addLineToPoint(CGPointMake(hPadding4, height4 / 4))
                path4.closePath()
                path4.fill()
                maskLayer4.path = path4.CGPath
                UIGraphicsEndImageContext()
                buttonHexa4.layer.mask = maskLayer4
                scrollCategories.addSubview(buttonHexa4)
                
                if IS_IPHONE6PLUS {
                    cordBtnHexTWO_Y = buttonHexaTwo.frame.maxY - 61
                }
                    else if IS_IPHONE5
                {
                    cordBtnHexTWO_Y = buttonHexaTwo.frame.maxY - 48
                }
                else{
                    cordBtnHexTWO_Y = buttonHexaTwo.frame.maxY - 55
                }
                cordBtn = 0
            }
        }
        
        let doneButton   = UIButton.init(type: UIButtonType.Custom) as UIButton
        doneButton.frame = CGRectMake(self.view.frame.minX + 20, cordBtnHexONE_Y + 250,self.view.frame.size.width - 40 , 55)
        doneButton.backgroundColor    = UIColor .clearColor()
        doneButton.setTitle("Done!", forState: UIControlState.Normal)
        doneButton.titleLabel?.font   = UIFont (name: "ProximaNovaSoft-Semibold", size: 22)
        doneButton.setTitleColor(UIColor.init(red: 0/255.0, green: 155/255.0, blue: 251/255.0, alpha: 1.0), forState: UIControlState.Normal)
        doneButton.layer.cornerRadius = 28
        doneButton.layer.borderWidth  = 2
        doneButton.layer.borderColor  = UIColor.init(red: 0/255.0, green: 155/255.0, blue: 251/255.0, alpha: 1.0).CGColor
        doneButton.addTarget(self, action: #selector(CategoriesVC.donebuttonAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        scrollCategories.addSubview(doneButton)
    }
    
    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollCategories.contentSize=CGSizeMake(self.view.frame.size.width, cordBtnHexONE_Y
            + 330)
    }
    
    @IBAction func buttonHexaAction(sender:UIButton!) {
        let btnsendtag:UIButton = sender
        
        var intIndex = NSInteger()
        
        intIndex = sender.tag
        
        let indexPath = NSIndexPath(forRow: intIndex - 1, inSection: 0)
        
        var imageViewChangeImage  = UIImageView()
        
        for subviews in btnsendtag.subviews {
            if subviews.isKindOfClass(UIImageView) {
                imageViewChangeImage = subviews as! UIImageView
            }
        }
        
        if btnsendtag.selected == false {
            
            let colorBtn = hexStringToUIColor((arr_Categories .valueForKey("color_code") .objectAtIndex(indexPath.row)) as! String)
            
            btnsendtag.backgroundColor = colorBtn
            
            btnsendtag.selected = true
            
            let imageUrl1 = NSURL(string: arr_Categories .valueForKey("category_selected_image") .objectAtIndex(indexPath.row) as! String)
            
            imageViewChangeImage.sd_setImageWithURL(imageUrl1!, placeholderImage: nil)
            
            btnsendtag.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            
            arr_SaveCatID .addObject ((arr_Categories .valueForKey("id") .objectAtIndex(indexPath.row)) as! String)
        }
        else
        {
            let color1 = hexStringToUIColor((arr_Categories .valueForKey("color_code") .objectAtIndex(indexPath.row)) as! String)
            
            btnsendtag.backgroundColor = UIColor.randomAlpha()
            btnsendtag.setTitleColor(color1, forState: UIControlState.Normal)
            btnsendtag.selected = false
            
            let imageUrl1 = NSURL(string: arr_Categories .valueForKey("category_image") .objectAtIndex(indexPath.row) as! String)
            
            imageViewChangeImage.sd_setImageWithURL(imageUrl1!, placeholderImage: nil)
            
            arr_SaveCatID .removeObject((arr_Categories .valueForKey("id") .objectAtIndex(indexPath.row)) as! String)
            
            let string_CatSlug = arr_Categories .valueForKey("category_slug") .objectAtIndex(indexPath.row) as! String
            
            let string_Id = arr_Categories .valueForKey("id") .objectAtIndex(indexPath.row) as! String
            
            let dict_removeObj: NSDictionary = ["category_slug":string_CatSlug, "id":string_Id]
            
            if (arr_SavedUserCat .valueForKey("category_slug") ).count != 0
            {
                if (arr_SavedUserCat .valueForKey("category_slug") ).containsObject(arr_Categories.objectAtIndex(indexPath.row).valueForKey("category_slug")) {
                    arr_SavedUserCat .removeObject(dict_removeObj)
                }
            }
        }
        
    }
    
    // MARK: - DONE BUTTON ACTION
    
    @IBAction func donebuttonAction(sender:UIButton!) {
        
        if arr_SavedUserCat.count != 0 {
            
            arr_SaveCatID .addObjectsFromArray(arr_SavedUserCat .valueForKey("id") as! [AnyObject])
            
            let array = arr_SaveCatID
            let intArray = array.map { Int($0 as! String)!}
            
            arr_SaveCatID .removeAllObjects()
            
            arr_SaveCatID .addObjectsFromArray(intArray)
        }
        
        if arr_SaveCatID.count == 0 {
            
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please select categories" as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
            self.saveCategories()
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - BACK BUTTON ACTION
    
    @IBAction func btnBackAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - BUTTON SKIP CATEGORIES ACTION METHOD
    
    @IBAction func btnSkipCategories(sender: AnyObject) {
        
        let questionVCbj = self.storyboard?.instantiateViewControllerWithIdentifier("QuestionVC")as! QuestionVC
        self.navigationController?.pushViewController(questionVCbj, animated: true)
    }
    
    //MARK: - GET CATEGORIES API METHOD
    
    func getCategoriesAPImethod()  {
        
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
           
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = [ "result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                
                let params: [String: AnyObject] = ["user_details":jsonString]
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KGetCategories)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    //MARK: < slider delegate for showing left and right menu >
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    ////////////////////////*************//////////////////////////////////
    
    // MARK: - HEXAGON BUTTON METHOD
    
    func saveCategories() {
        
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = [ "result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "your_categories": arr_SaveCatID]
            
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KSaveCategories)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        //AppManager.sharedManager.hideHUD()
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KGetCategories)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                
                arr_Categories .addObjectsFromArray(responseDict .valueForKey("categories")! as! [AnyObject])
                
                arr_SavedUserCat .addObjectsFromArray(responseDict .valueForKey("yourCategories")! as AnyObject as! [AnyObject])
                
                if arr_Categories.count>0 {
                    userDefaults .removeObjectForKey("arr_categories")
                    userDefaults .setObject(arr_Categories, forKey: "arr_categories")
                    self.hexagonButtonCategories()
                }
            }
                
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if serviceurl .isEqualToString(Header.KSaveCategories)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                let questionVCbj = self.storyboard?.instantiateViewControllerWithIdentifier("QuestionVC")as! QuestionVC
                self.navigationController?.pushViewController(questionVCbj, animated: true)
            }
                
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            
        }
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        //AppManager.sharedManager.hideHUD()
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }
    
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
    
    if (cString.hasPrefix("#")) {
        cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
    }
    
    if ((cString.characters.count) != 6) {
        return UIColor.grayColor()
    }
    
    var rgbValue:UInt32 = 0
    NSScanner(string: cString).scanHexInt(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
