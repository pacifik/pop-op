//
//  Hex.m
//  Hexagon
//
//  Created by Raman Kant on 5/18/16.
//
//

#import "Hex.h"
#import <QuartzCore/QuartzCore.h>

@implementation Hex

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
       // [self configureLayerForHexagon];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    if ((self = [super initWithCoder:coder])) {
        [self configureLayerForHexagon];
    }
    return self;
}

- (void)configureLayerForHexagon{
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.fillRule      = kCAFillRuleEvenOdd;
    maskLayer.frame         = self.bounds;
    CGFloat       width     = self.frame.size.width;
    CGFloat       height    = self.frame.size.height;
    CGFloat       hPadding  = width * 1 / 8 / 2;
    UIGraphicsBeginImageContext(self.frame.size);
    UIBezierPath * path     = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(width/2, 0)];
    [path addLineToPoint:CGPointMake(width - hPadding, height / 4)];
    [path addLineToPoint:CGPointMake(width - hPadding, height * 3 / 4)];
    [path addLineToPoint:CGPointMake(width / 2, height)];
    [path addLineToPoint:CGPointMake(hPadding, height * 3 / 4)];
    [path addLineToPoint:CGPointMake(hPadding, height / 4)];
    [path closePath];
    [path fill];
    maskLayer.path  = path.CGPath;
    UIGraphicsEndImageContext();
    self.layer.mask = maskLayer;
}

- (UIColor *)colorOfPoint:(CGPoint)point
{
    unsigned char   pixel[4]      = {0};
    CGColorSpaceRef colorSpace  = CGColorSpaceCreateDeviceRGB();
    CGContextRef    context        = CGBitmapContextCreate(pixel, 1, 1, 8, 4, colorSpace, kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    CGContextTranslateCTM(context, -point.x, -point.y);
    [self.layer renderInContext:context];
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    UIColor         * color = [UIColor colorWithRed:pixel[0]/255.0 green:pixel[1]/255.0 blue:pixel[2]/255.0 alpha:pixel[3]/255.0];
    return color;
}

#pragma mark - Hit testing -

- (BOOL)isAlphaVisibleAtPoint:(CGPoint)point forButton:(UIButton *)button{
    
    CGSize iSize    = button.frame.size;
    CGSize bSize    = self.bounds.size;
    point.x         *= (bSize.width != 0) ? (iSize.width / bSize.width) : 1;
    point.y         *= (bSize.height != 0) ? (iSize.height / bSize.height) : 1;
    UIColor         * pixelColor = [self colorOfPoint:point];
    CGFloat alpha   = 0.0;
    if ([pixelColor respondsToSelector:@selector(getRed:green:blue:alpha:)])
        [pixelColor getRed:NULL green:NULL blue:NULL alpha:&alpha];
    else {
        CGColorRef cgPixelColor = [pixelColor CGColor];
        alpha = CGColorGetAlpha(cgPixelColor);
    }
    return alpha >= kAlphaVisibleThreshold;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    
    BOOL superResult = [super pointInside:point withEvent:event];
    if (!superResult) {
        return superResult;
    }
    if (CGPointEqualToPoint(point, self.previousTouchPoint)) {
        return self.previousTouchHitTestResponse;
    } else {
        self.previousTouchPoint         = point;
    }
    BOOL response   = NO;
    response        = [self isAlphaVisibleAtPoint:point forButton:self];
    self.previousTouchHitTestResponse   = response;
    return response;
}

@end
