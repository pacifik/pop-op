//
//  LoginVC.swift
//  PopOn
//
//  Created by Nitin Suri on 12/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import SwiftLoader

class LoginVC: UIViewController,WebServiceDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var textUname: UITextField!
    @IBOutlet weak var btnFbLoginOutlet: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var textPwd: UITextField!
    
    var socialFB_ID   = String()
    var string_url    = String()
    var stringFB_Name = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        
        textUname.attributedPlaceholder = NSAttributedString(string:"User Name",
                                                                attributes:[NSForegroundColorAttributeName: UIColor.init(red: 41/255, green: 72/255.0, blue: 114/255.0, alpha: 1.0)])
        
        textPwd.attributedPlaceholder = NSAttributedString(string:"Password",
                                                           attributes:[NSForegroundColorAttributeName: UIColor.init(red: 41/255, green: 72/255.0, blue: 114/255.0, alpha: 1.0)])
        
        // Do any additional setup after loading the view.
        btnSignUp.backgroundColor = UIColor.clearColor()
        btnSignUp.layer.cornerRadius = 28
        btnSignUp.layer.borderWidth = 2
        btnSignUp.layer.borderColor = UIColor.init(red: 0/255.0, green: 155/255.0, blue: 251/255.0, alpha: 1.0).CGColor
        
        btnFbLoginOutlet.layer.cornerRadius = 28
        
       userDefaults .removeObjectForKey("profile_image")
       userDefaults .removeObjectForKey("user_name")
       userDefaults .removeObjectForKey("user_city")
       userDefaults .removeObjectForKey("state")
       userDefaults .removeObjectForKey("phone")
       userDefaults .removeObjectForKey("dob")
       userDefaults .removeObjectForKey("gender")
       userDefaults .removeObjectForKey("email")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - TEXT FIELD DELEGATE
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    // MARK: - Button ACTION FORGOT PASSWORD
    
    @IBAction func btnForgotPwd(sender: AnyObject) {
        //ForgotPasswordVC
        let forgotPwdObj = storyboard?.instantiateViewControllerWithIdentifier("ForgotPasswordVC")as!ForgotPasswordVC
        self.navigationController?.pushViewController(forgotPwdObj, animated: true)
    }
    
    // MARK: - BUTTON ACTION SIGN IN
    
    @IBAction func btnSignIn(sender: AnyObject) {
        
        let trimNameString = textUname.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let trimPasswordString = textPwd.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        if(trimNameString.characters.count == 0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter the user name." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else if(trimPasswordString.characters.count == 0)
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter the password." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        else
        {
                if trimPasswordString.characters.count >= 6
                {
                    if AppManager.NetWorkReachability.isConnectedToNetwork() == true
                    {
                        AppManager.sharedManager.delegate = self
                        SwiftLoader.show(animated: true)
                        
                        //let userDefaults = NSUserDefaults.standardUserDefaults()
                        let deviceToken=userDefaults.valueForKey("Device_Token");
                        
                        let dict_User_Info : NSDictionary = [ "result": true, "user_name": trimNameString, "password": trimPasswordString, "email": "" , "device_token": deviceToken!, "device_type": "ios"]
                        do
                        {
                            let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                            
                            let params: [String: AnyObject] = ["user_details":jsonString]
                            //print(params)
                            
                            AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KLogin)
                        }
                        
                    }
                    else
                    {
                        let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                        }
                        alertViewShow.addAction(okAction)
                        self.presentViewController(alertViewShow, animated: true, completion: nil)
                    }
                }
                else
                {
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Password must be at least 6 characters." as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
                
            }
}
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KLogin)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKeyPath("id"), forKey: "UserIdSave")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("access_token"), forKey: "access_token")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("email"), forKey: "email")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("phone"), forKey: "phone")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("dob"), forKey: "dob")
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("gender"), forKey: "gender")
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_full_name")
                
                if responseDict .objectForKey("user_details")! .valueForKey("user_name") as? String != nil {
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("user_name"), forKey: "user_name")
                }
                else
                {
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_name")
                }
                
                if responseDict .objectForKey("user_details")! .valueForKey("profile_image") as? String != nil {
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("profile_image"), forKey: "profile_image")
                }
                
                if responseDict .objectForKey("user_details")! .valueForKey("city") as? String != nil {
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("city"), forKey: "user_city")
                }
                if responseDict .objectForKey("user_details")! .valueForKey("state") as? String != nil {
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("state"), forKey: "state")
                }
                
                userDefaults .setValue("yes", forKey: "login")
                
                let ageCheckNum = responseDict .objectForKey("user_details")! .valueForKey("age") as! String
                
                let typeCatAge = NSInteger(ageCheckNum)
                
                if typeCatAge == 0 {
                    
                    userDefaults .setValue("no", forKey: "login1")
                    let UserProfileVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("UserProfileVC")as! UserProfileVC
                    self.navigationController?.pushViewController(UserProfileVCObj, animated: true)
                }
                else
                {
                    NSNotificationCenter.defaultCenter().postNotificationName("userDetailsUpdated", object: nil)
                    NSNotificationCenter.defaultCenter().postNotificationName("imageUpdated", object: nil)
                    userDefaults .setValue("yes", forKey: "login1")
                    let questionsVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("QuestionVC")as! QuestionVC
                    self.navigationController?.pushViewController(questionsVCObj, animated: true)
                }
                
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if serviceurl .isEqualToString(Header.KFBlogin)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
            userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKeyPath("id"), forKey: "UserIdSave")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("access_token"), forKey: "access_token")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("email"), forKey: "email")
                
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("phone"), forKey: "phone")
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_full_name")
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_name")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("dob"), forKey: "dob")
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("gender"), forKey: "gender")
                
                if responseDict .objectForKey("user_details")! .valueForKey("profile_image") as? String != nil {
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("profile_image"), forKey: "profile_image")
                }
                
                if responseDict .objectForKey("user_details")! .valueForKey("city") as? String != nil {
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("city"), forKey: "user_city")
                }
                if responseDict .objectForKey("user_details")! .valueForKey("state") as? String != nil {
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("state"), forKey: "state")
                }
                
                userDefaults .setValue("yes", forKey: "login")
                
                let ageCheckNum = responseDict .objectForKey("user_details")! .valueForKey("age") as! String
                
                let typeCatAge = NSInteger(ageCheckNum)
                
                if typeCatAge == 0 {
                    
                    userDefaults .setValue("no", forKey: "login1")
                    let UserProfileVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("UserProfileVC")as! UserProfileVC
                    self.navigationController?.pushViewController(UserProfileVCObj, animated: true)
                }
                else
                {
                    NSNotificationCenter.defaultCenter().postNotificationName("userDetailsUpdated", object: nil)
                    NSNotificationCenter.defaultCenter().postNotificationName("imageUpdated", object: nil)
                    userDefaults .setValue("yes", forKey: "login1")
                    let questionsVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("QuestionVC")as! QuestionVC
                    self.navigationController?.pushViewController(questionsVCObj, animated: true)
                }
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if serviceurl .isEqualToString(Header.KVerifyEmail)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKeyPath("id"), forKey: "UserIdSave")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("access_token"), forKey: "access_token")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("email"), forKey: "email")
                
                userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("phone"), forKey: "phone")
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_full_name")
                
                userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_name")
                
                
                if responseDict .objectForKey("user_details")! .valueForKey("profile_image") as? String != nil {
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("profile_image"), forKey: "profile_image")
                }
                
                if responseDict .objectForKey("user_details")! .valueForKey("city") as? String != nil {
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("city"), forKey: "user_city")
                }
                if responseDict .objectForKey("user_details")! .valueForKey("state") as? String != nil {
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("state"), forKey: "state")
                }
            }
        }
            else if serviceurl .isEqualToString(Header.KCheckEmailFB)
            {
                if responseDict .valueForKey("result")! .boolValue == true
                {
                    userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKeyPath("id"), forKey: "UserIdSave")
                    
                    userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("access_token"), forKey: "access_token")
                    
                    userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("email"), forKey: "email")
                    
                    userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("phone"), forKey: "phone")
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_full_name")
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("full_name"), forKey: "user_name")
                    
                    userDefaults.setObject(responseDict .objectForKey("user_details")! .valueForKey("dob"), forKey: "dob")
                    
                    userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("gender"), forKey: "gender")
                    
                    if responseDict .objectForKey("user_details")! .valueForKey("profile_image") as? String != nil {
                        
                        userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("profile_image"), forKey: "profile_image")
                    }
                    
                    if responseDict .objectForKey("user_details")! .valueForKey("city") as? String != nil {
                        
                        userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("city"), forKey: "user_city")
                    }
                    if responseDict .objectForKey("user_details")! .valueForKey("state") as? String != nil {
                        userDefaults .setObject(responseDict .objectForKey("user_details")! .valueForKey("state"), forKey: "state")
                    }
                    
                    userDefaults .setValue("yes", forKey: "login")
                    
                    let ageCheckNum = responseDict .objectForKey("user_details")! .valueForKey("age") as! String
                    
                    let typeCatAge = NSInteger(ageCheckNum)
                    
                    if typeCatAge == 0 {
                        
                        userDefaults .setValue("no", forKey: "login1")
                        let UserProfileVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("UserProfileVC")as! UserProfileVC
                        self.navigationController?.pushViewController(UserProfileVCObj, animated: true)
                    }
                    else
                    {
                  NSNotificationCenter.defaultCenter().postNotificationName("userDetailsUpdated", object: nil)
                        NSNotificationCenter.defaultCenter().postNotificationName("imageUpdated", object: nil)
                        userDefaults .setValue("yes", forKey: "login1")
                        let questionsVCObj = self.storyboard?.instantiateViewControllerWithIdentifier("QuestionVC")as! QuestionVC
                        self.navigationController?.pushViewController(questionsVCObj, animated: true)
                    }
                }
               else if responseDict .valueForKey("result")! .boolValue == false
               {
                   let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                   
                   let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                       UIAlertAction in
                    self.verifyEmailID()
                    
                   }
                   alertViewShow.addAction(okAction)
                   self.presentViewController(alertViewShow, animated: true, completion: nil)
               }
        }
        else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
    
    func failureRsponseError(failureError:NSError)
    {
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }
    
    // MARK: - BUTTON ACTION SIGN UP
    
    @IBAction func btnSignUp(sender: AnyObject) {
        
        let signupObj = storyboard?.instantiateViewControllerWithIdentifier("SignUpVC")as! SignUpVC
        self.navigationController?.pushViewController(signupObj, animated: true)
    }
    
    // MARK: - BUTTON FACEBOOK LOGIN ACTION METHOD
    
    @IBAction func btnFacebook(sender: AnyObject) {
        //self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends"]
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        //"me?fields=id,name,friends"
        fbLoginManager.logInWithReadPermissions(["public_profile","email","user_friends"], fromViewController: self, handler: { (result, error) -> Void in
            //print("User Logged In")
            
            if ((error) != nil)
            {
                // Process error
            }
            else if result.isCancelled
            {
                // Handle cancellations
            }
            else
            {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if result.grantedPermissions.contains("email")
                {
                    //print(result);
                    self.getFBUserData()
                    fbLoginManager.logOut()
                    // Do work
                }
            }
        })
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!)
    {
        print("User Logged Out")
    }
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
            }
            else
            {
                let printuserName : NSString = result.valueForKey("name") as! NSString
                print(printuserName)
                
                let userEmail : NSString = result.valueForKey("email") as! NSString
                print(userEmail)
            }
        })
    }
    func getFBUserData()
    {
        if((FBSDKAccessToken.currentAccessToken()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,friends"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil)
                {
                    if AppManager.NetWorkReachability.isConnectedToNetwork()==true
                    {
                        AppManager.sharedManager.delegate=self
                        
                        SwiftLoader.show(animated: true)
                        
                        //print(result)
                        
                        //print(result.valueForKey("friends")?.valueForKey("data")!.count)
                        
                       //arrayFBdata.sharedInstanceArrFB = result.valueForKey("friends")?.valueForKey("data") as! NSMutableArray
                        
                        let url = NSURL(string:result .valueForKey("picture")?.valueForKey("data")?.valueForKey("url") as! String)
                        
                        self.string_url = url!.absoluteString
                        
                        self.socialFB_ID = result .valueForKey("id") as! String
                        
                        self.stringFB_Name    = result .valueForKey("name") as! String
                        
                        if (result .valueForKey("email") != nil)
                        {
                            do
                            {
                                let deviceToken=userDefaults.valueForKey("Device_Token");
                                
                                let dict_User_Info : NSDictionary = [ "result": true, "social_id": result .valueForKey("id")! , "full_name": result .valueForKey("name")!, "email": result .valueForKey("email")! , "device_token": deviceToken!, "device_type": "ios","image":self.string_url]
                                
                                //print(dict_User_Info)
                                do
                                {
                                    let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                                    let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                                    
                                    let params: [String: AnyObject] = ["user_details":jsonString]
                                    AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KFBlogin)
                                }
                                
                            }
                            
                        }
                        else
                        {
                            self.checkEmailFB()
                        }
                        
                        }
                    else
                    {
                        let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                        }
                        alertViewShow.addAction(okAction)
                        self.presentViewController(alertViewShow, animated: true, completion: nil)
                    }
                    
                }
                else
                {
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Facebook error" as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
            })
        }
    }
    
    func checkEmailFB() -> Void {
        
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            
            SwiftLoader.show(animated: true)
            
            //let userDefaults = NSUserDefaults.standardUserDefaults()
            let deviceToken=userDefaults.valueForKey("Device_Token");
            
            let dict_User_Info : NSDictionary = ["result": true, "social_id": socialFB_ID, "device_token": deviceToken!, "device_type": "ios"]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                
                let params: [String: AnyObject] = ["user_details":jsonString]
                print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KCheckEmailFB)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
        
    }

    func verifyEmailID() -> Void {
        
        var loginTextField: UITextField?
        let alertController = UIAlertController(title: "Facebook login", message: "Enter email", preferredStyle: .Alert)
        let ok = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
            let trimEmailString=loginTextField!.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
            
            if(trimEmailString.characters.count==0)
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Please enter the email." as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
            else
            {
                if !AppManager.sharedManager.isValidEmail(trimEmailString)
                {
                    let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "E-mail is invalid." as String, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
                else
                {
                    do
                    {
                        let deviceToken = userDefaults.valueForKey("Device_Token");
                        
                        let dict_User_Info : NSDictionary = [ "result": true, "social_id": self.socialFB_ID , "full_name": self.stringFB_Name, "email": (loginTextField?.text)! , "device_token": deviceToken!, "device_type": "ios","image":self.string_url]
                        
                        //print(dict_User_Info)
                        do
                        {
                            let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                            
                            let params: [String: AnyObject] = ["user_details":jsonString]
                            AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KVerifyEmail)
                        }
                        
                    }
                }
            }
        })
        let cancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in
            print("Cancel Button Pressed")
        }
        alertController.addAction(ok)
        alertController.addAction(cancel)
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            // Enter the textfiled customization code here.
            loginTextField = textField
            loginTextField?.delegate = self
            loginTextField?.placeholder = "Enter your email id"
        }
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
}
