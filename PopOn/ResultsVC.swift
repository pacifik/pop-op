//
//  ResultsVC.swift
//  PopOn
//
//  Created by Nitin Suri on 01/06/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit

import SwiftCharts
import SwiftLoader

class ResultsVC: UIViewController,WebServiceDelegate {

    @IBOutlet weak var btnGenderOutlet: UIButton!
    @IBOutlet weak var btnRigthSlider: UIButton!
    @IBOutlet weak var btnResultOutlet: UIButton!
    @IBOutlet weak var btnChoice2: UIButton!
    @IBOutlet weak var btnChoice1: UIButton!
    @IBOutlet weak var viewGraphBG: UIView!
    
    var arr_avgResult = NSMutableArray()
    
    private var chart: Chart?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        btnRigthSlider.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        btnChoice2.layer.cornerRadius = 22
        btnChoice2.layer.borderWidth = 2
        btnChoice2.layer.borderColor = UIColor.init(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0).CGColor
        
        btnChoice1.layer.cornerRadius = 22
        btnChoice1.layer.borderWidth = 2
        btnChoice1.layer.borderColor = UIColor.init(red: 235/255.0, green: 76/255.0, blue: 85/255.0, alpha: 1.0).CGColor
        
        self.avgResultsAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBackAction(sender: AnyObject) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(PollsVC) || controller.isKindOfClass(ProfileFollowerVC) || controller.isKindOfClass(MyProfileVC) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }

    @IBAction func btnResultAction(sender: AnyObject) {
        
        let results_Gender = self.storyboard?.instantiateViewControllerWithIdentifier("GenderVC")as!
        GenderVC
        self.navigationController?.pushViewController(results_Gender, animated: false)
    }
    
    @IBAction func btnGenderAction(sender: AnyObject) {
        
        let results_Age = self.storyboard?.instantiateViewControllerWithIdentifier("AgeVC")as!
        AgeVC
        self.navigationController?.pushViewController(results_Age, animated: false)
    }
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    // MARK: - HEXAGON BUTTON METHOD
    
    func avgResultsAPI() {
        
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = [ "result": true, "user_id": userDefaults .valueForKey("UserIdSave")!, "poll_id": userDefaults .valueForKey("poll_id")!,"access_token": userDefaults .valueForKey("access_token")!]
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KResultsAge)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KResultsAge)
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_avgResult .removeAllObjects()
                arr_avgResult .addObject(responseDict .objectForKey("avgResult")!)
                
                let emojiChoice2 = arr_avgResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("answer")! as? String
                let dataEmojiChoice2: NSData = emojiChoice2!.dataUsingEncoding(NSUTF8StringEncoding)!
                let strEmojiHated2: String = String(data: dataEmojiChoice2, encoding: NSNonLossyASCIIStringEncoding)!
                
                btnChoice2 .setTitle(strEmojiHated2, forState: UIControlState .Normal)
            
                let emojiChoice1 = arr_avgResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("answer")! as? String
                let dataEmojiChoice1: NSData = emojiChoice1!.dataUsingEncoding(NSUTF8StringEncoding)!
                let strEmojiHated1: String = String(data: dataEmojiChoice1, encoding: NSNonLossyASCIIStringEncoding)!
                btnChoice1 .setTitle(strEmojiHated1, forState: UIControlState .Normal)
                
                //..........................**  GRAPH **................................
                
                let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
                
                let chartPoints = [(17, arr_avgResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("choice2age17VotesPer")! as! NSInteger), (25, arr_avgResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("choice2age18_24VotesPer")! as! NSInteger), (33, arr_avgResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("choice2age25_29VotesPer")! as! NSInteger), (41, arr_avgResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("choice2age30_39VotesPer")! as! NSInteger), (50, arr_avgResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("choice2age40_49VotesPer")! as! NSInteger), (58, arr_avgResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("choice2age50_64VotesPer")! as! NSInteger), (65, arr_avgResult .objectAtIndex(0) .valueForKey("choice2")! .valueForKey("choice2age65VotesPer")! as! NSInteger)].map{ChartPoint(x: ChartAxisValueInt($0.0), y: ChartAxisValueInt($0.1))}
                
                
                let xValues = ChartAxisValuesGenerator.generateXAxisValuesWithChartPoints(chartPoints, minSegmentCount: 17, maxSegmentCount: 65, multiple: 2, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings)}, addPaddingSegmentIfEdge: false)
                let yValues = ChartAxisValuesGenerator.generateYAxisValuesWithChartPoints(chartPoints, minSegmentCount: 0, maxSegmentCount: 100, multiple: 2, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings)}, addPaddingSegmentIfEdge: true)
                
                let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Axis title", settings: labelSettings))
                let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "Axis title", settings: labelSettings.defaultVertical()))
                let chartFrame = ExamplesDefaults.chartFrame(self.view.bounds)
                
                let chartSettings = ExamplesDefaults.chartSettings
                chartSettings.leading  = -28
                chartSettings.trailing = 65
                chartSettings.labelsToAxisSpacingX = 20
                chartSettings.labelsToAxisSpacingY = 15
                let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
                let (xAxis, yAxis, innerFrame) = (coordsSpace.xAxis, coordsSpace.yAxis, coordsSpace.chartInnerFrame)
                
                let showCoordsTextViewsGenerator = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
                    let (chartPoint, screenLoc) = (chartPointModel.chartPoint, chartPointModel.screenLoc)
                    let w: CGFloat = 20
                    let h: CGFloat = 10
                    
                    let text = "(\(chartPoint.x), \(chartPoint.y))"
                    let font = ExamplesDefaults.labelFont
                    let textSize = ChartUtils.textSize(text, font: font)
                    let x = min(screenLoc.x + 5, chart.bounds.width - textSize.width - 5)
                    let view = UIView(frame: CGRectMake(x, screenLoc.y - h, w, h))
                    let label = UILabel(frame: view.bounds)
                    label.text = "(\(chartPoint.x), \(chartPoint.y))"
                    label.font = ExamplesDefaults.labelFont
                    view.addSubview(label)
                    view.alpha = 0
                    UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                        view.alpha = 1
                        }, completion: nil)
                    return view
                }
                
                let showCoordsLinesLayer = ChartShowCoordsLinesLayer<ChartPoint>(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: chartPoints)
                let showCoordsTextLayer = ChartPointsSingleViewLayer<ChartPoint, UIView>(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: chartPoints, viewGenerator: showCoordsTextViewsGenerator)
                
                let touchViewsGenerator = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
                    let (chartPoint, screenLoc) = (chartPointModel.chartPoint, chartPointModel.screenLoc)
                    let s: CGFloat = 30
                    let view = HandlingView(frame: CGRectMake(screenLoc.x - s/2, screenLoc.y - s/2, s, s))
                    view.touchHandler = {[weak showCoordsLinesLayer, weak showCoordsTextLayer, weak chartPoint, weak chart] in
                        guard let chartPoint = chartPoint, chart = chart else {return}
                        showCoordsLinesLayer?.showChartPointLines(chartPoint, chart: chart)
                        showCoordsTextLayer?.showView(chartPoint: chartPoint, chart: chart)
                    }
                    return view
                }
                
                let touchLayer = ChartPointsViewsLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: chartPoints, viewGenerator: touchViewsGenerator)
                
                let lineModel = ChartLineModel(chartPoints: chartPoints, lineColor: UIColor(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0), lineWidth: 2, animDuration: 2, animDelay: 0)
                let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, lineModels: [lineModel])
                
                let circleViewGenerator = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
                    let circleView = ChartPointEllipseView(center: chartPointModel.screenLoc, diameter: 20)
                    circleView.animDuration = 2
                    circleView.fillColor = UIColor.clearColor()
                    circleView.borderWidth = 2
                    circleView.borderColor = UIColor.init(red: 139/255.0, green: 255/255.0, blue: 40/255.0, alpha: 1.0)
                    return circleView
                }
                let chartPointsCircleLayer = ChartPointsViewsLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: chartPoints, viewGenerator: circleViewGenerator, displayDelay: 0, delayBetweenItems: 0.05)
                
                let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.clearColor(), linesWidth: ExamplesDefaults.guidelinesWidth)
                let guidelinesLayer = ChartGuideLinesDottedLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, settings: settings)
                
                let chart = Chart(
                    frame: chartFrame,
                    layers: [
                        xAxis,
                        yAxis,
                        guidelinesLayer,
                        showCoordsLinesLayer,
                        chartPointsLineLayer,
                        chartPointsCircleLayer,
                        showCoordsTextLayer,
                        touchLayer,
                    ]
                )
                
                
                
                self.view.addSubview(chart.view)
                self.chart = chart
                
                //..........................**  GRAPH2 **................................
                
                let labelSettings2 = ChartLabelSettings(font: ExamplesDefaults.labelFont)
                
                let chartPoints2 = [(17,arr_avgResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("choice1age17VotesPer")! as! NSInteger), (25, arr_avgResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("choice1age18_24VotesPer")! as! NSInteger), (33, arr_avgResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("choice1age25_29VotesPer")! as! NSInteger), (41, arr_avgResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("choice1age30_39VotesPer")! as! NSInteger), (50, arr_avgResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("choice1age40_49VotesPer")! as! NSInteger), (58, arr_avgResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("choice1age50_64VotesPer")! as! NSInteger), (65, arr_avgResult .objectAtIndex(0) .valueForKey("choice1")! .valueForKey("choice1age65VotesPer")! as! NSInteger)].map{ChartPoint(x: ChartAxisValueInt($0.0), y: ChartAxisValueInt(-$0.1))}
                
                let xValues2 = ChartAxisValuesGenerator.generateXAxisValuesWithChartPoints(chartPoints2, minSegmentCount: 17, maxSegmentCount: 65, multiple: 2, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings2)}, addPaddingSegmentIfEdge: false)
                let yValues2 = ChartAxisValuesGenerator.generateYAxisValuesWithChartPoints(chartPoints2, minSegmentCount: 0, maxSegmentCount: 100, multiple: 2, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings2)}, addPaddingSegmentIfEdge: true)
                
                let xModel2 = ChartAxisModel(axisValues: xValues2, axisTitleLabel: ChartAxisLabel(text: "Axis title", settings: labelSettings2))
                let yModel2 = ChartAxisModel(axisValues: yValues2, axisTitleLabel: ChartAxisLabel(text: "Axis title", settings: labelSettings2.defaultVertical()))
                let chartFrame2 = ExamplesDefaults.chartFrame2(self.view.bounds)
                
                let chartSettings2 = ExamplesDefaults.chartSettings
                chartSettings2.leading  = -28
                chartSettings2.trailing = 65
                
                chartSettings2.labelsToAxisSpacingX = 100
                chartSettings2.labelsToAxisSpacingY = 23
                let coordsSpace2 = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings2, chartFrame: chartFrame2, xModel: xModel2, yModel: yModel2)
                let (xAxis2, yAxis2, innerFrame2) = (coordsSpace2.xAxis, coordsSpace2.yAxis, coordsSpace2.chartInnerFrame)
                
                let showCoordsTextViewsGenerator2 = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
                    let (chartPoint, screenLoc) = (chartPointModel.chartPoint, chartPointModel.screenLoc)
                    let w: CGFloat = 20
                    let h: CGFloat = 10
                    
                    let text = "(\(chartPoint.x), \(chartPoint.y))"
                    let font = ExamplesDefaults.labelFont
                    let textSize = ChartUtils.textSize(text, font: font)
                    let x = min(screenLoc.x + 5, chart.bounds.width - textSize.width - 5)
                    let view = UIView(frame: CGRectMake(x, screenLoc.y - h, w, h))
                    let label = UILabel(frame: view.bounds)
                    label.text = "(\(chartPoint.x), \(chartPoint.y))"
                    label.font = ExamplesDefaults.labelFont
                    view.addSubview(label)
                    view.alpha = 0
                    UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                        view.alpha = 1
                        }, completion: nil)
                    return view
                }
                
                let showCoordsLinesLayer2 = ChartShowCoordsLinesLayer<ChartPoint>(xAxis: xAxis2, yAxis: yAxis2, innerFrame: innerFrame2, chartPoints: chartPoints2)
                let showCoordsTextLayer2 = ChartPointsSingleViewLayer<ChartPoint, UIView>(xAxis: xAxis2, yAxis: yAxis2, innerFrame: innerFrame2, chartPoints: chartPoints2, viewGenerator: showCoordsTextViewsGenerator2)
                
                let touchViewsGenerator2 = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
                    let (chartPoint, screenLoc) = (chartPointModel.chartPoint, chartPointModel.screenLoc)
                    let s: CGFloat = 30
                    let view = HandlingView(frame: CGRectMake(screenLoc.x - s/2, screenLoc.y - s/2, s, s))
                    view.touchHandler = {[weak showCoordsLinesLayer2, weak showCoordsTextLayer2, weak chartPoint, weak chart] in
                        guard let chartPoint = chartPoint, chart = chart else {return}
                        showCoordsLinesLayer2?.showChartPointLines(chartPoint, chart: chart)
                        showCoordsTextLayer2?.showView(chartPoint: chartPoint, chart: chart)
                    }
                    return view
                }
                
                let touchLayer2 = ChartPointsViewsLayer(xAxis: xAxis2, yAxis: yAxis2, innerFrame: innerFrame2, chartPoints: chartPoints2, viewGenerator: touchViewsGenerator2)
                
                let lineModel2 = ChartLineModel(chartPoints: chartPoints2, lineColor: UIColor.init(red: 235/255.0, green: 76/255.0, blue: 85/255.0, alpha: 1.0), lineWidth: 2, animDuration: 2, animDelay: 0)
                let chartPointsLineLayer2 = ChartPointsLineLayer(xAxis: xAxis2, yAxis: yAxis2, innerFrame: innerFrame2, lineModels: [lineModel2])
                
                let circleViewGenerator2 = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
                    let circleView = ChartPointEllipseView(center: chartPointModel.screenLoc, diameter: 20)
                    circleView.animDuration = 2
                    circleView.fillColor = UIColor.clearColor()
                    circleView.borderWidth = 2
                    circleView.borderColor = UIColor.init(red: 235/255.0, green: 76/255.0, blue: 85/255.0, alpha: 1.0)
                    return circleView
                }
                let chartPointsCircleLayer2 = ChartPointsViewsLayer(xAxis: xAxis2, yAxis: yAxis2, innerFrame: innerFrame2, chartPoints: chartPoints2, viewGenerator: circleViewGenerator2, displayDelay: 0, delayBetweenItems: 0.05)
                
                let settings2 = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.clearColor(), linesWidth: ExamplesDefaults.guidelinesWidth)
                let guidelinesLayer2 = ChartGuideLinesDottedLayer(xAxis: xAxis2, yAxis: yAxis2, innerFrame: innerFrame2, settings: settings2)
                
                
                let chart2 = Chart(
                    frame: chartFrame2,
                    layers: [
                        xAxis2,
                        yAxis2,
                        guidelinesLayer2,
                        showCoordsLinesLayer2,
                        chartPointsLineLayer2,
                        chartPointsCircleLayer2,
                        showCoordsTextLayer2,
                        touchLayer2,
                    ]
                )
                
                //chart2.view.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
                
                self.view.addSubview(chart2.view)
                self.chart = chart2

            }
                
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }

}
