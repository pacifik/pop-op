//
//  ProfileFollowerVC.swift
//  PopOn
//
//  Created by Nitin Suri on 26/05/16.
//  Copyright © 2016 Nitin Suri. All rights reserved.
//

import UIKit
import SwiftLoader

class ProfileFollowerVC: UIViewController,SlideNavigationControllerDelegate,WebServiceDelegate {

    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var tablePolls:           UITableView!
    @IBOutlet weak var imageUserPic:         UIImageView!
    @IBOutlet weak var lblUserName:          UILabel!
    @IBOutlet weak var lblUserLoaction:      UILabel!
    @IBOutlet weak var btnPollsOutlet:       UIButton!
    @IBOutlet weak var btnFollowerOutlet:    UIButton!
    @IBOutlet weak var btnFollowingOutlet:   UIButton!
    @IBOutlet weak var tableProfileFollower: UITableView!
    @IBOutlet weak var lblPollsOutlet:       UILabel!
    @IBOutlet weak var lblFollowerOutlet:    UILabel!
    @IBOutlet weak var lblFollowingOutlet:   UILabel!
    @IBOutlet weak var btnRightToggle:       UIButton!
    
    var arr_MyPolls     = NSMutableArray()
    var arr_MyFollowers = NSMutableArray()
    var arr_Following   = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        btnRightToggle.addTarget(slidecontroller, action: #selector(slidecontroller.toggleRightMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        lblFollowerOutlet.textColor = UIColor.whiteColor()
     
        lblPollsOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
   
        lblFollowingOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
   
        btnFollowerOutlet.selected  = true
        btnPollsOutlet.selected     = false
        btnFollowingOutlet.selected = false
   
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.navigationController?.navigationBarHidden = true
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
        self.profileViewAPI()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileFollowerVC.followerSelected), name: "btnFollower", object: nil)
        
        //btnFollowing
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileFollowerVC.followingSelected), name: "btnFollowing", object: nil)
    }
    
    func followerSelected()
    {
        lblFollowerOutlet.textColor = UIColor.whiteColor()
        
        lblPollsOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        lblFollowingOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        btnFollowerOutlet.selected  = true
        btnPollsOutlet.selected     = false
        btnFollowingOutlet.selected = false
        
        self.profileViewAPI()
        
        
    }
    
    func followingSelected() {
        
        lblFollowerOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        lblPollsOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        lblFollowingOutlet.textColor = UIColor.whiteColor()
        
        btnPollsOutlet.selected     = false
        btnFollowingOutlet.selected = true
        btnFollowerOutlet.selected  = false
        
    self.profileViewAPI()
        
       
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if btnFollowerOutlet.selected == true {
            
            return 65
        }
        else if btnFollowingOutlet.selected == true
        {
            return 65
        }
        else
        {
           return 100
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        
        if btnFollowerOutlet.selected == true {
            
            return arr_MyFollowers.count
        }
        else if btnFollowingOutlet.selected == true
        {
            return arr_Following.count
        }
        else if btnPollsOutlet.selected == true
        {
            return arr_MyPolls.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if btnFollowerOutlet.selected == true {
            let cellIdentifier = "followerCell"
            
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            let image_user = cell.viewWithTag(14) as? UIImageView
            if arr_MyFollowers.objectAtIndex(indexPath.row)  .valueForKey("follower_profile_image") as! String != "" {
                
                let imageUrl1 = NSURL(string: arr_MyFollowers.objectAtIndex(indexPath.row)  .valueForKey("follower_profile_image") as! String)
                
                image_user!.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "user-small"))
            }
            else
            {
                image_user?.image = UIImage (named: "user-small")
            }
            
            let lblUsername = cell.viewWithTag(10)as? UILabel
            
            if arr_MyFollowers.objectAtIndex(indexPath.row) .valueForKey("follower_user_name") as? String == "" {
                
                lblUsername?.text = arr_MyFollowers.objectAtIndex(indexPath.row) .valueForKey("follower_full_name") as? String
            }
            else
            {
            lblUsername?.text = arr_MyFollowers.objectAtIndex(indexPath.row) .valueForKey("follower_user_name") as? String
            }
            
            let str_City = arr_MyFollowers.objectAtIndex(indexPath.row) .valueForKey("follower_city")  as? String
            
            let str_State = arr_MyFollowers.objectAtIndex(indexPath.row) .valueForKey("follower_state")  as? String
            
            let lblLocation = cell.viewWithTag(11) as? UILabel
            
            if (str_City == "" && str_State == "") {
                
                lblLocation!.text = ""
            }
            else
            {
                lblLocation!.text = str_City! + ", " + str_State!
            }
            
            let btn_Follow_Outlet = (cell.viewWithTag(12) as? UIButton)!
            
            if arr_MyFollowers.objectAtIndex(indexPath.row) .valueForKey("alreadyFollowing")  as? String == "yes" {
                
                btn_Follow_Outlet .setImage(UIImage (named: "check"), forState: UIControlState .Normal)
            }
            else
            {
                btn_Follow_Outlet .setImage(UIImage (named: "add-2"), forState: UIControlState .Normal)
            }
            
            let indexPerSelection = indexPath.row
            
            btn_Follow_Outlet.layer .setValue(indexPerSelection, forKey: "followIndex")
            
            btn_Follow_Outlet.addTarget(self, action: #selector(ProfileFollowerVC.buttonFollow_Follow), forControlEvents: UIControlEvents.TouchUpInside)
            
            return cell
        }
        else if btnFollowingOutlet.selected == true
        {
            let cellIdentifier = "followingCell"
            
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            let image_user = cell.viewWithTag(13) as? UIImageView
            if arr_Following.objectAtIndex(indexPath.row)  .valueForKey("following_profile_image") as! String != "" {
                
                
                let imageUrl1 = NSURL(string: arr_Following.objectAtIndex(indexPath.row)  .valueForKey("following_profile_image") as! String)
                
                image_user!.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "user-small"))
            }
            else
            {
                image_user?.image = UIImage (named: "user-small")
            }
            
            let lblUsername = cell.viewWithTag(6)as? UILabel
            //following_user_name
            if arr_Following.objectAtIndex(indexPath.row) .valueForKey("following_user_name") as? String == "" {
                lblUsername?.text = arr_Following.objectAtIndex(indexPath.row) .valueForKey("following_full_name") as? String
            }
            else
            {
            lblUsername?.text = arr_Following.objectAtIndex(indexPath.row) .valueForKey("following_user_name") as? String
            }
            
            let str_City = arr_Following.objectAtIndex(indexPath.row) .valueForKey("following_city")  as? String
            
            let str_State = arr_Following.objectAtIndex(indexPath.row) .valueForKey("following_state")  as? String
            
            let lblLocation = cell.viewWithTag(7) as? UILabel
            
            if (str_City == "" && str_State == "") {
                
                lblLocation!.text = ""
            }
            else
            {
                lblLocation!.text = str_City! + ", " + str_State!
            }
            
            let btn_Follow_Outlet = (cell.viewWithTag(8) as? UIButton)!
            
            if arr_Following.objectAtIndex(indexPath.row) .valueForKey("alreadyFollowing")  as? String == "yes" {
                
                btn_Follow_Outlet .setImage(UIImage (named: "check"), forState: UIControlState .Normal)
            }
            else
            {
                btn_Follow_Outlet .setImage(UIImage (named: "add-2"), forState: UIControlState .Normal)
            }
            
            btn_Follow_Outlet.layer .setValue(indexPath.row, forKey: "followingIndex")
            
            btn_Follow_Outlet.addTarget(self, action: #selector(ProfileFollowerVC.buttonFollow_Following), forControlEvents: UIControlEvents.TouchUpInside)
            
            return cell
        }
        else
        {
            let cellIdentifier = "pollsCell"
            
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            let lblTitle = cell.viewWithTag(1)as? UILabel
            
            let emojiRetrieveQuestion = arr_MyPolls.objectAtIndex(indexPath.row) .valueForKey("question") as? String
            let dataemojiQues: NSData = emojiRetrieveQuestion!.dataUsingEncoding(NSUTF8StringEncoding)!
            let strEmojiQues: String = String(data: dataemojiQues, encoding: NSNonLossyASCIIStringEncoding)!
            
            lblTitle?.text = strEmojiQues
            
            var intAvg2 = NSInteger()
            intAvg2 = arr_MyPolls.objectAtIndex(indexPath.row) .valueForKey("avgChoice2Percentage") as! NSInteger
            
            let lblChoice1 = cell.viewWithTag(2) as? UILabel
            
            let fAvg2 = "\(String(intAvg2))%"
            
            lblChoice1?.text = String(fAvg2)
            
            var intAvg1 = NSInteger()
            intAvg1 = arr_MyPolls.objectAtIndex(indexPath.row) .valueForKey("avgChoice1Percentage") as! NSInteger
            
            let lblChoice3 = cell.viewWithTag(3) as? UILabel
            
            let fAvg1 = "\(String(intAvg1))%"
            
            lblChoice3?.text = String(fAvg1)
            
            var intVotes = NSInteger()
            intVotes = arr_MyPolls.objectAtIndex(indexPath.row) .valueForKey("Votes") as! NSInteger
            
            let lblVotes = cell.viewWithTag(4) as? UILabel
            lblVotes?.text = String(intVotes)
            
            return cell
        }
        
    }
    
    // DID SELECT TABLEVIEW DELEGATE METHOD
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if btnFollowerOutlet.selected == true || btnFollowingOutlet.selected == true {
            
            let userProfileObj = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileVC")as! MyProfileVC
            
            if btnFollowingOutlet.selected == true {
                
                let followID1 = (arr_Following .objectAtIndex(indexPath.row) .valueForKey("following_id"))! as AnyObject
                
                userDefaults .setValue(followID1 , forKey: "otheruserID")
            }
            else if btnFollowerOutlet.selected == true
            {
                let followID = (arr_MyFollowers .objectAtIndex(indexPath.row) .valueForKey("follower_id"))! as AnyObject
                
                userDefaults .setValue(followID , forKey: "otheruserID")
            }
            
            self.navigationController?.pushViewController(userProfileObj, animated: true)
        }
        else
        {
        let results_Age = self.storyboard?.instantiateViewControllerWithIdentifier("AgeVC")as!
        AgeVC
            
        let poll_ID = arr_MyPolls .objectAtIndex(indexPath.row) .valueForKey("poll_id")! as AnyObject
            
            userDefaults .setValue(poll_ID, forKey: "poll_id")
            
        self.navigationController?.pushViewController(results_Age, animated: true)
        }
    }

    // MARK: - BUTTON FOLLOWING - FOLLOW ACTION METHOD
    
    @IBAction func buttonFollow_Following(sender:UIButton)
    {
        let btnFollowing = sender
        
        let nsindexPathFlng = btnFollowing.layer .valueForKey("followingIndex") as! NSInteger
        
        if arr_Following.objectAtIndex(nsindexPathFlng) .valueForKey("alreadyFollowing")  as? String == "no" {
            
            let followID = (arr_Following .objectAtIndex(nsindexPathFlng) .valueForKey("following_id"))! as AnyObject
            
            userDefaults .setValue(followID , forKey: "followID")
            
            self.followUserAPI()
            
        }
        else
        {
            let followID = (arr_Following .objectAtIndex(nsindexPathFlng) .valueForKey("following_id"))! as AnyObject
            
            userDefaults .setValue(followID , forKey: "followID")
            
            self.actionSheetUnfollow()
        }
    }
    
    // MARK: - BUTTON FOLLOW - FOLLOW ACTION METHOD
    
    @IBAction func buttonFollow_Follow(sender:UIButton)
    {
        let btnFollow = sender
        
        print(btnFollow.layer .valueForKey("followIndex"))
        
        let nsindexPathFollow = btnFollow.layer .valueForKey("followIndex") as! NSInteger
        
        print(nsindexPathFollow)
        
        if arr_MyFollowers.objectAtIndex(nsindexPathFollow) .valueForKey("alreadyFollowing")  as? String == "no" {
            
            let followID = (arr_MyFollowers .objectAtIndex(nsindexPathFollow) .valueForKey("follower_id"))! as AnyObject
            
            userDefaults .setValue(followID , forKey: "followID")
            
            self.followUserAPI()
        }
        else
        {
            let followID = (arr_MyFollowers .objectAtIndex(nsindexPathFollow) .valueForKey("follower_id"))! as AnyObject
            
            userDefaults .setValue(followID , forKey: "followID")
            
            self.actionSheetUnfollow()
        }
        
    }
    
    // MARK: - BUTTON POLLS ACTION

    @IBAction func btnPollsAction(sender: AnyObject) {
        
        lblFollowerOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        lblPollsOutlet.textColor = UIColor.whiteColor()
        
        lblFollowingOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        btnPollsOutlet.selected     = true
        
        btnFollowingOutlet.selected = false
        btnFollowerOutlet.selected  = false
        
        tablePolls .reloadData()
        
    }
    
    // MARK: - BUTTON FOLLOWER ACTION
    
    @IBAction func btnFollowerAction(sender: AnyObject) {
        
        lblFollowerOutlet.textColor = UIColor.whiteColor()
        
        lblPollsOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        lblFollowingOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        btnPollsOutlet.selected     = false
        btnFollowingOutlet.selected = false
        btnFollowerOutlet.selected  = true
        
        tablePolls .reloadData()
    }
    
    // MARK: - BUTTON FOLLOWING ACTION 
    
    @IBAction func btnFollowingAction(sender: AnyObject) {
        
        lblFollowerOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        lblPollsOutlet.textColor = UIColor.init(red: 40/255.0, green: 131/255.0, blue: 255/255.0, alpha: 1.0)
        
        lblFollowingOutlet.textColor = UIColor.whiteColor()
        
        btnPollsOutlet.selected     = false
        btnFollowingOutlet.selected = true
        btnFollowerOutlet.selected  = false
        
        tablePolls .reloadData()
    }
    
    //MARK: < slider delegate for showing left and right menu >
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return false
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return true
    }
    
    // MARK: - FOLLOW USER API
    
    func followUserAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "follow_id": userDefaults.valueForKey("followID")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KFollowUser)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }

    // MARK: - MY POLLS API
    
    func profileViewAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!, "view_user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
            //print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KProfileView)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }
    
    func actionSheetUnfollow() -> Void {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Pop-Op", message: "Unfollow", preferredStyle: .ActionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        let unfollowActionButton: UIAlertAction = UIAlertAction(title: "Unfollow", style: .Default)
        { action -> Void in
            
            self.UnfollowUserAPI()
        }
        
        actionSheetControllerIOS8.addAction(unfollowActionButton)
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
    }

    
    // MARK: - UNFOLLOW USER API
    
    func UnfollowUserAPI()  {
        if AppManager.NetWorkReachability.isConnectedToNetwork() == true
        {
            AppManager.sharedManager.delegate = self
            SwiftLoader.show(animated: true)
            
            let dict_User_Info : NSDictionary = ["result": true, "user_id": userDefaults .valueForKey("UserIdSave")!,"access_token": userDefaults .valueForKey("access_token")!, "follow_id": userDefaults.valueForKey("followID")!]
            
            //print(dict_User_Info)
            do
            {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict_User_Info, options:  NSJSONWritingOptions())
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                let params: [String: AnyObject] = ["user_details":jsonString]
                
                print(params)
                
                AppManager.sharedManager.postDataOnserver(params, postUrl: Header.KUnfollow)
            }
        }
        else
        {
            let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: "Make sure your device is connected to the internet." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
            }
            alertViewShow.addAction(okAction)
            self.presentViewController(alertViewShow, animated: true, completion: nil)
        }
    }

    
    // MARK: - SERVER API RESPONSE
    
    func serverResponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        print(responseDict)
        SwiftLoader.hide()
        if serviceurl .isEqualToString(Header.KProfileView)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                arr_Following .removeAllObjects()
                arr_MyPolls .removeAllObjects()
                arr_MyFollowers .removeAllObjects()
            
            arr_MyPolls .addObjectsFromArray(responseDict .objectForKey("profileData")! .objectForKey("polls")! as AnyObject as! [AnyObject])
                
            arr_Following .addObjectsFromArray(responseDict .objectForKey("profileData")! .objectForKey("following")! as AnyObject as! [AnyObject])
                
            arr_MyFollowers .addObjectsFromArray(responseDict .objectForKey("profileData")! .objectForKey("followers")! as AnyObject as! [AnyObject])
                
                
                if responseDict .objectForKey("profileData")! .valueForKey("view_user_name") as? String == "" {
                    lblUserName.text = responseDict .objectForKey("profileData")! .valueForKey("view_full_name") as? String
                }
                else
                {
                    lblUserName.text = responseDict .objectForKey("profileData")! .valueForKey("view_user_name") as? String
                }
                
            //lblUserName.text = responseDict .objectForKey("profileData")! .valueForKey("view_full_name") as? String
                
           if responseDict .objectForKey("profileData")! .valueForKey("view_profile_image") as! String != "" {
            
            let imageUrl1 = NSURL(string: responseDict .objectForKey("profileData")! .valueForKey("view_profile_image") as! String)
            
            imageUserPic.sd_setImageWithURL(imageUrl1!, placeholderImage: UIImage (named: "user-small"))
            }
            else
           {
            imageUserPic.image = UIImage (named: "user-lrg")
           }
            let str_City = responseDict .objectForKey("profileData")! .valueForKey("view_user_city")  as? String
            
            let str_State = responseDict .objectForKey("profileData")! .valueForKey("view_user_state")  as? String
            
                if (str_City == "" && str_State == "") {
                    
                    lblUserLoaction!.text = ""
                }
                else
                {
                    lblUserLoaction!.text = str_City! + ", " + str_State!
                }
           
            tablePolls.reloadData()
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if serviceurl .isEqualToString(Header.KFollowUser)
        {
            //print(responseDict)
            if responseDict .valueForKey("result")! .boolValue == true
            {
                userDefaults .setValue(responseDict .valueForKey("countFollowers"), forKey: "countFollowers")
                
                userDefaults .setValue(responseDict .valueForKey("countFollowing"), forKey: "countFollowing")
                
                NSNotificationCenter.defaultCenter().postNotificationName("countFollowing", object: nil)
                
                self.profileViewAPI()
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        else if (serviceurl .isEqualToString(Header.KUnfollow))
        {
            if responseDict .valueForKey("result")! .boolValue == true
            {
                userDefaults .setValue(responseDict .valueForKey("countFollowers"), forKey: "countFollowers")
                
                userDefaults .setValue(responseDict .valueForKey("countFollowing"), forKey: "countFollowing")
                
                NSNotificationCenter.defaultCenter().postNotificationName("countFollowing", object: nil)
                
                self.profileViewAPI()
            }
            else if responseDict .valueForKey("result")! .boolValue == false
            {
                let alertViewShow = UIAlertController(title:Header.APP_TITLE as String, message: responseDict .valueForKey("response")! as! NSString as String, preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                }
                alertViewShow.addAction(okAction)
                self.presentViewController(alertViewShow, animated: true, completion: nil)
            }
        }
        
    }
    func failureRsponseError(failureError:NSError)
    {
        print(failureError)
        SwiftLoader.hide()
        AppManager.sharedManager.Showalert(Header.APP_TITLE, alertmessage: failureError.localizedDescription)
    }

}
